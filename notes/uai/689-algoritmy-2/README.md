# Algoritmy II

## Graf

-   G (V, E)
    -   V - vrcholy - vertices
        -   stupen vrcholu - pocet hran do nej vedoucich
        -   soucet stupnu vsech vrcholu je sudy
    -   E - hrany - edges, E je podmn. VxV, |E| = pocet hran
-   souvisly/nesouvisly
-   ne/ohodnoceny
-   ne/orientovany
-   izomorfni
    -   grafy se lisi pouze "ocislovanim" vrcholu
-   podgraf, indukovany podgraf
-   komponenta grafu - maximalni souvisly podgraf
-   kostra
    -   takovy podgraf, ktery je stromem
    -   minimalni kostra - takova kostra, ktera ma nejnizsi soucet ohodnoceni hran
-   sled - posloupnost vrcholu takova, ze mezi kazdymi dvema po sobe jdoucimi vrcholy je hrana
-   tah - sled ve kterem se neopakuji hrany
-   cesta - sled ve kterem se neopakuji vrcholy a kazdy vrchol je na ceste prave jednou
-   typy: cesta, kruznice, uplny, strom, bipartitni, uplny bipartitni

### Graficky zapis

-   obrazkem
-   definici
-   matice sousednosti
    -   vhodna pro huste grafy
    -   je treba dopredu znat pocet vrcholu
    -   pamet `O(|V|^2)`, init `O(|V|^2)`, vlozeni hrany `O(1)`, hledani/odebrani hrany `O(1)`
    -   vylepseni - ukladat pulku pro neorientovane, booleany misto intu
-   matice incidence
    -   vztah mezi hranou a jejimi koncovymi vrcholy
    -   vhodna pro ridke grafy -> cim mene hran, tim mensi matice
    -   je treba dopredu znad pocet vrcholu i hran
    -   pamet `O(|V|\*|E|)`, init `O(|V|\*|E|)`
    -   vylepseni - modifikace pro orient. a ohodn., booleany mist ointu, symetricka - staci jen pulka
-   matice vzdalenosti
    -   najdeme nejmensi vzdalenost mezi vrcholy
-   seznam sousedu
    -   2 pole - prvni pole seznam vrcholu (odkazuje do druheho), druhe pole za sebou seznam sousedu
        -   ![2d pole](./graph-2d-array.png)
    -   spojove seznamy - pro kazdy vrchol seznam jeho sousedu
        -   ![linked list](./graph-linked-list.png)
    -   vyhoda - snadne zjisteni sousedu, pridani hrany
    -   pamet `O(|V|+|E|)`, init `O(|V|+|E|)`, vlozeni hrany `O(1)` - spojak, hledani `O(|E|)`
-   Laplaceova matice

### Prochazeni grafem

-   efektivni pruchod grafem - `O(n+m)`
    -   kazda hrana se projde max jednou
    -   hranou se vraci az kdyz z uzlu nevede dalsi cesta
    -   hranou vedouci do jiz navstveneho uzlu se hned vraci

#### Pruchod do hloubky - DFS

```
dfs:
    zasobnik
    zasobnik.push(prvni_vrchol)
    while zasobnik not empty:3
        vrchol = zasobnik.pop()
        if(vrchol not discovered):
            vrchol is discovered
            for all vrchol edges as soused:
                zasobnik.push(soused)
```

#### Pruchod do sirky - BFS

```
bfs:
    fronta
    prvni_vrchol is discovered
    fronta.enqueue(prvni_vrchol)
    while fronta not empty:
        vrchol = fronta.dequeue()
        if(vrchol is goal)
            return
        for all vrchol edges as soused
            if(soused not discovered)
                soused is discovered
                fronta.enqueue(soused)
```

### Strom

-   souvisly graf, ktery neobsahuje jako podgraf kruznici (po pridani libovolne hrany vznikne kruznice)
    -   vrcholy stupne 1 - listy

### Kostra

#### Kruskaluv algoritmus

-   min. kostra grafu
-   oznacuju nejmensi hrany
-   davam pozor aby nevznikl cyklus
-   cas `O(|E|log|V|)`
-   cas - setridene hrany `O((|V|+|E|)alfa|V|)`

#### Jarnikuv (Primuv) algoritmus

-   mam mnozinu vybranych vrcholu
-   k ni vzdy pridam sousedni vrcholy s nejlepsi vahou, davam pozor na cykly
-   cas - pole `O(|V|^2+|E|)`
-   cas - halda `O(|V|^2+|E|log|V|)`

#### Boruvkuv algoritmus

-   udrzuje si les (mnozina vrcholu)
-   na zacatku nepropojene vrcholy
-   pridava nejlepsi hrany a propojuje stromy v lese
-   cas `O(|E|log|V|)`

### Hledani nejkratsi cesty

#### Relaxace

-   technika , kterou vyuzivaji alg. pro hledani nejkr. cesty
-   relaxace hrany _(u, v)_, je test, jestli je mozne udelat kratsi cestu do _v_ tak ze pojedeme pres _u_

#### Bellman-Forduv algoritmus

-   hleda nejkratsi cestu v ohodnocenem grafu ze startu do vsech vrcholu
-   prochazi nekolikrat vsechny vrcholy a postupne upravuje hodnoty cest
    -   k tomu vyuziva relaxaci
-   cas `O(|V|*|E|)`

#### Djikstruv algoritmus

-   pamatuje si mnozinu jiz navstivenych hran a odhad delky do nenavstivenych vrcholu
-   v kazdem pruchodu vybere nenavstiveny vrchol, jehoz vzdalenost od startu je nejmensi
    -   vrchol navstivi a aktualizuje vzdalenosti do jeho sousedu
-   cas `O(|V|log|V|+|E|)`

#### Floyd-Warshalluv algoritmus

-   projde vsechny moznosti cest mezi kazdymi dvema vrcholy
-   najde vzdalenost mezi vsemi dvojicemi vrcholu
-   cas `O(|V|^3)`

## Vyhledavani retezcu

-   pro hledany retezec hledame vzor uvnitr textu
-   retezec - _S_ velikosti _m_
-   podretezec - cast retezce, _S[i..j]_
-   prefix, suffix

### Brute force

-   pro kazdou pozici v textu kontrolujeme
-   cas `O(mn)` - nejhorsi pripad, vetsinout `O(m+n)`
-   rychlejsi nad velkou abecedou, pomaly nad malou (binarni soubory..)

### Boyer-Moore algoritmus

-   zrcadlovy pristup - zaciname na konci
-   preskakuje skupiny znaku ktere se neshoduji
-   funkce Last() - zobrazuje znaky abecedy do mnoziny celych cisel
    -   vraci posledni vyskyt znaku
    -   uchovava se jako pole
-   cas `O(mn+A)` - rychlejsi na vetsi abecede

### KMP - Knuth-Morris-Pratt

-   vyhledava zleva do prava
-   posouva se o nejvetsi mozny skok
-   chybova funkce
    -   nejdelsi prefix P[0..k] ktery je suffixem P[1..k]
-   cas - optimalni `O(m+n)`
-   nikdy se neposouva zpet => vyhodny u velkych souboru

### Rabin-Karp

-   vypocita
    -   kontrolni soucet pro vzor delky _m_
    -   kontrolni soucet pro kazdy podretezec delky _m_
-   prochazi a porovnava soucty - kdyz shoda -> zkontroluje znak po znaku
-   kontrolni soucet
    -   zvolime prvocislo _q_
    -   zvolime pocet znaku v abeced _d_
    -   ![crc](./rabin-karp-crc.png)
-   cas - nejhorsi `O(m(n-m+1)`

## Algoritmicke problemy

### Backtracking

-   systematicky pruchod vsemi moznymi konfiguracemi prostoru
-   obvykle DFS (mensi spotreba pameti nez BFS)
-   pokud nelze postupovat dal smerem k listu -> navrat k poslednimu rozhodovacimu mistu

### Princip Merge sort

-   2 setridena pole
-   pomocna pamet (velikost `arr1.length+arr2.length`)
-   u kazdeho pole index (ukazatel)
-   porovnavame prvky na indexech a do vysledku pridame mensi prvek

### Hra NIM

-   2 hraci, hromada kamenu
-   hraci se stridaji, v kazdem kole musi odebrat 1,2 nebo 3
-   prohrava ten kdo odebere posledni
-   problemy
    -   odebirani tak, aby protihrac mel nevyhodnou pozici, tj. 1,5,9... => 4n+1
    -   stroj tedy odebira _x_ kamenu z poctu kamenu _K_ => K-x == 4n+1 => tj. x = K-1%4 (jeli x== 0 vysledek bude nevyhodne pro stroj)

## Sifry

-   symetricke - stejny klic na zasifrovani i desifrovani
    -   Caesarova, Enigma, DES, IDEA, Skipjack
-   asymetricke - na zasifrovani se pouzije verejny klic a na rozsifrovani soukromy
    -   zalozeno na jednocestnych funkcni (diskretni logaritmus)
    -   RSA, Diffie-Hellman, ElGamal, Knapsack
-   hashovaci funkce
    -   ze vstupu udela konstatni pocet bitu
    -   MDA, DSA, XOR, md5, bcrypt
-   jednosmerna funkce
    -   jednim smerem snadno, druhym obtizne

**grupa** - mnozina _G_ a binarni operac - grupova operace, ta prirazuje libovolnym 2 prvkum grupy a,b prvek c (take z grupy) _c=a.b_

**cyklicka grupa** - generovana jedinym prvkem _g_ -> prvky jsou mocniny _g_

### Diffie-Hellman

-   kryptograficky protokol umoznujici vymenu klicu pres nezabezbeceny kanal
-   bez dalsich technik muze byt prolomen man in the middle -> nutna autentizace (verejny a soukromy klic) napr. RSA
-   postup
    -   A a B se domluvi na multiplikativni cyklicke konecne grupe _G_ s generatorem _g_
    -   A si zvoli nahodne cislo _a_ a odesle _g^a_ -> B
    -   B si zvoli nahodne cislo _b_ a odesle _g^b_ -> A
    -   A vypocte _(g^b)^a_
    -   B vypocte _(g^a)^b_

### RSA

-   verejny a soukromy klic

### Caesarova sifra

-   posun znaku
-   snadno brute forcem nebo frekvencni analyzou

### MD5

-   128bit hash
-   prolomena (nasla se kolize uz 2005)

## Komprese

-   redukce objemu dat
-   kompresni pomer - pomer komprimovanych a originalnich dat
-   bezztratova komprese - zpetna rekonstrukce dat - ZIP, RAR, gzip
-   ztratova komprese - zlomek puvodni velikosti, nektera data se ztraceji - JPEG, MPEG, MP3
-   metody
    -   jednoduche - kodovani opakujicich se znaku - RLE
    -   statisticke - zalozeno na cetnosti znaku
    -   slovnikove - kodovani castych posloupnosti - LZW
    -   transformacni - ortogonalnich, popr jinych transformacich - JPEG

### LZW

-   hleda opakujici se posl. znaku a uklada je do slovniku
-   kdyz nemam ve slovniku pridam +1 znak
