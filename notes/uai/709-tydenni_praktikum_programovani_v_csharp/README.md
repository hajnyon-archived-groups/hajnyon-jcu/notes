# 709 - Týdenní praktikum programování v C#

## Zaklady .NET, C#

### .NET

- vznik 2002 - odpoved na javu - sjednoceni jazyku (podarilo se)
- chteli kompatibilitu - linux, win .. (nepodarilo se - primarne windows)
- C# hlavni jazyk .NET prostredi
- vzniko MONO - fork pro linux, ale nepodpora MS
- Xamarin - mobilni platforma
- vznik .NET Core - multiplatformni

### .NET Core

- multiplatformnost
- current .NET 5.0
- open source
- lightweight, rychlejsi oproti .NET
- CLI (new, run, test)

### C#

- namespace - izoluje tridy aby se zabranilo duplikaci jmen
- `main` - metoda spousti aplikaci
  - uz neni potreba, lze udelat podobne jako node script
- `.csproj` - konfiguracni soubor (verze .net)
- `int, string, var ...`
- `if, else, switch, for, foreach ...`
- `Console.Write("aaa {0}", zeroArgument)`
- `$"aaa {zeroArgument}"` interpolace
- `Console.ReadLine` - cte stringy, je treba parse (`int.parse()`)
- `\\` nutne escapovat zpetne lomitko nebo escaping vypnout `@"c:\aa\"`
- `List`
- `int.TryParse(input, out)` - vraci true/false jak se parse povede



## GUI

## Communication (REST, GRPC)

## Containers
