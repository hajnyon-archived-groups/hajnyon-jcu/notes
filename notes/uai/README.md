---
sidebar: false
---

# UAI

## LS 2021/22

[716 - PKI a elektronicky podpis](./716-pki_a_elektronicky_podpis/README.md)

## ZS 2021/22

[700 - Agilni metody vyvoje software](./700-agilni_metody_vyvoje_software/README.md)

[717 - Pocitacova kriminalita (kybernalita)](./717-pocitacova_kriminalita/README.md)

[732 - Teoreticka informatika](./732-teoreticka_informatika/README.md)

## LS 2020/21

[675 - 3D Tisk a modelovani](./675-3d_tisk_a_modelovani/README.md)

[680 - Moderni webove technologie](./680-moderni_webove_technologie/README.md)

[696 - Bezpecnost informacnich systemu](./696-bezpecnost_informacnich_systemu/README.md)

[766 - Vypocetni inteligence](./766-vypocetni_inteligence/README.md)

[771 - Pokrocile programovani v C++](./771-pokrocile_programovani_v_cpp/README.md)

[775 - Internetove aplikace](./775-internetove_aplikace/README.md)

[789 - Multimedialni technologie](./789-multimedialni_technologie/README.md)

## ZS 2020/21

[683 - Pokrocile databazove systemy](./683-pokrocile-databazove-systemy/README.md)

[685 - Objektove programovani II](./685-objektove_programovani-2/README.md)

[687 - Znackovaci jazyky](./687-znackovaci_jazyky/README.md)

[691 - Data mining](./691-data_mining/README.md)

[693 - Nastroje softwaroveho modelovani](./693-nastroje_softwaroveho_modelovani/README.md)

[701 - Administrace Windows Serveru](./701-administrace_windows_serveru/README.md)

[709 - Tydenni praktikum programovani v C#](./709-tydenni_praktikum_programovani_v_csharp/README.md)

[719 - Prostredky osobni identifikace a biometrie](./719-prostredky_osobni_identifikace_a_biometrie/README.md)

[785 - Architektura aplikaci](./785-architektura_aplikaci/README.md)

## LS 2019/20

[684 - Operacni systemy 1](./684-operacni_systemy-1/README.md)

[689 - Algoritmy II](./689-algoritmy-2/README.md)

[695 - Objektove programovani I](./695-objektove_programovani-1/README.md)

[697 - Databáze](./697-databaze/README.md)

[698 - Architektura pocitacu I](./698-architektura_pocitacu-1/README.md)

[699 - Pocitacove site](./699-pocitacove_site/README.md)

## ZS 2019/20

[686 - Algoritmy I](./686-algoritmy-1/README.md)

[684 - Pravni minimum pro informatiky](./648-pravni_minimum_pro_informatiky/README.md)

[761 - Uvod do SW aplikaci](./761-uvod_do_sw_aplikaci/README.md)
