# 785 - Architektura aplikaci

## 1. hodina

### Zakladni vrstvy

-   **vstupne vystupni operace** - sluzby ovladani souboru (I/O operace)
-   **datove sluzby** - akce s db (definice, manipulace, transakcni zpracovani)
-   **logika dat** - **zde konci sluzby db serveru**
    -   podpora operaci s daty (integritni omezeni)
    -   nekdy oznacovano jako business logika
    -   struktura dat (tabulky), dodrzovani klicu a jinych integritnich omezeni, spravnost dat
-   **logika aplikace**
    -   vsechno co nepatri do ostatnich vrstev :)
    -   vypocty, akce v ramci aplikace (pridavani, odesilani pres API, logovani)
    -   pr.: kdyz vlozim v IS skoly novou tridu -> logika aplikace rika, ze se ma zalozit i tridni kniha, priradi tridniho uzivatele atd..
-   **prezentacni logika** - rizeni interakce (pr.: pri prechodu na jinou stranku uzivatele upozornim, ze nema ulozene zmeny, redirect po uspesnym ulozeni, error stranka pri 404 apod.)
-   **prezentacni sluzby** - graficky/textovy/.. rozhrani pro uzivatele

### Varianty architektury

-   klient/server se vzdalenymi daty
    -   server - jen uloziste dat (datove sluzby, ovladani souboru)
    -   klient - zbytek (prezentacni sluzby a logika, logika aplikace a logika dat)
-   klient/server se vzdalenou prezentaci
    -   server - logika aplikace a dat, datove sluzby, ovladani souboru
    -   klient - jen prezentacni sluzby, logika
    -   typicky starsi php aplikace
-   klient/server s rozdelenou logikou
    -   klient a server sdili casti
-   trivrstva architektura
    -   klient - prohlizec (js etc)
    -   aplikacni server - apache + php etc
    -   databazovy server - mysql etc
    -   prinosy
        -   rozdeleni prace (sprava dat, prezentace, grafika, logika aplikace ..)
        -   horizontalni (vice serveru) a vertikalni (vykonnejsi server) skalovani
        -   ruzne druhy GUI (webova, desktopova, mobilni appka ..)
        -   standardizovany pristup (pr: REST API)
        -   centralizace udrzby aplikace, moznost vyuziti sdilenych objektu nekolika aplikacemi

### Archtektonicky prezentacni vzory

-   motivace
    -   rozdeleni kodu
    -   neopakovani kodu - DRY
    -   testovani
-   https://www.codeproject.com/Articles/66585/Comparison-of-Architecture-presentation-patterns-M

![](./mvc-schema.png)

#### MVC

-   model (data)
    -   vstupne/vystupni operace, datove sluzby, logika dat
-   view (HTML)
    -   prezentacni sluzby
-   controller (ovladani)
    -   logika aplikace, prezentacni logika (napr. vytvorim uzivatele a vratim seznam uzivatelu, vyhodnoceni validace atd)
-   neni to navrhovy vzor, ale architektonicky vzor

-   pr.
    -   uzivatel pristoupi na stranku - GET na controller -> router to hodi na nakej specifickej
    -   controller vola DBRepository (ta kominukuje s DB - ORM) a vola taky Model (mapovani)
    -   controller pak rekne View hele vykresli tohle
    -   view si veme data, da je do sablony a vraci stranku

#### MVP

-   evoluce MVC
-   view zacali byt chytrejsi a bylo mozny s nima interagovat (rich rozhrani - formularove aplikace, do urcity miry i web. aplikace)
    -   uzivatel neodesila pres controller, ale interaguje s view, ktery to resi samo (ale zpracovani se porad deje v **Presenteru**)
-   presenter - prezentacni logika
-   view - prezentacni sluzby - posila do Presenteru pomoci eventu
-   pouziti: JS fe, desktop (java swing, winform, nektery android appky)

#### MVVM

-   model view viewmodel
-   ![](./mvvm.png)
-   chybi spojeni view a modelu
-   viewmodel - podobna funkce jako controller
    -   komunikuje s modelem
    -   prezentacni logika
    -   veskery data, ktery view bude potrebovat + prezentacni logika - zpracovani akci (click, input atd - commands)
-   view - s nim interaguje uzivatel
    -   pomerne hloupy: UI (XML, HTML..) a UI Logic (prezentacni sluzby)
-   propojeni View <-> ViewModel
    -   1:1
    -   commands (typicky jen jednim smerem do ViewModelu)
    -   data binding - provazani promenny s inputem/nejakym view elementem
        -   muze byt vice smerny (obou, jedno)
    -   notification - typicky viewmodel nevi o view (jen pomoci bindingu), ale muzou se pouzit (asi Signal v aurelii)
-   pouziti: typicky desktopova/mobilni aplikace

### Lambda vyrazy

-   vychazi z delegatu
    -   delegat - metoda, splnujici nejake rozhrani, ktera jde zavolat
-   zkracenej zapis definice metody, uz znam typy parametru i navratove hodnoty `(x,y) => x == y;`

## Navrhove vzory - desing patterns

-   poskytuji reseni na obecny a caste problemy
-   typy
    -   creational - vytvareni objektu, class a struktur
    -   structural - zjednoduseni vztahu a vazeb
    -   behavioral - strategy, visitor
    -   concurrent - souvisejici s vlakny, locky atd

### Singleton

-   neco (instance) kterou potrebuju jen jednou
-   pr: cache, connector do db, thread pool

### Factory

-   strojove vyrabene objekty
-   class (potrebuju za behu vytvorit tridy - neznam na zacatku - pr. serializace) i object factory
-   pr: definice hry (postavy, mistnosti ..) v jsonu -> factory z toho vytvori objekty

### Strategy

-   potrebuju menit algoritmus, ktery neco pocita
-   schovam skupinu algoritmu za abstrakci a algoritmus muzu menit bez nutnosti zmeny toho, kdo to pouziva (nemusime prohazovat metody atd)
-   pr. pocitam prumer - 2 metody aritmeticky a median - program

### Dependency injection

-   trida prijima objekty ktery potrebuje, na misto toho aby si je sama tvorila

### Observer

-   mam objekt/y ktery se meni a jiny objekt/y na to reaguji
-   notifikuje listenery

### Builder

-   tvori jiny, komplexni objekt

### Adapter

-   konverze jedne struktury dat do jine struktury (jine interfaces)
-   pr: XML -> JSON, BIN -> JSON atd.

### State

-   obaluje nekolik ruznych stavu objektu

### Lazy initialization

-   taky lazy loading
-   oddalujeme vytvoreni instance az do doby, kdy ji potrebujeme (jinak zabira pamet)

### Object pool

-   drahy objekty na vytvoreni (pr vlakna) - tak si je drzim v poolu a pouzivam znova
-   pr. connection pool

### Facade

-   zastresuje vice objektu, ktery zvenku potrebuju pouzivat jednim zpusobem

### Command

-   obaluje pozadavek - trida tak vola jen spust akci a nevi co dela

### Iterator

-   umoznuje iterovat prvky
-   pr. vlastni datove struktury

## Preklady

-   dobre mit mimo projekt, abysme kvuli zmene jazyka nemuseli kompilovat aplikaci
-   .NET
    -   `Window.Resource` - muzu si dat veci co se mi v xaml opakujou
    -   Resource File - v podstate slovnik, podporuje vice typu hodnot (obrazky, audio..)
        -   key - value
        -   build akce
            -   defaultne Embedded resource - pribalene k aplikaci ve specifickem souboru - muzeme aktualizovat
            -   content - pouze zkopiruje do vystupni slozky
        -   podobne funguje i `Settings.settings` - lze ukladat do user slozky nebo k aplikaci primo (dalsi moznosti: cloud, db, registry)

## Reflexe

-   umoznuje prohledavat prelozeny kod (dll, exe) a muzu se podivat co tam je a vytahat si neco co vyhovuje mym podminkam (interface) a muzu volat veci, na ktere nemam vazbu
-   vytvari instance tridy ikdyz nezname primo tridu( ale jen interfac) - pattern `Factory`
-   v podstate dependency injection

## Ostatni

### Docker

-   virtualizace
-   container - vlastne bali aplikace a poustit je na sdilenem jadre (muze ale balit i cele OS), rozdil oproti virtualu, ze ten ma cele jadro a cely OS u sebe - nic nesdili

### DAL - Data access layer

-   jedina vrstva starajici se o komunikaci s ulozistem

### ViewModel

-   usecase
    -   potrebuju upravanej model (treba orezat hesla atd) do view
    -   potrebuju vic modelu ve view - obalim to viewmodelem
    -   data ktery potrebuju jen ve view ale ne v modelu - treba zmena hesla: potrebuju 2x heslo (kontrola zadani), ale v modelu ukladam jen jedno
-   vetsinou jen plain data, popr. lokalizace
-   preklad z modelu do viewmodelu se provadi v servisnich vrstvach (v nich je prezentacni logika)

### Lazy loading

-   nacitam resourcy az kdyz je potrebujue (vytvareni instanci, cteni z db, stahovani obrazku na webu etc)
