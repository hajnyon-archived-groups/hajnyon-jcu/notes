# 687 - Znackovaci jazyky

## Uvod

-   znackovaci jazyky slouzi pro popis dat
    -   binarni - rychle zpracovani/necitelne
    -   textove
        -   zapis i cteni vyzaduje konverzi
        -   citelne a snadno editovatelne
        -   idealni pro prenositelnost mezi programy
        -   nemaji dane formatovani
        -   spatne se v nich hleda vyhledavani
-   struktura
    -   pevna - napr. `/etc/passwd` -> `novak:x:500:500:Milan Novak:/home/novak:/bin/zsh`
        -   usporne, ale neprehledne
    -   volna - jista volnost v poradi nebo formatu - napr. css
-   formalizace
    -   historicky - typografie (\*roff, TEX) - popis vzhledu
    -   noveji - popis struktury dat (a nastroje pro zachazeni, kontroly, konverze)

## XML

-   puvodne GML (Generalized Markup Language), IBM v 80. letech
    -   potreba ukladat pravni spisy - problem nekompbility v programech - potrebovali to sjednotit
-   postupne se vyvinul standard SGML (Standard Generalized Markup Lang)
    -   obecny, umoznoval definici vlastni znackovacich jazyku pomoci DTD
    -   nejznamejsi aplikaci je HTML
    -   prilis komplexni na implementaci - pouzivala se pouze dulezita podmnozina -> vznik XML
-   eXtensible Markup Language
    -   podmnozina SGML, zachovava moznost definici DTD
    -   narozdil od SGML je predem dano mnoho parametru a nelze je menit - max. delka znaku, oddelovace, spec. zn. ..
    -   pocita se sirsim pouzitim - neni omezeno na angl. jako nektere predchozi - ruzne kodovani
-   pouziti
    -   rss feedy
    -   konfiguraky
    -   prenos dat (import, export, REST API)
    -   open office - binarni XML
-   vlastnosti
    -   kodovani - UTF8 preferovano
        -   wihout BOM (byte order mark) - identifikator, ktery cpe windows na zacatek xml
    -   format je informacne bohatsi - samotne tagy nesou informaci (`<user id="22"></user>`) - uplatneni pri vyhledavani
    -   snadna konverze do jinych formatu
    -   snadna implementace v jazycich
    -   validace a kontrola struktury dokumentu (kontrolu provadi **parser**)
    -   podporad namespacu - muzu v jednom dokumentu pouzivat nekolik druhu znackovani
    -   odkazy - vice moznosti nez v HTML - spojovani vice dokumentu (standardy: XLink, XPointer, XPath)
-   vyhody
    -   validace pred samotnym zpracovanim (oproti JSONu - tam musim nejdriv parsnout)
-   nevyhody
    -   velikost (napr oproti jsonu - zalezi)
-   XHTML
    -   melo byt striktne standardizovane HTML
    -   zakazovany ruzne atributy, ktere se ale pouzivaly
    -   lower case, striktne rozdelena struktura a data

### Format

-   atribut x tag
    -   atribut - zpresnuje (eg. enum, styl)
    -   tag - mel by vzdy mit data (ma kodovani, podporuje CNAME)
    -   `id` je vyhodnejsi dat do atributu (odkazovani)
-   dokument zacina XML deklaraci `<?xml version="1.0" encoding="utf-8" ?>`
-   kazdy dokument musi mit jeden korenovy element
-   `<![CDATA[..]]` - pro kod, nepotrebuju prepisovat specialni znaky na kody
-   specialni znaky: `&` -> `&amp;`
-   instrukce - pouziti pro pripojeni stylu, prikazy pro preprocesory atd `<?xml-stylesheet href="styl.css" type="text/css"?>`

### DTD - Definice typu dokumentu

-   rika jak ma vypadat struktura XML
-   pravidla pro strukturu dokumentu
-   vyhody
    -   automaticka kontrola XML oproti DTD definici
    -   pomoci parseru lze zjistit, jestli v dokumentu neco chybi/prebyva
    -   muzeme vyuzivat vice/cizi DTD
-   nevyhody
    -   slaba typova kontrola
    -   nestandartni syntaxe
-   syntaxe
    -   elementy
        -   `<!ELEMENT faktura (odberatel, dodavatel, polozka+)>` <=> `<faktura><odberatel></odberatel><dodavatel></dodavatel><polozka></polozka></faktura>`
        -   `EMPTY` - element nemuze obsahovat dalsi elementy (pr. `<br>`)
        -   `ANY` - bez omezeni
        -   `#PCDATA` - obsahem elementu je jiz jen text, `(#PCDATA|tučně|kurzíva)` smisena skupina - obsahuje text a jeste znaky (tucne, kurziva)
        -   `|` - or, `+` - jedna a vice, `?` - nepovinny vyskyt, `*` - 0 a vice
    -   deklarace atributu
        -   `<!ATTLIST odstavec zarovnani(vlevo|vpravo) #IMPLIED>`
        -   `#IMPLIED` - standartni hodnota, `#REQUIRED` - povinny atribut, muzeme specifikovat i konkretni hodnoty atributu, `#ID` pro unikatni id a `IDREF` pro odkaz na existujici id
    -   pripojeni DTD k dokumentu
        -   `<!DOCTYPE faktura SYSTEM "faktura.dtd">` - faktura - korenovy element
    -   entity - umoznuji rozdeleni dokumentu do vice souboru a muzou pomoci pri opakovanem textu
        -   `<!ENTITY kap1 SYSTEM "kapitola1.xml">` se pak odkazuje pres `&kap1;`

### XML - schemata

-   cilem bylo nahradit DTD a dale rozsirit moznosti
    -   lepsi moznost definice datovych typu
    -   syntaxe schemat zalozena na XML
-   jazyku pro zapis schemat vznikalo nezavisle na sobe nekolik: microsoft, w3c..

#### Datove typy

-   datove typy jdou urcit i u elementu (oproti DTD)
-   zakladni
    -   cisla, cas, stringy, datum ..
    -   string, boolean, float, double, decimal, timeInstant, timeDuration, binary, uri
-   odvozene
    -   prvni skupina - z klasickeho DTD
    -   muzu definovat vlastni
    -   zalozena na integritnim omezeni (omezuju typy, pr. string -> enum)
    -   muzeme vyuzit regularni vyrazy
-   v ramci schemat definujeme element pomoci `<element name="" type""></element>`
-   subelement pomoci `<element name="" type""><type><element name="" type=""></element></type></element>`
-   `ref` odkazuju se na element definovany jinde
-   a
-   atributy - `<attribute name="vystavil" type="string" />`
-   propojeni
    -   `<faktura xmlns="faktura.xsd">...</faktura>`
    -   https://www.freeformatter.com/xml-validator-xsd.html - validator

priklad vlastniho typu

```xml
<datatype name="castka" source="decimal">
    <precision value="8" />
    <scale value="2" />
</datatype>
<!-- regexp -->
<datatype name="ico" source="string">
    <pattern value="\d{8}" />
</datatype>
<datatype name="psc" source="string">
    <pattern value="\d{3} \d{2}" />
</datatype>
<!-- enum -->
<datatype name="mena" source="string">
    <enumeration value="CZK" />
    <enumeration value="EUR" />
    <enumeration value="USD" />
</datatype>
<!-- size -->
<datatype name="login" source="string">
    <minLength value="3" />
    <maxLength value="8" />
</datatype>
```

#### Namespace

-   w3c - prefix `xs:` nebo `xsd:`
    -   `<sequence>` - skladani po sobe jdoucich elementu
    -   `list` - typ ktery muzu rozsirit (napr. matchuje sekvenci cisel: `0 1 2 3`)
    -   `union` - muzu sloucit 2 typy a podporovat tak vice typu u elementu
    -   `simpleType`
    -   `complexType` - obsahuje v sobe vice elementu
        -   `sequence`, `choice` - vyber n elemenetu ze seznamu, `all` - nezalezi na poradi elementu
    -   `mixed` - smiseny obsah (text na stejne urovni kombinovan s tagy - kurziva, bold atd.) `<p>aaa <b>aaa</b> aaa</p>`
    -   pokud se vynecha `type` bere se to jako `anyType`
-   namespace zarucuje, ze se mi nebijou stejne pojmenovany elementy
-   `targetNamespace` - nazev namespacu, u korenoveho elementu schematu
-   `elementFormDefault` - uvadi, ze cele schema musi pouzivat kvalifikovane elementy (tj. zarazene do nejakeho namespacu)
    -   muzu urcit u kazdeho elementu zvlast pomoci `form` - s hodnotou `qualified`
-   muzu definovat jiny namespace per element (aby se mi netloukly stejny jmena tagu)
-   validator: https://www.liquid-technologies.com/online-xsd-validator

#### Pristupy k navrhu schemat

```xml
<?xml version="1.0" encoding="UTF-8"?>
<zamestnanec>
  <jmeno>Jan</jmeno>
  <prijmeni>Novák</prijmeni>
  <adresa>
    <ulice>Dlouhá 2</ulice>
    <město>Praha 1</město>
    <psč>110 00</psč>
  </adresa>
  <plat>34500</plat>
</zamestnanec>
```

##### Matrjoska

-   globalni je jeden element a ostatni jsou uvnitr

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

  <xs:element name="zamestnanec">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="jmeno" type="xs:string"/>
        <xs:element name="prijmeni" type="xs:string"/>
        <xs:element name="adresa">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ulice" type="xs:string"/>
              <xs:element name="město" type="xs:string"/>
              <xs:element name="psč" type="xs:string"/>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="plat" type="xs:decimal"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

</xs:schema>
```

##### Salamova kolecka

-   vsechny elementy jsou globalni a pak jsou slozeny pomoci odkazu
-   vyhodne pro velkou variabilitu elementu (ruzne zanoreni atd)
-   nedefinu typ s restr. omezenim

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

  <xs:element name="jmeno" type="xs:string"/>
  <xs:element name="prijmeni" type="xs:string"/>
  <xs:element name="ulice" type="xs:string"/>
  <xs:element name="město" type="xs:string"/>
  <xs:element name="psč" type="xs:string"/>
  <xs:element name="plat" type="xs:decimal"/>

  <xs:element name="adresa">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="ulice"/>
        <xs:element ref="město"/>
        <xs:element ref="psč"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="zamestnanec">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="jmeno"/>
        <xs:element ref="prijmeni"/>
        <xs:element ref="adresa"/>
        <xs:element ref="plat"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

</xs:schema>
```

##### Slepy Benatcan

-   pro vsechny elementy se nadefinuji typy
-   elementy jsou definovany lokalne a pri shode jmen muzou mit ruzne modely obsahy
-   rozsirena definice typu danych elementu z kterych vznikaji slozitejsi typy
-   pouziju pri striktni definici definovanych typu s ne tak velkou variabilitou elementu

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

  <xs:simpleType name="jmenoType">
    <xs:restriction base="xs:string">
      <xs:minLength value="1"/>
      <xs:maxLength value="15"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="prijmeniType">
    <xs:restriction base="xs:string">
      <xs:minLength value="1"/>
      <xs:maxLength value="20"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="uliceType">
    <xs:restriction base="xs:string"/>
  </xs:simpleType>

  <xs:simpleType name="městoType">
    <xs:restriction base="xs:string"/>
  </xs:simpleType>

  <xs:simpleType name="psčType">
    <xs:restriction base="xs:token">
      <xs:pattern value="[0-9]{3} [0-9]{2}"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="platType">
    <xs:restriction base="xs:decimal">
      <xs:minInclusive value="0"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="adresaType">
    <xs:sequence>
      <xs:element name="ulice" type="uliceType"/>
      <xs:element name="město" type="městoType"/>
      <xs:element name="psč" type="psčType"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="zamestnanecType">
    <xs:sequence>
      <xs:element name="jmeno" type="jmenoType"/>
      <xs:element name="prijmeni" type="prijmeniType"/>
      <xs:element name="adresa" type="adresaType"/>
      <xs:element name="plat" type="platType"/>
    </xs:sequence>
  </xs:complexType>

  <xs:element name="zamestnanec" type="zamestnanecType"/>

</xs:schema>
```

### XSLT

-   XSL
    -   eXstensible Stylesheet Language
    -   umoznuje definovat vzhled elementu
    -   oddeluje styl od dat
    -   aut. gen. obsahu, cislovani, kapitol atd
-   XSL Transformations - umoznuje z XML vytvaret ruzne dalsi formaty na zaklade formatovacich formatu (XSL FO)
    -   HTML, PDF atd
-   XML + XSLT -> XSLT procesor -> XML/HTML/text
    -   ![](./xslt.png)
-   XML + XSLT -> XSLT procesor -> formatovaci objekty (FO - take XML) -> PDF/PS/RTF
    -   vytvorime si styl, ten se pomoci XSLT aplikuje na XML - dostaneme nove XML, ktere se sklada z form. objektu - tento dokument pak umi interpretovat procesor FO, ktery z nej vytvori treba PDF
    -   ![](./xslt-fo.png)
-   XSLT procesor
    -   mohou byt soucasti prohlizece
    -   saxon (java), xalan, xt, msxml (MS)
    -   libxslt/xsltproc - C
-   XLS
    -   muzeleme linknout v xml nebo prohnat nejakym programem (viz vys)
        -   `<?xml-stylesheet href="mujstyl.xsl" type="text/xsl"?>`
    -   `for-each`
        -   `sort` - select (podle ktere hodnoty), order, case-order, ..
    -   `value-of` - vrati hodnotu nejakeho elementu
    -   `apply-templates` - muzeme skladat vice
    -   pouziti pomoci xsltproc: `xsltproc faktury.xsl faktury.xml > faktury.html`
-   XPath
    -   cesty v ramci dokumentu
    -   dokument reprezentovan jako strom
    -   vyber elementu
        -   pomoci jmena
        -   `*` dcerine
        -   `.` aktualni element
        -   tyto zapisy lze kombinovat pomoci `/` a `//`
    -   vyber atributu pomoci `@atribut`
        -   `@*` - vsechny atributy prvku
    -   filtrovani uzlu
        -   `a[1]` `a[6]` index, `a[last()]` `a[@atribut]`
-   podminky
    -   `if` - parametr je XPath a kdyz je true pouzije se obsah elementu
    -   `choose` `when` `otherwise` - `switch` `case` `default`
    -   `message` - console log + atribut `terminate` - try/catch

## CSS - Kaskadove styly

-   CSS pro XML se lehce lisi od HTML
-   `<?xml-stylesheet href="path.css" type="text/css"?>` - pridani stylu k xml
-   nejvetsi rozdil - elementy v xml nemaji defaultni nastaveni
    -   musime pridat `display: block|list-item|table|table-cell` ..
    -   selektory
        -   dite `A > B`
        -   nasledujici sourozenec `A + B`
        -   atribut `A[attr]`, `A[attr=value]`, `A[attr~=value]` - vybere A ktere ma v atributu mimojine i `value` (pouziva se pro seznam v atibutu), `A[attr|=value]` - to same ale seznam je s pomlckou
        -   id `A#id`

## HTML - HyperText Markup Language

-   prvni definice v r. 1991 jako soucast projektu WWW - pro sdileni vysledku vyzkumu po celeme svete
-   pozdavky uzivatelu WWW vzrustaly - obohacovani HTML (formulare apod)
-   1995 vznikl navrh HTML 3.0 - zahrnoval napr matematicke vzorce, lepsi obtekani obrazku, styly dokumentu
-   poc. 1996 bylo jasne, ze HTML 3.0 byl vetsi skok a prohlizece nedokazali naimplementovat -> W3C se dohodlo, ze udelaji jen neco
-   1997 HTML 4.0 - na dlouhou dobu bez zmeny (jen opravy)
-   2000 specifikace XHTML 1.0 - odvozene od XML (ne od SGML jako HTML) - v zasade jen kosmeticka zmena - navic IE neumel zpracovat - nepouzivalo se
-   2007 spojeni W3C a WHATWG - zacalo vznikat HTML5
    -   pridava nove veci, podporuje syntaxi XML i XHTML
    -   moznost cachovani obsahu pomoci atr u html `manifest`
-   koncovky `.htm` nebo `.html`
-   drive se v nem definoval i styl, ale dnes uz vedle pomoci CSS
-   prvni tag `<!DOCTYPE html>` - definuje ze je to html
-   `<html></html>` - root tag, atr: `lang`, `manifest`
    -   `<head></head>`
        -   meta informace (kodovani, informace pro vyhledavace, og)
        -   titulek
        -   `base` - relativni slozka
        -   `link` - styly, favicon, fonty
        -   `style` - styly primo v html
        -   `script` a `noscript`
    -   `<body></body>` - samotny obsah

### Body

-   drive skladano z tabulek nebo ramcu (iframe), pozdeji z divu
-   dnes v HTML5 pomoci semantickych tagu (zachovana header, footer)
    -   `header`, `hgroup` - hlavicka stranky
    -   `nav` - navigacni prvky
    -   `footer` - paticka
    -   `section` - telo dokumentu
    -   `article` - samotne informace/clanky
    -   `aside` - postranni panel
    -   `main` - mel by byt pouze jeden
    -   dalsi: `time`, `summary` a `details`, `figure` a `figcaption`
    -   standartni: `p`, `h1-h6`,
        -   frazove (vyskytuji se casto v `<p>` - vyznam textu): `em`, `abbr`, `pre` a `code`, `strong`
        -   prezencni: `b`, `i`, `u`, `s`, `mark`
            -   ve starsim HTML ke stylovani textu, v HTML5 jim byl zmenen vyznam -> doporuceno nepouzivat
    -   seznamy:
        -   `ul`, `ol` a `li`
        -   `dl`, `dt` a `dd`
        -   `datalist` - autocomplete u elementu
        -   `table`, `tr`, `td`, `thead`, `tbody`, `tfoot`, `caption`, `colgroup` (seskupuje casti pro stylovani tabulky)
    -   formulare:
        -   `form` - method, action ..
        -   `input` - autocomplete, disabled ..
            -   type: text, password, file, date, time, range, checkbox, radio, hidden...
        -   `label` - for

### CSS pro HTML

-   styl, zarovnani, pro ruzne typy (tisk, mobily), animace ...
-   oddeleni vzhledu od obsahu, lepsi moznosti formatovani, snazsi sprava, rychlejsi nacitani
-   historie
    -   CSS1 - malo veci, mala podpora prohlizecu
    -   CSS2 - nove veci (max/min wh, curosor, position..), prohlizece implementovali casti
    -   CSS3 - rozdelene na moduly, vetsi podpora prohlizecu
-   vyhody
    -   jedn. udrzba web. stranek - oddeleni 2 jazyku
    -   cachovani stylu
    -   lze dyn. menit pomoci js
-   nevyhody
    -   podpora prohlizecu
    -   defaultni nastaveni stylu prohlizece (css reset atd)
    -   nepodporuje promene a dalsi konstrukty (resi SASS, LESS)
-   lze pridat inline - `style`, v html `<style>` nebo pomoci `link` z ext souboru
-   selektory `*`, `tag`, `.class`, `#id`, `:hover`,
-   jednotky:
    -   absolutni: px, cm, mm, in ..
    -   relativni: em (velikost pisma rod. elementu - meni se), rem (velikost pisma v dok.), vw, vh (viewport units), % (procenta z rod. ele.)
-   barvy
    -   nazvy, HEX, RGB, RGBA (alfa kanal 0-1), HSL (hue - 0-360, saturation - 0-100%, lightness - 0-100%), HSLA (alfa)
-   font: family, size, weight
-   text: align, decoration, transform, style

## JavaScript - XML a JSON

### XML

-   DOM XML (Document Object Model) - definuje metody pro pristup k XML
-   XML je vsak nutne nacist do objektu DOM XML
-   soucasne prohlizece maji vestaveni parser XML
-   HTML DOM a XML DOM - stromova struktura pro pristup a manipulaci k dokumentum
-   XML DOM
    -   `new DOMParser()` - nactu xml, dokazu po se po nem pohybovat
    -   podobne metody jako HTML DOM: `getElementsByTagName`, `appendChild`..
    -   podobne vlastnosti jako HTML DOM: `nodeName`, `parentNode`..
-   nacitani ze serveru pomoci `XMLHttpRequest`
    -   nacitani dat bez nutnosti refreshe stranky
    -   odesilani dat

### JSON

-   JavaScript Object Notation
-   format pro vymenu dat
-   nezavisly na jazyce
-   key:value, oddelena carkami
    -   podpora: string, number, null, array, object
-   `JSON.parse` a `JSON.stringify`
-   rozdily
    -   stejny jako XML: je samopopisujici se, hierarchicky, impl. v mnoha jazycich, lze nacist JS
    -   neni stejny jako XML: nepouziva konc. znacku, je kratsi, ma pole
-   nejvetsi rozdil:
    -   XML musi byt analyzovano parserem, JSON lze analyzovat funkci v JS
    -   diky tomu lze XML validovat predem, JSON az po parsovani (JSON Schema)
