# Otazky

## 1 Jakými právnímy předpisy je v ČR ošetřena problematika počítačové kriminality?

-   [Zákon č. 40/2009 Sb. trestní zákoník](https://www.zakonyprolidi.cz/cs/2009-40)
    -   [§ 230 Neoprávněný přístup k počítačovému systému a nosiči informací](https://www.zakonyprolidi.cz/cs/2009-40#f3920237)
    -   [§ 231 Opatření a přechovávání přístupového zařízení a hesla k počítačovému systému a jiných takových dat](https://www.zakonyprolidi.cz/cs/2009-40#f3920258)
    -   [§ 232 Poškození záznamu v počítačovém systému a na nosiči informací a zásah do vybavení počítače z nedbalosti](https://www.zakonyprolidi.cz/cs/2009-40#f3920268)
    -   [§ 182 Porušení tajemství dopravovaných zpráv](https://www.zakonyprolidi.cz/cs/2009-40#f3919713)
-   dalsi - upresnujici
    -   Občanský zákoník (40/1964)
    -   Obchodní zákoník (513/1991)
    -   Telekomunikační zákon (513/1991)
    -   Autorský zákon (121/2000)
    -   Zákony na ochranu průmyslového vlastnictví (14/1993)
    -   Zákon o ochraně osobních údajů (101/2000)
    -   Zákon o regulaci reklamy (40/1995)
    -   Zákon o elektronickém podpisu (227/2000)
    -   Zákon o svobodném přístupu k informacím (106/1999)
    -   Zákon o informačních systémech veřejné správy (365/2000)
    -   Zákon č. 181/2014 Sb., o kybernetické bezpečnosti (základní věci ohledně kyberbezpečnosti práva a povinnosti)
-   mezinarodni
    -   Úmluva o počítačové kriminalitě (slouží jako vodítko pro státy při vytváření národní legislativy proti kyberkriminalitě a jako základ pro mezinárodní spolupráci mezi členskými zeměmi této Úmluvy).

## 2 Čeho se týká paragraf 230 zákona č. 40?

-   [§ 230 Neoprávněný přístup k počítačovému systému a nosiči informací](https://www.zakonyprolidi.cz/cs/2009-40#f3920237)
-   prekonani bezpecnostniho opratreni a tim neopravneni ziskani pristupu k poc. sys. nebo jeho cast - _az 2 roky, zakaz cinosti nebo propadnuti veci_
-   pristup k poc. sys nebo nosici inf.
    -   neoprav. pouzije data
    -   data znici, poskodi, zmeni atd.
    -   data padela, zmeni tak aby byla povazovana za prava
    -   neopravnene vlozi data do poc. sys. nebo jinak zasahne do programoveho nebo techn. vybaveni pocitace nebo zarizeni
    -   _az 3 roky, zakaz cinosti nebo propadnuti veci_
-   veci z predchoziho v umyslu zpusobit skodu nebo jinou ujmu nebo ziskat prospech, umyslne neopravnene omezeni funkcnosti - _6mes az roky, zakaz cinosti nebo propadnuti veci_
-   veci z predchozich jako clen organizovane skupiny, zpusobeni znacne skody, zavazna porucha v cinnosti organu stat. spr., uzemni samospr. nebo jineho organu, ziska cinem znacny prospech, zpusobi cinem vaznou poruchu pravnicke nebo fyz. osoboy, ktera podnika - _1 az 5 roky nebo penezity trest_
-   veci z predchich zpusobi skodu velkeho rozsahu, ziska prospech velkeho rozsahu - _3 az 8 let_

## 3 Čeho se týká paragraf 231 zákona č. 40?

-   [§ 231 Opatření a přechovávání přístupového zařízení a hesla k počítačovému systému a jiných takových dat](https://www.zakonyprolidi.cz/cs/2009-40#f3920258)
-   umyslne spachany trestny cin poruseni tajemstvi dopravovanych zprav podle § 182 (b, c) nebo trestny cin podle § 230 nekomu proda, vyveze nebo prechovava atd. - _az 2 roky, propadnuti veci nebo zakaz cinnosti_
    -   zarizeni, postup, nastroj, program prizpusobeny k neopravnenemu pristupu do site, pocitaci nebo systemu
    -   heslo, pristupovy kod, data, postup nebo podobny prostredek pomoci nehoz ziska pristup
-   predchozi bod jako clen organizovane skupiny nebo ziska znacny prospech - _az 3 roky, propadnuti veci nebo zakaz cinnosti_
-   predchozi bod ziska velky prospech - _6 mes az 3 roky_

## 4 Čeho se týká paragraf 232 zákona č. 40?

-   [§ 232 Poškození záznamu v počítačovém systému a na nosiči informací a zásah do vybavení počítače z nedbalosti](https://www.zakonyprolidi.cz/cs/2009-40#f3920268)
-   v hrube nedbalosti porusenim povinnosti vyplyvajici ze zamestnani, povolani nebo funkce ze zakona nebo smlouvy a tim zpusobi cizimu majetku skodu - _az 6 mes, propadnuti veci nebo zakaz cinnosti_
    -   data znici, poskodi, pozmeni
    -   zasahne do technickeho nebo progr. vybaveni pocitace nebo systemu pro zpracovani dat
-   predchozi bod ve velkem rozsahu - _az 2 roky, propadnuti veci nebo zakaz cinnosti_

## 5 Čeho se týká paragraf 181 zákona č. 40?

-   ## [Zákon č. 181/2014 Sb. Zákon o kybernetické bezpečnosti a o změně souvisejících zákonů (zákon o kybernetické bezpečnosti)](https://www.zakonyprolidi.cz/cs/2014-181?text=kyberneticke+bezpecnosti)

-   [§ 182 Porušení tajemství dopravovaných zpráv](https://www.zakonyprolidi.cz/cs/2009-40#f3919713)
-   umyslne poruseni tajemstvi - _az 2 roky nebo zakaz cinnosti_
    -   pisemnosti pri poskytovani postovni sluzby nebo prepravy
    -   datove, textove, hlasove, zvukove, obrazove zpravy posilane elektronicky a priraditelne identifikovanemu ucastnikovi nebo uzivateli (kdo prijima)
    -   neverejneho prenostu pocitacovych dat ze systemu, do systemu nebo v jeho ramci, vcetne elektromagnetickeho vyzarovani systemu, prenasejici takova data
-   umysl zpusobit jinemu skodu nebo opatrit si neopravneni obsah - _az 2 roky nebo zakaz cinnosti_
    -   vyzrazeni tajemstvi z pisemnosti, telegramu, hovoru nebo prenosu pomoci site elektr. komunikace, ktery nebyl urcen jemu
    -   vyuziti takoveho tajemstvi
-   _6 mes az 3 roky nebo zakaz cinnosti_
    -   predchozi jako clen organizovane skupiny
    -   spacha ze zavrzenihodne pohnutky
    -   zpusobi znacnou skodu
    -   spacha v umyslu ziskat znacny prospech
-   _1 az 5 let nebo penezni trest_
    -   spacha prechozi jako uredni osoba
    -   zpusobi skodu velkeho rozsahu
    -   spacha cin v umyslu ziskat velky prospech
-   zamestnanec provozovatele postovnich sluzeb, telekomunikacnich sluzeb nebo poc. systemu - _1 az 5 let nebo zakaz cinnosti nebo penezni trest_
    -   spacha cin z predchozich bodu
    -   jinemu umozni spachat tyto ciny
    -   pozmeni nebo potlaci pisemnost obsazenou v zasilce nebo dopravovanou
-   _az 3 roky_
    -   predchozi bod ve velkem rozsahu
    -   spacha cin v umyslu ziskat prospech velkeho rozsahu

## 6 Uveďte příklady doplňující legislativy k zákonu č. 40.

-   Občanský zákoník (40/1964)
-   Obchodní zákoník (513/1991)
-   Telekomunikační zákon (513/1991)
-   Autorský zákon (121/2000)
-   Zákony na ochranu průmyslového vlastnictví (14/1993)
-   Zákon o ochraně osobních údajů (101/2000)
-   Zákon o regulaci reklamy (40/1995)
-   Zákon o elektronickém podpisu (227/2000)
-   Zákon o svobodném přístupu k informacím (106/1999)
-   Zákon o informačních systémech veřejné správy (365/2000)
-   Zákon č. 181/2014 Sb., o kybernetické bezpečnosti (základní věci ohledně kyberbezpečnosti práva a povinnosti)
-   Úmluva o počítačové kriminalitě (slouží jako vodítko pro státy při vytváření národní legislativy proti kyberkriminalitě a jako základ pro mezinárodní spolupráci mezi členskými zeměmi této Úmluvy).

## 7 Co to je počítačový systém?

-   [40/2007 Sb. o informacnich systemech verejne spravy](https://www.zakonyprolidi.cz/cs/2007-40#p2-1-b)
    > informačním systémem funkční celek nebo jeho část zabezpečující cílevědomou a systematickou informační činnost. Každý informační systém zahrnuje data, která jsou uspořádána tak, aby bylo možné jejich zpracování a zpřístupnění, a dále nástroje umožňující výkon informačních činností;
-   wiki:
    > Jako počítačový systém může být chápán samostatný počítač i rozsáhlá počítačová síť zahrnující množství počítačů, terminálů a dalších prvků. Spojení „počítačový systém“ také vyjadřuje, že k jeho provozu je kromě technického vybavení nezbytné i programové vybavení.
-   dalsi
    > Jakékoli zařízení, nebo skupina vzájemně propojených nebo souvisejících zařízení, z nichž jedno nebo více provádí na základě programu automatické zpracování dat.

## 8 Co to jsou provozní data?

Jakákoli poćítačová data související s přenosem dat prostřednictvím počítačového systému, generovaná počítačovým systémem, který tvořil součást komunikačního řetězce, jež vyjadřují původ, cíl, trasu, dobu, objem, dobu trvání přenosu dat, nebo druh použité služby.

## 9 Kdo to je poskytovatel služeb?

-   jakýkoli veřejný, nebo soukromý subjekt, který poskytuje uživatelům služby možnost komunikovat prostřednictvím počítačového systému
-   jakýkoli jiný subjekt, který zpracovává, nebo ukládá počítačová data pro tuto komunikační službu, nebo uživatele této služby

## 10 Uveďte některé trestné činy proti důvěrnosti, integritě a dostupnosti počítačových dat a systémů.

-   Neoprávněný přístup
-   Neoprávněné zachycení informací
-   Zásah do dat
-   Zásah do systému
-   Zneužití zařízení

## 11 Uveďte trestné činy související s obsahem.

-   Dětská pornografie
-   Podporování násilí, fašismu, atd.

## 12 Uveďte příklady trestné činnosti související s informačnímy systémy.

-   Šíření pornografie s extrémním obsahem
-   Šíření dětské pornografie
-   Vydírání
-   Padělání peněz a listin
-   Šíření poplašné zprávy
-   Neoprávněné zacházení s osobními údaji
-   Neoprávněná manipulace s PC systémem

## 13 Uveďte metody používané při trestné činnosti související s informačnímy systémy.

-   neopravneny pristup k serveru
-   neopravneni pristup ke klientovi (osobni PC, tablet, mobil)
-   odposlech a modifikace prenasenych dat
-   socialni inzenyrstvi
-   fyzicky utok

## 14 Uveďte základní zdroje informací a nástroje použitelné při testování bezpečnosti WEB serverů a aplikací.

-   zdroje
    -   pentest-standard - navody a postupy pro testovani, sber informaci, modelovani utoku, analyza zranitelnosti, vyuzitelnosti exploitu, technicke navody na konkretni systemy a nastroje
    -   OSSTM - Open Source Security Testing Methodology Manual
        -   metoda pro vypocet zranitelnosti - benchmark pro porovnani zabezbeceni
    -   OWASP - Open Web Application Security Project
        -   bezpecnost webu a webovych aplikaci
        -   testing a development guide
    -   CVE - Common Vulnerabilities and Exposures - db zranitelnosti a znamych chyb
    -   Google Hacking Database
-   nastroje
    -   nmap, metasploit, OpenVAS, Kali Linux (a nainstalovane aplikace), burp, wireshark, nc atd.

## 15 Uveďte metody získávání informací o WEB serveru, aplikaci.

-   web server
    -   google hacking (muzu najit hostname firmy, login stranky, indexovane adresare atd.)
    -   robots.txt, site mapy
    -   http hlavicky (`X-Powered-By: Wordpress`, `Server: Apache`), jejich poradi muze take odhalit server
    -   `nc`, `nmap`
    -   nekorektni dotaz - genericka 404 apod.
    -   analyza komunikace pres lokalni proxy
    -   sitereport - aplikace shromazdujici informace o sluzbach
-   aplikace
    -   domeny (DNS zaznamy - CNAME, A, PTR)
    -   fierce - slovnik ktery zkousi domeny
    -   google - indexovane stranky
    -   typicky adresare (google, slovnik)
    -   scan portu
    -   query parametry `?application=prf`
    -   analyza struktury - crawling stranky (OWASP ZAP, Burp proxy)

## 16 Uveďte nedostatky konfigurace WEB serveru a aplikace.

-   SSL/TLS
    -   nepouzivani sifrovani
    -   SSL zastarale, dnes TLS >=1.2
    -   pouziti slabe sifry
-   HSTS
    -   predchazeni utokum MIM - downgrade z https na http
-   management serveru
    -   aktualizace (zname chyby)
    -   nastaveni generickych stranek (404 atd)
-   management aplikace
    -   odstraneni komentaru, souboru s napovedou, priklady, manualy atd
    -   spravne nastaven directory traversal
    -   nenechavat stare backupy, tmp slozy atd
    -   spravne nastaveny pristupy (admin rozhrani, porty)
-   spravne nastavene HTTP dotazy - nektere metody jdou zneuzit (XST - cross site tracing)

## 17 Uveďte základní útoky na autentizační mechanizmy WEB aplikací.

-   hadani jmen hesla (slovnik, brute force, socialni inzenyrstvi)
    -   implicitni hesla (routery, televize atd)
-   zneuziti obnovy hesla (testovani existujichc uctu)
-   zapomenute heslo v repozitari, kodu, backupu, logach atd
-   obejiti autentizacnich mechanizmu
    -   primy pristup na zabezpecenou url, poslani dotazu na API bez prihlaseni
    -   utok na session id / id relace

## 18 Uveďte způsoby, kterými je možné přenášet autentizační údaje.

-   sifrovane (https) / nesifrovane (http)
-   GET (query parametry) / POST (body)

## 19 Co to je Identifikátor relace (SessionID) a k čemu slouží.

-   vetsinou hash, ktery je po prihlaseni k webove aplikaci vygenerovan serverem a ulozen na klientovi v cookies
-   slouzi k tomu, aby se uzivatel nemusel prihlasovat s kazdym requestem, ale staci do hlavicek pribalit prave session id z cookie

## 20 Jaké vlastnosti by měl mít bezpečný Identifikátro relace?

-   nahodny, dostatecne dlouhy, unikatni, sifrovane ulozene
-   generovan az po prihlaseni, smazan pri odhlaseni, mel by mit konecnou platnost

## 21 Uveďte příklady útoků na autentizační mechanizmy.

viz 17

## 22 Uveďte příklady útoků na Identifikátor relace.

-   predictable session id / token
-   session sniffing
    -   ziskani uzivatelovo session id, podstrceni (pomoci query stringu)
-   session fixation - aplikace nezmeni session po prihlaseni (http)
-   XSS
-   MIM - odposlech
-   MIB - Man In the Browser
    -   napr. trojan mezi browserem a knihovnou (exe <-> dll)

## 23 Co to je autorizace a jak můžeme testovat její nedostatky?

-   rozdil
    -   autentizace - overeni identity uzivatele
    -   autorizace - overeni opravneni uzivatele na akci/pristup k datum
-   typy
    -   black box testing - o aplikaci nic nevime
    -   grey box testing - zname zdrojovy kod, server atd
-   nedostatky
    -   path traversal - `web.cz/../../../etc` (pouziti kodovani pro obchazeni filtru apod.)
    -   pristup primo na zabezpecenou url bez prihlaseni
    -   pristup primo na zabezpecenou url po odhlaseni
    -   pristup primo na zabezpecenou s uzivatelem s mensimi pravy
    -   podvrhnuti parametru `?level=admin`

## 24 Proč je důležitá kontrola vstupů WEB aplikace?

-   nedostatecne zabezpeceni muze vest k utokum na aplikaci
-   pravidelne v top 10 OWASP
-   all input is evil
    -   vstupum od uzivatele se neda duverovat

## 25 Vyjmenujte útoky, ke kterým lze zneužít nedostatečnou kontrolu vstupů aplikace.

-   XSS (reflected, stored), SQL injekce (XML, LDAP, IMAP), OS commanding, Buffer overflow, CSRF

## 26 Co to je XSS?

-   stored (ulozena napr. v db, cookies)
    -   pomoci neosetreneho vstupu utocnik ulozi skodlivy kod do db
    -   kod se pak zobrazi na strance pro ostatni uzivatele
    -   kod pak muze krast cookies, vyuzivat exploity prohlizece, obsah schranky, menit elementy na strance
-   reflected
    -   data se dostanou do prohlizece v url
    -   pomoci kodovani muze utocnik obejt filtr
    -   skodlivy kod pak muze ukrast cookie, obsah schranky, prekreslit stranku (zmeni odkazy, formulare atd)

## 27 Co to je SQL Injection?

-   pomoci neosetreneho vstupu (vyhledavani, login atd) utocnik muze zmeni provadeny SQL dotaz v databazi a dostat tak citliva data
-   _inband_ - ten samy kanal pro input i output
-   _outband_ - ruzne kanaly pro input (formular na strance) i output (prikaz databaze - DNS, HTTP)
-   _inferential_ - blind - time-based (podle casu napr zjistim jestli uzivatel existuje nebo ne)
-   priklad `SELECT ';-- ID == 1`

## 28 Co to je OS commanding?

-   injekce prikazu na system do neosetreneho vstupu
-   priklad (`; id`)

## 29 Co to je buffer overflow?

-   utocnik zada presne takovy vstup, aby se zapsal az za vyhrazenou pamet - tim prepise napr. kam se ma program vratit a zavola svou funkci
-   priklad: exploity v prohlizeci, kde se muze vykonat utok pomoci buffer overflow

## 30 Proč jsou pro útočníka zajímavé odposlechy dat na síti?

-   jmena, hesla, id relace
-   analyza aplikace pomoci jeji komunikace

## 31 Kde v síti je možné data odposlechnout?

-   kdekoliv na siti (routery, switche), prohlizec

## 32 Jak funguje odposlech na přepínači pomocí ARP spoofingu?

-   obrazek s ARP cachema
-   v cache uz resolvnuty preklady IP <-> MAC (arp -a)
-   utok spociva v tom, ze do cache se utocnik snazi podstrcit svou MAC napr.:
    -   posila falesny ARP requesty a spoleha na to, ze ostatni pocitace si ho ulozi do cache apod.
    -   posloucha ARP requesty a odpovida rychleji nez prijemce
-   muzeme utocit i na routery
-   ochrana pomoci kontroly duplicitnich adres, kontrola pri zmene ARP tabulky (detection and prevention), staticke zaznamy, sifrovana komunikac

## 33 Jaké jsou metody odposlechu dat v případě, že neputují přímo sítí útočníka?

-   passive sniffing
    -   odposlech toku dat na siti (nejcasteji Huby - rozesilaji requesty vsem zarizenim)
-   active sniffing
    -   manipulace s odposlechnutymi daty - routery + switche
    -   MAC Flooding
    -   DHCP Attacks
    -   DNS Poisoning
    -   Spoofing Attacks
    -   ARP Poisoning
-   RIP, spoofing (ruzne typy) - presmerovani na jiny web

## 34 Lze odposlechnout data zasílaná pomocí asymetrické kryptografie?

-   ano, SSL strip, podvrzeni verejnych klice, MIM utoky, slaba sifra, odposlouchavani na nizsich vrstvach (IP datagram)

## 35 Jaké existují metody obrany proti odposlechům na síti?

-   sifrovani (HTTPS everywhere), zabezpeceni pdvrzeni klice, neignorovat chyby v certifikatech, HSTS protokol, pristupovat rovnou na https, zakazani http, povolit certificate pinning atd

## 36 V čem spočívá problematika bezpečnosti WiFi sítí?

-   lehce muzu vytvorit podvodny AP (wifi pristup) a delat MIM
-   vyuziti exploitu routeru, slabe/implicitni heslo, stary protokol WPA, brute force na heslo
-   ruseni signalu
-   signal lze zachytit na 10ky km (parabolicka antena)

## 37 Co to je škodlivý kód a k čemu slouží?

-   cokoliv, co nechceme, aby uzivatel spusti (kod podstrceny utocnikem)
-   ruzne kategorie: vir, cerv, chobotnice, kralik, trojan, backdoor atd
-   malware - Malicious Software
    -   jakykoliv software ktery byl vytvoren za ucelem poskozeni, odposlouchani, sifrovani dat (za uplatu) atd

## 38 Jaké jsou základní metody ochrany proti škodlivému kódu?

-   firewall (spravne nastaveny, blokovani portu, prichozich spojeni atd), hw firewall
-   honeypot (napr. na turrisu), IDS (Intrusion Detection System) - detekce - wireshark, snort ,IPS (Intrusion Protection System) - detekce i blokace
-   antivirus (spravne nakonfigurovany), aktualizace (jak sw, tak hw - routery)
-   aktualizovat aplikace, overovani jejich puvodu (ruzne klice)
-   pouzivani sandboxu pro testovani neoverenych aplikaci, bootovani z flasky (os jede z pameti)
-   zalohovani dat
-   web pouzivat sifrovane

## 39 Jak můžeme škodlivý kód v síti detekovat?

-   honeypot, firewall (SW/HW), IDS, IPS
-   analyza provozu/traficu na siti

-   ne antivirem, ale v siti!

## 40 Jaký je rozdíl mezi virem a červem?

-   viry
    -   siri se jen interakci s uzivatelem (akce - pripojeni USB, otevreni prilohy mailu atd)
    -   automaticke spusteni (na starsich Win autorun), automaticke prehrani (autoplay)
-   cervi
    -   automaticke sireni
    -   ruzne moduly
        -   vyhledavac obeit - scanner, ktery hleda sluzby, pres ktere by se mohl sirit dat
        -   sireni infekce - typicky vyuziva nejaky exploit
        -   rohrani pro vzdaleny pristup (typicky se cerv nahlasi na nejaky server, ze ovladl pocitac)
        -   rozhrani pro aktualizaci - cerv aktualizuje databazi exploitu, obrane mechanizmy pred detekci atd
        -   funkcni casti - keylogger, scanner, DDoS utok
        -   sber statistik
        -   hodiny zivotni cyklu - jak dlouho ma byt aktivni

## 41 Co to je BotNet?

-   sit ovladnutych stroju, rizena z jednoho centra
-   vyuziva se ke spamu, DDoS utokum

## 42 Jaké stopy zanecháváme při práci s Internetem a kdo k nim má přístup?

-   IP adresy, MAC adresy (lokalni sit), porty
-   email adresy, jmena serveru pres ktere email prochazi
-   otisk prohlizece (metadata o systemu, prohlizeci)

## 43 Jaká jsou běžná opatření ke zvýšení soukromí na Internetu?

-   nastaveni prohlizece
    -   neukladat data z formularu, hesla
    -   zakazani flash pluginu, javy
    -   neodesilat skutecnou adresu ftp serverum
    -   neakceptovat cookies nebo je alespon analyzovat
    -   automaticky nepovolovat pouziti polohy, notifikaci, zapnuti senzoru (kamera, mikrofon, pohyb atd), blokovat redirecty, blokovani reklam, automaticke stahovani, pristup k zarizeni (USB, serial port), pristup ke schrance, HID zarizenim, platebni metodam
-   pouzivat anonymizujici prohlizec (firefox, brave vivaldi)
    -   odriznuti od googlu
    -   kontrola URL podle db, detekce nestandardniho chovani
    -   always HTTPS
-   analyza prichozich a odchozich dat (automaticke odesilani dat o pouzivani, bug reportu, kontrola aktualizaci)

## 44 Jak můžeme anonymně přistupovat k Internetu?

-   VPN, Tor, pseudoanomni pristup
-   Proxy servery - na server se pripojuju pres nekoho (takze podobne jako pseudoanonymni)

## 45 Jaký je rozdíl mezi anonymitou a pseudoanonymitou?

-   pseudoanonymita
    -   typicky free proxy servery a VPN
    -   musíme věřit poskytovatelům/službě, že nic nezveřejní
    -   všechny transakce jsou zaznamenány u poskytovatele
-   anonymita
    -   jako je například přes Tor síť, se klient připojuje přes síť serverů, kdy poskytovatel internetu na straně klienta není schopen zjistit s kým klient komunikuje a aby server nedovedl klienta identifikovat.
    -   nachylne na statisticke utoky
    -   Tor prohlizec, Tails OS

## 46 Co to je sociální inženýrství?

-   manipulace lidi za ucelem provedeni akce nebo ziskani informace
-   ruzne typy
    -   baiting - fyzicky predmet - rozhazu USB se skodlivym programem
    -   emaily
        -   phishing
        -   spearing
    -   falesny telefonni hovor - utocnik se vydava za banku, reditele atd

## 47 Co to je phishing?

-   rozesilani podvodnych emailu, ktere se tvari jako oficialni email od firmy/urady/banky
-   typicky odkazuji na stranku, ktera vypada jako original, ale zadane udaje odesila utocnikovi

## 48 Co to je spearing?

-   cileni emailu na jednotlivce
-   vim kdo to je a dokazu na nej presne zacilit a upravit email, tak aby mu veril
-   zase cili na stazeni emailu, odkaz na fake stranku atd

## 49 Jaké metody používáte k zabezpečení svého osobního počítače a jak byste mohli jeho zabezpečení zvýšit?

-   sifrovani disku
-   zaheslovani systemu
-   zapnuty a nakonfigurovany firewall
-   aktualizace OS, programu
-   privatni a nakonfigurovany prohlizec
    -   adblock, vynucene https, vypnute notifikace, poloha atd
-   tor pro specificke akce
-   spravce hesel
    -   silne master heslo
    -   jina random hesla k dalsim sluzbam
-   hw klic pro kriticke aplikace (HW Token, Trezor)
-   pozornost pri prochazeni webu, instalovani programu, pripojovani k sitim
-   zlepseni
    -   pouzivani sandboxu, VPN
    -   lepsi zabezpeceni domaci site
    -   lepsi zabezpeceni mobilu (antivir, VPN)
    -   pouzivani Have I Been Pawned na ucty

## 5O Jaká je základní metoda obrany proti sociálnímu inženýrství?

-   prevence - vzdelani
-   rozumne chovani s pocitacem a pri rozhovorech s cizimi lidmi
-   obezretnost na socialnich sitich a obecne s daty, ktera a komu sdilim
