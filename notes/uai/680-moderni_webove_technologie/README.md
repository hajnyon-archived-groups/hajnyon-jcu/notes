# 680 - Moderni webove technologie

## Uvod

### React

-   pouziti
    -   webovy appky
    -   desktop (electron)
    -   react native - mobilni appky
-   vznik cca 2011 v facebooku
-   technicky knihovna (ne framework - napr Vue, Angular)
    -   vetsinou se pouziva vice knihoven
-   z hlediska MVC - fe je V (ale muze mit celou vlastni architekturu)
-   prinasi zasadni zmenu paradigmatu - nepiseme kod ktery neco meni, ale ktery popisuje jak ma vypadat vysledek (DOM)
-   server sider vs browser
    -   server side - HTML a cela stranka se renderuje na serveru
    -   browser - nacte se appka - ta si pak saha pro data (http requesty)
-   DOM - document object model
    -   pomala prace s nim (js rychly)
    -   standardizace napric prohlizeci
    -   je to vlastne globalni promenna
    -   react ma virtualni kopii, kterou upravuje a pak prekresluje DOM (jen casti ktere se zmenili)
    -   -> Virtualni DOM
-   komponenty
    -   rozdeleni stranky na casti
    -   root
        -   pod nim dalsi casti aplikace (nav, header, todolist..)

### Inicializace

-   cdn
    -   react - development/production verze
    -   react-dom - interakce s DOMem
    -   babel - preklad (kompilace) do js (`<script type="text/babel">`) - jsx
-   cli
    -   `create-react-app`

### JSX

-   html in js (JavaScript XML)
-   prekladan do js (pro prohlizece)
    -   jsx -> babel -> js
    -   meni funkce do ES6
-   `render` - zobrazeni vystupu do DOMu
-   pouziva se camelcase u html atributu - pr.: `class` => `className`

```jsx
function formatName(user) {
    return `${user.firstName} ${user.lastName}`;
}

const user = {
    firstName: 'Andrew',
    lastName: 'Gamekeeper'
};

const element = <h1 className="greeting">Hello from React, {formatName(user)}</h1>;

ReactDOM.render(element, document.getElementById('root'));
```
