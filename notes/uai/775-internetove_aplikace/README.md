# 775 - Internetove aplikace

## Uvod

-   pristupy k navrhu sitovych aplikaci
    -   klient
        -   uzivateluv kontaktni bod
        -   PC, laptop ..
        -   typicky GUI (zadavani pozadavku, format vystupu ..)
    -   server
        -   vykonny, slouzi klientovi
        -   casto pristupu k dalsim sluzbam (db, mail ..)
        -   musi byt trvale dostupny
-   internetove aplikace
    -   komponenty tvorici internet (DNS, ..)
    -   nastroje pripojujici uzivatele k intern. zdrojum (prohlizece, applety, CGI)
    -   sluzby dostupne pres internet (ecommerce, banking, zpravodajstvi ...)
    -   aplikace bezici nad kolekci zdroju dostupnych pres inter. (SETI@home)
    -   nastroje tvorici nova prostredi nad internetem - middleware service
-   type
    -   pristup
        -   centralizovane
        -   decentralizovane
    -   funkce
        -   aplikace
        -   middleware service
-   zdroje na internetu
    -   otazka duvery a spolehlivosti
    -   nespolihliva kom. - mechanizmy pro preklenuti
    -   nespolehlive zdroje - fault-tolerant pristup
    -   velmi heterogenni prostredi
        -   synchronni pristup - svazujici
        -   asynchronni - resi i casove prodlevy
    -   potencialne velke mnozstvi zdroju - skalovani
-   design
    -   decentralizovana reseni jsou budoucnost
    -   problemy
        -   distr. management
        -   odolnost proti chybam a vypadkum
        -   detekce vypadku
        -   komunikacni naklady
        -   bezpecnost
    -   v praxi i centralizovane postupy
-   middleware
    -   vypocetni site - HW a SW infrastruktura
    -   odpovedne za vykon nadstavbovych aplikaci
        -   informacni sluzby
        -   vyhledavaci sluzby
        -   management zdroju
        -   bezpecnost
        -   detekce problemu a zotaveni z nich
-   pozadavyk na vyhledavaci sluzby
    -   skalovani
    -   spolehlivost
    -   flexibilita
    -   efektivita
    -   presnost, bezpecnost
    -   nezavislost na globalnich hierarchiich

## Zakladni webove technologie

-   www
    -   hypertext
    -   GUI
    -   bezpecnost - ssl, zakaz pristupu k lokalnim zdrojum ..
    -   distribuovanost
    -   cena sw
    -   interaktivita - personalizace
    -   jednoduche a otevrene standardy (HTML, HTTP ..)
    -   rozsiritelnost
-   klienti
    -   prohlizece
        -   vystup je tvoren klientem
        -   og: Mosaic, Netscape, IE
        -   current: Chrome (a odnoze), Firefox, Opera
    -   jine systemy
        -   vyuziti casti www jako middlewaru
        -   ruzne formy: http, html, json, rest api
        -   preklenuti platformni zavislost, rozsireni v komunite
-   servery
    -   unix: apache, nginx
    -   windows: apache, ms iis
    -   aplikacni servery: tomcat, glassfish
    -   novejsi: nodejs, go, rust ..
    -   dneska vede nginx, pak apache, ..

### HTTP

-   1.1
    -   nad TCP, textovy prenos, v zakl. nesifrovany, dlouho hlavicky
    -   TLS
-   2.0
    -   z google SPDY
    -   komprese hlavicek, server push (vice odpovedi na jeden dotaz - docitani js, css bez zadosti)
    -   povinne TLS, dobra podpora prohlizecu, multiplexovani (vice pozadavku soucasne), hodne malych souboru je ok
-   3.0
    -   google QUIC pres UDP
    -   tenky zaklad nezarucuje temer nic, nahrada TCP - efektivnejsi
    -   TLS 1.3

### Obsah WWW

-   HTML
    -   zakladni jazyk, soucasne v5.3, standardy
    -   drive varianta XHTML
    -   standardy jsou de facto jen doporuceni
-   dalsi standardy: MathML, SVG, WebGL
-   RIA - rich internet applications
-   DOM - document object model
-   CSS - v1,2,3 (modularni sklatba, dalsi selectory, transformace, animace ..)
    -   less - preprocesor, promenne, extend, mixin, cykly, ..

## Javascript

-   puvodne klasicky model: klient -> www server -> app server -> db -> a zpet
-   programovani pro www
    -   snaha rozsireni www
    -   DOM, SPA -> js
    -   prenos bin. kodu -> java, activex - MS (virtualti stroj u klienta, applet)
    -   presun zateze na klienta
-   cookies - vyjimka o nezasahovani do local uloziste
    -   moznost ulozeni informace ze serveru u klienta
-   VBScript (MS), JavaScript (Netscape)
-   puvodne odlehcena verze javy - podobna syntaxe
-   objektovy - DOM
-   JSON
    -   standard od 2009
    -   `JSON.parse()`, `JSON.stringify()`
    -   podpora v jinych jazycich
-   jQuery
    -   zjednoduseni rutinnich funkci a manipulace s DOMem

## HTML 5

-   posun od 4
-   reakce na aktualni potreby - RIA, tenky klient
-   offline podpora (manifest), nove prvky (geolokace, inputy, canvas, web workers, storage)

## Komunikace klient - server

-   ucel - vymena dat mezi klien - server
-   cile - rychlost, plynulost
-   rada technickych variant
    -   TCP
    -   UDP
    -   vyssi protokoly: http, websockets, webrtc
-   http - GET, POST, PUT, DELETE, HEAD, PATCH, OPTIONS
-   kriteria: min. objem dat, komprese, prenaset jen co je zrovna potreba (ostatni pozdeji)
-   AJAX - Asynchronous JS and XML
-   websockets
    -   plne duplexni obousmerna komunikace pres jeden TCP socket
    -   real time two-way communication
    -   `ws://localhost:3000`
-   WebRTC
    -   online peer-to-peer prenost mezi prohlizeci (media, data), streaming
    -   ICE (Interactive Connectivity Establishment), SDP (Session Description Protocol)
    -   spojeni navazovano pres server
    -   STUN server - udrzovani IP adres klientu
    -   TURN server - zprostredkovani komunikace pokud nelze primo mezi klienty
    -   podpora API v prohlizecich: getUserMedia, MediaRecorder, RTCPeerConnection, RTCDataChannel

### NodeRed

-   flow based programming
-   nekolik typu bloku, ktere navzajem propojuji
-   datove toky: `msg` -> `payload`, `topic`

## Programovani na strane serveru

-   moznost vyuziti prostredku server (disk, procesor)
-   vazba na dalsi sluzby - db, mail ..
-   nastroje
    -   CGI - Common Gateway Interface
    -   ASP, PHP, JSP (Java), Nodejs
    -   java servkets, server pages
-   MVC - Mode View Controller
-   Nodejs
    -   jednovlaknovy beh, asynchronni komunikacni model
    -   callack funkce - udalosti
    -   zakladem interpret V8 z chromu
    -   npm - moznost sdileni kodu client/server
-   API - Application Programming Interface
    -   zajisteni komunikace mezi systemy - vymena dat
    -   OpenAPI - obecny standard pro popis API pomoci JSON nebo YAML (swagger)
    -   RESTful API - REpresentational State Transfer
        -   na http protokolum omezeny pocet metod
-   web services - uziti webovych standardu pro komunikaci mezi IS
    -   HTTP, XML (SOAP), XML, NS, Schema - WSDL
    -   RPC - vzdalene volani procedur
    -   SOA - Service Oriented Architecture
    -   SOAP - XML pres POST

## Mikrosluzby a provoz internetovych aplikaci

-   vyvoj monolitickych aplikaci -> rozdeleni na specializovane komponenty
-   komunikace mezi sebou pomoci API
-   vyhody: nezavisly vyvoj casti, jednoduche horizontalni skalovani, mensi tym
-   uziti
    -   zakladem je jasne definovana a omezena funkcionalita nabizena pres API
-   Docker
    -   nastroj pro virtualizaci
    -   kontejner - odlheceny virtualni stroj
        -   vlastni os, sw, zdroje (ram, disk ..)
        -   vnejsi interface - porty
    -   kompletni oddeleni sluzeb na jednom stroji
    -   image - charakterizuje sluzbu a sw nutny pro beh kontejneru
        -   data mimo
    -   volume - disk pro data, oddeleny od hostitelskeho pocitace
    -   site - moznost vlastni site pro kontejnery
    -   stack - vice kontejneru - docker-compose
    -   swarm - vicenasobne spusteni kontejneru (skalovani dle zatizeni)
    -   Dockerfile - vlastni image
-   Kubernetes
    -   orchestracni nastroj
    -   zakladem kontejnerova reseni - docker, rocket
    -   pod - nejmensi nasaditelna jednotka
    -   label - par klic-hodnota - identifikace podu
    -   replica set - zajistuje beh podu na vice strojich soucasne
    -   service - logicke set podu zajistujici chod aplikace
    -   volume - ukladani dat kontejneru
    -   node - fyzicky/virtualni stroj spravovany k8s
    -   cluster - mnozina nodu

## Sitove aplikace a IoT

-   IoT
    -   cela skala zarizeni, typicky mala pamet, vykon, omezena konektivita, ale velmi dobra komunikace se senzory a dalsimi prvky, mala spotreba
    -   arduino
        -   embedded systemy - pocitac vlozeny do vetsiho funkcniho celku
        -   jednoduche programovani, ekosystem
        -   dve procedury `setup()` a `loop()`
    -   Raspberry Pi
        -   Raspberry OS nebo jine distro
        -   arm
    -   ESP32
        -   nekolik pouziti - server, wifi scan ..
        -   cidlo teploty a hallova sonda, diody
        -   micropython

## Identity management

-   IdM system
    -   sada sluzeb a pravidel pro spravu uzivatelu v organizaci
        -   info o osobach, zarizenich, skupinach, rolich
        -   autentifikace a autorizace
        -   jednotny pristup
    -   bezpecnostni komponenta
-   identita
    -   jednoznacna identifikace daneho objektu
        -   osoba, uzivatel, ale take zarizeni a nekdy i sluzby
    -   shlukovani do skupin
-   rizeni
    -   uzivatelsky zivotni cyklus
        -   CUD (create, update, delete)
        -   vazby - skupiny, role
            -   casto casove omezene
        -   politiky - hesla, prava
            -   obnova hesel, definice typu prav - cteni, zapis, listing ..
    -   zivotni cyklus zarizeni a dalsich entit
        -   CUD, klice, politiky
-   typy reseni
    -   centralizovani, distribuovane - obvykle kombinace
-   odpovednosti IdM serveru
    -   autentizace uzivatelu a sluzeb
        -   hesla, SSO, 2FA, certifikaty, klice
    -   autorizace - pravidla pro sluzby a uzivatele
    -   sprava souvisejici se sitovanim - DNS, DHCP ..
    -   audit, reporting
-   odpovednosti klienta IdM
    -   nacitani informaci, autentizace, autorizace
-   nastroje IdM
    -   stejne dulezite jako technologie
    -   bezpecnost a slozitost stoji proti sobe
    -   diagnosticke nastroje
-   windows - Active Directory
-   linux - LDAP (Lightweight Directory Access Protocol)

### Role Based Access Control

-   drive prima vazba uzivatel <-> objekt - nesystemove
-   dnes prava uzivatelu prirazena pomoci roli
-   uzivatel, role, pravo, objekt, operace, session
-   hierarchie roli
