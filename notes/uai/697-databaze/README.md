# 697 - Databaze

## Zakladni pojmy

### Data

-   surova nezpracovana fakta
-   informace - data ktera prosla zpracovanim nebo dostala urcitou strukturu

### Databaze

-   soubor dat
-   ruzne formy - papirove, elektronicke (napr. tabulky, soubory)
-   relacni, grafove, souborove

#### Vlastnosti databaze

-   reprezentuje nejake aspekty realneho sveta
-   predstavuje logicky spjaty souhrn dat s danym vyznamem
-   je navrzena, implementovana a spravovana s urcitym zamerem, s daty urcenymi ke specifickym ucelum, ma skupinu urcitych uzivatelu a specificke operace

### SŘBD - System rizeni baze dat

-   software umoznujici interakci mezi uzivatelem, db aplikaci a samotnou databazi
-   **definice databaze** - specifikace datovych typu, struktur a podminek omezujici data (integritni omezeni)
-   **konstrukce databaze** - proces ukladani samotnych dat na vhodne nosice, ktere jsou rizeny SRBD
-   **sprava db, pristup, manipulace s daty** - zahrnuje dotazovaci a vyhledavaci funkce, aktualizacni operace, generaci vystupu

**Databazovy system** = **databaze** + **SRBD**

![DB system](./db_system.png)

-   SRBD poskytuje uzivateli tzv. **konceptualni reprezentaci dat - konceptualni schema**
    -   formalizovani popisuje danou aplikaci (realny svet) pro:
        -   chapani objektu uzivateli, projektanty
        -   integraci uzivatelskych pohledu a implementaci
        -   zobrazni mezi pohledy a fyzickym ulozenim dat

### Tradicni zpracovani dat

-   zavislost programu a dat
-   redundance (nadbytecnost) a nekonzistence dat

### Databazove technologie

-   program je datove a operacne nezavisly
-   vicenasobne pohledy na data
-   sdileni data a provadeni viceuzivatelskych transakci

**datova abstrakce** = _nezavislost program <-> data_ + _nezavislost program <-> operace_

### Role

-   spravce db, navrhar db, systemovy analytik, aplikacni programator
-   navrhar SRBD, nastroju
-   uzivatele

## Konceptualni modely dat

-   navrh databaze
    -   pozadavky zakaznika a analyza
    -   konceptualni navrh databaze
    -   logicky navrh db - mapovat datoveho modelu
    -   fyzicky navrh databaze
-   druhy - ER, sitove, hierarchicke, OO

### Entitne relacni model

-   pojmy, kterymi se vyjadruje struktura databaze
-   **model** - zjednoduseny obraz sveta
-   tvori db architekt

#### Entita

-   mnozina objektu se stejnymi vlastnostmi, nezavisla na ostatnich
-   muze byt fyzicky objekt nebo abstraktni
-   musi byt jasne pojmenovana
-   **silna entita** - nezavisla na existenci jine
-   **slaba entita** - zavisla na existenci jine (slozeny klic?)
-   **supertyp** - nadtrida, obsahuje podtridy - vlastne dedeni
-   **subtyp** - podtrida, ma atributy nadtridy a jeste muze mit sve dalsi atributy

#### Relace

-   popisuji vztahy (asociace) mezi entitami
-   **parcialita** - povinne, nepovinne
-   **kardinalita** - maxinmalni pocet instanci jedne entity, ktere mohou byt sdruzene s entitou na druhe strane vztahu
-   **typy**
    -   _1:1_ - vzacne, spise chyba v navrhu (kdyz vazba povinna z obou stran - muzu sloucit)
    -   _1:n_ - caste
    -   _m:n_ - caste, resime vlozenim asociacni tabulky v relacnim modelu

#### Atribut

-   vlastnost entity nebo relace
-   mel by byt atomicky - nedelitelny

### Relacni model

-   reprezentuje db jako soubor relaci
-   dotazy pracuji vzdy jen s urcitou mnozinou dat
-   data zastoupena 2 rozmernymi tabulkami
-   tvori db specialisti (programator pak prepisuje do dane technologie - MySQL, Postgre..)
-   normalizace
    -   prevod ER modelu na Relacni schema
    -   kazde entite vytvorime tabulku

#### Atribut

-   vlastnost entity nebo relace
-   mel by byt atomicky - nedelitelny
-   jednoduche - z jedne komponenty (pr. catalogNumber)
-   slozene - z vice komponent (pr. genre - muze jich mit vic)
-   ovozene - lze jej odvodit z jineho (datum narozeni z rodneho cisla)
-   **domena** - mnozina pripustnych hodnot daneho atributu

#### Klic

-   minimalni super klic (minimalni pocet atributu ktere tvori unikatni vlastnost)

##### Super klic

-   mnozina atributu ktera je unikatni

##### Kandidatni klic

-   vybirame z nej primarni klic pro entity
-   mel by splnovat
    -   minimalni pocet atributu
    -   nejmensi pravdepodobnost zmeny (casovy invariant)
    -   nejsnazsi pouziti

##### Primarni klic

-   je unikatni
-   minimalni
-   nesmi obsahovat NULL
-   casovy invariant - v case se nemeni

##### Cizi klic

-   domena(PK) !== domena(FK)
-   FK - muze mit null

#### Navrhove vzory

-   **ciselnik** - #, \*, o, relativne staly seznam
-   **role**
-   **asociacni entita**
-   **master - detail** - nakup a polozka nakupu
-   **predpis - realizace** - blueprint a vyrobek
-   **strom** -

## Normalni formy

-   Soubor pravidel, ktere kdyz db splnuje tak je na dobre ceste aby byla dobra :D omg (kdyz ne, muze dochazet k problemum jako duplikovany obsah atd)
-   normalizace
    -   technika pro vytvoreni sady tabulek s minimalni redundancim ktera staci na pozdavky dat a jejich typu
    -   provadi se na jako sada testu na tabulce, zjistuje se zda jsou dodrzena pravidla

**Funkcni zavislost** - vztah mezi daty, B je funkcne zavisle na A, prave kdyz ke kazde hodnote A existuje nejvyse jedna hodnota B, znacime A->B

### Armstrongova pravidla

-   **reflexivita** - kdyz Y je podmnozina X pak X->Y
-   **trivialni funkcni zavislost** - AB->A, AB->B, AB->AB
-   **tranzitivita** - kdyz A->B a B->C pak taky A->C
-   **kompozice** - kdyz X->Y a X->Z pak X->YZ

### 0. data jsou v relacich

### 1. atributy jsou atomicke

-   jednoduche, dale nedelitelne atributy
-   vlastne nepovoluje relace uvnitr relace

### 2. plna funkcionalni zavislost

-   neklicovy atribut musi byt zavisly pouze na primarnim klici (nesmi existovat castecne funkcni zavislosti)

### 3. neexistujou tranzitivni zavislosti

-   osoba -> funkce ^ funkce -> plat ==> osoba -> plat

## Relacni databaze

-   data v nich obsazena, samy obsahuji identifikaci
-   uloziste (hardisk, sitove...)
    -   data v souborech -> data organizovana do bloku -> nekolik radku
    -   presun dat do RAMky - PCIE (zabira cas i prostredky)
    -   pravdepodobnost ze budeme chtit nasledujici radek -> presuneme cely blok
    -   **blok** - razene - pridava se na konec
        -   organizace pomoci Clustered index - udava poradi
        -   pri mazani by se nevyplatilo to setrasat -> radek se pouze oznaci jako smazany -> shrink - jednou za cas se data preskladaji
    -   pri zakladani se alokuje vetsi prostor
    -   hledani v souborech - narocne (musel bych tahat bloky do pameti) -> index
        -   kazdemu radku dam row id a do indexu si ulozim hodnotu -> pak prohledavam jen index a pomoci nej se dostanu na spravny radek -> slozitost linearni (protoze se implementuje jako spojak)
        -   stromy - logaritmus, musi byt vyvazeny -> B(balanced) stromy -> B+ stromy
        -   pri mazani musim aktualizovat index
        -   zpomalime manipulaci s daty (musim indexy aktualizovat) -> plati pravidlo - kdyz chci podle neceho hledat mel bych to mit indexovane
        -   vyuziti **hash** - otisk dat - hash tabulky -nemusim pak porovnavat vsechno
-   pamet - pri praci s daty je potreba je nacist bloky
    -   jednou za cas se zase data ulozi zpatky do souboru

## SQL

-   structured query language
-   jazyk pro relacni databaze
-   deklarativni (neproceduralni) - co se ma udelat, ne jak se to ma udelat (tj. algoritmus je prenechan interpretu jazyka)
-   domenove spicificky jazyk -> manipulace s daty v SRBD

### Generace programovacich jazyku

-   **0. Nic**
-   **1. GL - strojovy kod** (binarni, osmickovy ..)
-   **2. GL - jazyk symbolickych adres** - assembler
-   **3. GL - strojove nezavisle programovaci jazyky** - proceduralni (strukturovane / objektove orientovane)
-   **4. GL - programmer-friendly jazyky** - vyssi uroven abstrakce, princip WYSIWYG, neproceduralni
-   **5. GL - plne neproceduralni jazyky** - programator definuje objekty, omezeni a kriteria pro reseni

### Historie SQL

-   1974 - vznik jazyku Sequel - Structured English QUEry Language
-   postupne pridavany funkce a vznik ruznych standardu
    -   SQL 2
    -   SQL 3 - objektovy rysy
-   stal se z nej standard pro pristup k relacnim db
-   rozsireni
    -   T-SQL - Transact SQL - Microsoft
    -   PL/SQL - Procedural Language for SQL - Oracle

### Prikazy

-   DDL - Data Definition Language
    -   vytvareni a uprava struktura db
    -   `CREATE`, `ALTER`, `DROP`
-   DML - Data Manipulation Language
    -   manipulace s daty
    -   `SELECT`, `INSERT`, `UPDATE`, `DELETE`
-   DCL - Data Control Language
    -   rizeni prav uzivatelu
    -   `GRANT`, `REVOKE`
-   TCL - Transaction Control Language
    -   rizeni prace s transakcemi
    -   `BEGIN`, `COMMIT`, `ROLLBACK`

### Datove typy

-   kazdy system je muze mit jine
-   zakladni
    -   textove - `CHAR`, `VARCHAR`, `NVARCHAR`, `TEXT`
    -   ciselne - `INT`, `DECIMAL`, `NUMERIC`, `FLOAT`, `DOUBLE`
    -   datum/cas - `DATE`, `TIME`, `DATETIME`, `YEAR`
    -   dalsi - `BIT`, `BITARRAY`, `GPS`, `XML`, `IMAGE`

### Databazove pohledy - Views

-   v podstate ulozene SQL dotazy
-   tabulky s automaticky generovanym obsahem
-   vyhody
    -   zjednoduseni, zrychleni dotazu
    -   znovupouzitelne
    -   pocitane sloupce na zaklade dat z vice tabulek

### Transakce

-   prevadi db z jednoho konzistentniho stavu do druheho (pri prechodu muze dojit k nekonzistenci)
-   soubor operaci, ktere je nutne provest spolecne
-   resi konkurencni pristup k datum, konzistentni stav po chybe (rollback), izolace dat pri volani
-   stavy
    -   **commited** - splnena -> novy konzistentni stav
        -   pokud zjistime ze potrvzena transakce je spatne -> kompenzacni transakce
    -   **aborted** - zrusena -> musi byt obnoven stav
        -   zrusena transakce je treba vratit zpet - **rollback**

#### ACID

-   **A - atomicita** - vsechny transakce se musi provest
-   **C - conzistence** - existuje pouze jedna pravda (2 stroje nemohou ve stejny cas dostat ruznou odpoved)
-   **I - izolace** - vykonani jedne transakce neovlivnuje jinou (data zpracovavane v transakci nejsou v tu dobu dostupne jine transakci)
-   **D - durabilita** - po potrvzeni transakce jsou data trvale zapsana a muzeme je povazovat za platna

-   **Ztrata aktualizovanych dat** - data jsou premazana na zaklade starsi hodnoty (nactu z db do promenny a mezitim se mi hodnota v db zmeni)
-   **Dirty read - problem docasne aktualizace** - cteni neaktualni hodnoty - cteni dat ktera nejsou commitnuta (transakce neni dokoncena - izolace) - muze byt chteny v realtime systemech kde chci hodnotu rychle (ikdyz nemusi byt spravna)
-   **Problem nespravneho souctu** - scitam polozky A-Z a v pulce souctu nekdo inkrementuje vsechny hodnoty o 10 => pulka souctu bude spatne
-   **Problem neopakovatelneho cteni** - ctu hodnotu opakovane a v pulce mi ji nekdo zmeni

#### Transakcni zurnal

-   logy kde si uchovavam transakce (begin, read/write, end, commit, rollback, checkpoint) abych mohl restornout pri vypadku
-   obsahuje
    -   start_transaction
    -   write_item
    -   read_item
    -   commit
    -   abort
    -   checkpoint - ulozeni zmen na hdd
    -   rollback - vraceni zamen

### Rizeni konkurencniho pristupu

#### Deadlock

1. vice zadatelu se snazi pristoupit na zdroj
2. nesdilitelny zdroj
3. cekani

#### Zamykani

-   zamek - promenna svazana s datovou polozkou popisujici moznost operaci, ktere se daji provadet

##### Binarni zamek

-   stav odemknuto/zamknuto
-   zamyka read i write
-   nevyhoda - nemuzeme cist vickrat

##### Viceurovnove zamky

-   exkluzivni a sdilene zamky
-   umoznuji zamknout read a write nezavisle

##### Dvoufazove protokoly

-   expanzivni - nabiram zamky
-   redukcni - uvolnuju zamky

##### Casova znamka

-   nemuze vzniknout deadlock (neni tu cekani)
-   inkrementalni hodnota
-   polozka
    -   RS - read stamp - vzdycky >= WS
        -   transakce ktera je starsi nez ta, ktera naposledy cetla, nemuze cist -> jinak problem nespr. souctu
        -   transakce ktera je novejsi nez ta, ktera naposledy cetla, muze cist
        -   transakce ktera je starsi nez ta, ktera naposledy cetla, muze cist
    -   WS - write stamp
        -   transakce ktera je starsi nez ta, ktera tam naposledy zapsala, nemuze cist -> jinak ztrata aktualizovanych dat
        -   transakce ktera je novejsi nez ta, ktera tam naposledy zapsala, muze cist

##### System multiverzi

-   kazda polozka ma jeste index
-   vice verzi dat
