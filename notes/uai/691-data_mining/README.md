# 691 - Data mining

-   z google disku


    <h2 class="c37" id="h.40m4g3hmqv18"><span class="c16">Otazky</span></h2>
    <h3 class="c40" id="h.pdivdcvgd7hc">
      <span class="c8"
        >1. Popi&scaron;te proces dob&yacute;v&aacute;n&iacute;
        znalost&iacute; ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Jde o proces objevov&aacute;n&iacute; implicitn&iacute;ch (dop&#345;edu
        nezn&aacute;m&yacute;ch) a potenci&aacute;ln&#283;
        pou&#382;iteln&yacute;ch znalost&iacute; v datech. Zahrnuje:
        datab&aacute;ze, statistiku a um&#283;lou inteligenci.</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >C&iacute;l: automatick&eacute; vyhled&aacute;v&aacute;n&iacute;
        z&aacute;konitost&iacute; v rozs&aacute;hl&yacute;ch souborech
        dat.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav">
      <span class="c8"
        >2. K &#269;emu slou&#382;&iacute; proces selekce dat ?</span
      >
    </h3>
    <p class="c4">
      <span
        >V&yacute;b&#283;r relevantn&iacute; podmno&#382;iny z dostupn&yacute;ch
        dat. V&yacute;stupem je </span
      ><span class="c26"
        ><a class="c15" href="#h.nmjylv9yllav">Datov&aacute; matice</a></span
      ><span class="c2">.</span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >(M&#367;&#382;e b&yacute;t slo&#382;it&yacute; probl&eacute;m, data
        mohou b&yacute;t v r&#367;zn&yacute;ch datab&aacute;z&iacute;ch,
        form&aacute;tech, &hellip;)</span
      >
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 457.65px;
          height: 246.34px;
        "
        ><img
          alt=""
          src="./images/image76.png"
          style="
            width: 457.65px;
            height: 246.34px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-1">
      <span class="c8"
        >3. K &#269;emu slou&#382;&iacute; proces
        p&#345;edzpracov&aacute;n&iacute; dat ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >P&#345;&iacute;prava dat pro dal&scaron;&iacute;
        zpracov&aacute;n&iacute;:</span
      >
    </p>
    <ul class="c17 lst-kix_ra3pa3aaddkw-0 start">
      <li class="c3 li-bullet-0">
        <span class="c2"
          >&#269;i&scaron;t&#283;n&iacute; odlehl&yacute;ch hodnot</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >nahrazen&iacute; chyb&#283;j&iacute;c&iacute;ch hodnot</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">detekce z&aacute;visl&yacute;ch atribut&#367;</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >odstran&#283;n&iacute; offset&#367; a trend&#367;</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">extrakce p&#345;&iacute;znak&#367;</span>
      </li>
      <li class="c3 li-bullet-0"><span class="c2">agregace dat</span></li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-2">
      <span class="c8"
        >4. K &#269;emu slou&#382;&iacute; transformace dat ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Transformace dat do tvaru, kter&yacute; je vhodn&yacute; pro
        pou&#382;it&eacute; analytick&eacute; metody (oper&aacute;tory):</span
      >
    </p>
    <ul class="c17 lst-kix_2rypdkah42hh-0 start">
      <li class="c3 li-bullet-0">
        <span class="c2">Selekce atribut&#367;</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >V&aacute;&#382;en&iacute; atribut&#367; (atribut A bude m&iacute;t o
          50 % v&#283;t&scaron;&iacute; v&aacute;hu ne&#382; atribut B)</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">Normalizace atribut&#367;</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >Funk&#269;n&iacute; transformace a dopl&#328;ov&aacute;n&iacute;
          atribut&#367; vypo&#269;ten&yacute;mi hodnotami
        </span>
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-3">
      <span class="c8"
        >5. Co prob&iacute;h&aacute; p&#345;i dolov&aacute;n&iacute;
        znalost&iacute; ?</span
      >
    </h3>
    <p class="c4">
      <span class="c19"
        >Dolov&aacute;n&iacute; vlastnost&iacute; = data mining</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >Prob&iacute;h&aacute; zde hled&aacute;n&iacute; souvislost&iacute; v
        datech pomoc&iacute; metod a klasifik&aacute;tor&#367;.</span
      >
    </p>
    <p class="c4"><span class="c2">Metody:</span></p>
    <ul class="c17 lst-kix_m9zp4tfjzfe-0 start">
      <li class="c3 li-bullet-0">
        <span class="c2"
          >metody um&#283;l&eacute; inteligence nebo metody zalo&#382;en&eacute;
          na strojov&eacute;m u&#269;en&iacute;</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">shlukov&eacute; anal&yacute;zy</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >modelov&aacute;n&iacute; a automatick&eacute; tvorby modelu</span
        >
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-4">
      <span class="c8"
        >6. V &#269;em spo&#269;&iacute;v&aacute; vizualizace dat ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Proces zkoum&aacute;n&iacute; dat a informac&iacute; po jejich
        p&#345;eveden&iacute; do grafick&eacute; podoby.</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >C&iacute;l: pozn&aacute;v&aacute;n&iacute; a pochopen&iacute; dosud
        nezn&aacute;m&yacute;ch skute&#269;nost&iacute; a
        &#345;e&scaron;en&iacute; probl&eacute;mu zobrazen&iacute;
        v&iacute;cerozm&#283;rn&yacute;ch dat.</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >Nejpou&#382;&iacute;van&#283;j&scaron;&iacute; grafy:</span
      >
    </p>
    <ul class="c17 lst-kix_tst812dd7i4n-0 start">
      <li class="c3 li-bullet-0"><span class="c2">Scatter</span></li>
      <li class="c3 li-bullet-0"><span class="c2">Scatter Matrix</span></li>
      <li class="c3 li-bullet-0"><span class="c2">Bubble</span></li>
    </ul>
    <h3 class="c9" id="h.x6csa0sxkya4">
      <span class="c8"
        >7. Co je interpretace dat a reportov&aacute;n&iacute; ?</span
      >
    </h3>
    <p class="c4">
      <span
        >V&yacute;sledky anal&yacute;z jsou op&#283;t &#269;&iacute;sla =&gt; </span
      ><span>nutnost</span
      ><span class="c13"
        >&nbsp;p&#345;eveden&iacute; do prezentovateln&eacute; formy</span
      >
    </p>
    <ul class="c17 lst-kix_6g6271ewf7ai-0 start">
      <li class="c3 li-bullet-0">
        <span class="c2">formulace z&aacute;konitost&iacute;</span>
      </li>
      <li class="c3 li-bullet-0"><span class="c2">grafy</span></li>
      <li class="c3 li-bullet-0">
        <span class="c2">koment&aacute;&#345;</span>
      </li>
    </ul>
    <p class="c4">
      <span
        >V&yacute;stup anal&yacute;zy se prezentuj&iacute; ve form&#283;
        zpr&aacute;v (report&#367;)</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-5">
      <span class="c8"
        >8. Co je datov&aacute; matice? Co obsahuj&iacute;
        &#345;&aacute;dky a sloupce ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Z&aacute;kladn&iacute; datov&aacute; struktura pro ulo&#382;en&iacute;
        dat.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <ol class="c17 lst-kix_9snmuya19wfd-0 start" start="1">
      <li class="c3 li-bullet-0">
        <span class="c2">Sloupce: atributy (prom&#283;nn&eacute;)</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">&#344;&aacute;dky: vzory (p&#345;&iacute;pady)</span>
      </li>
    </ol>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >Je to vstup pro proces dolov&aacute;n&iacute; dat. Obsahuje
        relevantn&iacute; (proces selektov&aacute;n&iacute;),
        p&#345;edzpracovan&aacute; (proces p&#345;edzpracov&aacute;n&iacute;)
        data.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >M&#367;&#382;e b&yacute;t v r&#367;zn&yacute;ch form&aacute;tech (csv,
        txt, xml, tabulka: xls, sql &hellip;):</span
      >
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 517.5px;
          height: 244.14px;
        "
        ><img
          alt=""
          src="./images/image58.png"
          style="
            width: 517.5px;
            height: 244.14px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 351.15px;
          height: 95.65px;
        "
        ><img
          alt=""
          src="./images/image74.png"
          style="
            width: 351.15px;
            height: 95.65px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-6">
      <span class="c8"
        >9. Uve&#271;te p&#345;&iacute;klad datov&eacute; matice v
        textov&eacute;m form&aacute;tu.</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >datum p&#345;&iacute;jmen&iacute; jm&eacute;no p&#345;&iacute;chod
        odchod</span
      >
    </p>
    <p class="c4">
      <span class="c2">12.5.2009 Vom&aacute;&#269;ka Josef 7:30 16:15</span>
    </p>
    <p class="c4">
      <span class="c2">12.5.2009 Nov&aacute;k Pavel 9:20 14:35</span>
    </p>
    <p class="c4">
      <span class="c2">13.5.2009 Vom&aacute;&#269;ka Josef 6:15 18:20</span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-7">
      <span class="c8"
        >10. Uve&#271;te p&#345;&iacute;klad datov&eacute; matice v CSV
        form&aacute;tu.</span
      >
    </h3>
    <p class="c4">
      <span class="c2">datum;prijmeni;jmeno;prichod;odchod</span>
    </p>
    <p class="c4">
      <span class="c2">12.5.2009;Vom&aacute;&#269;ka;Josef;7:30;16:15</span>
    </p>
    <p class="c4">
      <span class="c2">12.5.2009;Nov&aacute;k;Pavel;9:20;14:30</span>
    </p>
    <p class="c4">
      <span class="c2">13.5.2009;Vom&aacute;&#269;ka;Josef;6:15;18:20</span>
    </p>
    <h3 class="c9" id="h.tunbjrsovxvx">
      <span class="c8"
        >11. Uve&#271;te p&#345;&iacute;klad datov&eacute; matice v XML
        form&aacute;tu.</span
      >
    </h3>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 472.5px;
          height: 328.87px;
        "
        ><img
          alt=""
          src="./images/image83.png"
          style="
            width: 472.5px;
            height: 328.87px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-8">
      <span class="c8">12. Co je v&yacute;sledkem selekce dat ?</span>
    </h3>
    <p class="c4">
      <span>Relevantn&iacute; podmno&#382;ina z dat. </span
      ><span class="c26"
        ><a class="c15" href="#h.nmjylv9yllav">Datov&aacute; matice</a></span
      ><span class="c2">.</span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-9">
      <span class="c8"
        >13. K &#269;emu se pou&#382;&iacute;v&aacute; jazyk SQL p&#345;i
        selekci dat ?</span
      >
    </h3>
    <ul class="c17 lst-kix_6qt2rkgw7nht-0 start">
      <li class="c3 li-bullet-0"><span class="c2">Selekce, filtrace</span></li>
      <li class="c3 li-bullet-0"><span class="c2">Agregace</span></li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >Nahrazen&iacute; chyb&#283;j&iacute;c&iacute;ch hodnot</span
        >
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-10">
      <span
        >14. Napi&scaron;te p&#345;&iacute;kaz awk, kter&yacute; z
        textov&eacute;ho souboru vytiskne 3. a 5. sloupec zdrojov&eacute;ho
        textov&eacute;ho datov&eacute;ho souboru a d&aacute;le vytiskne
        sou&#269;et obou sloupc&#367;.</span
      >
    </h3>
    <p class="c34">
      <span class="c23"
        >echo $&#39;1 2 3 4 5\n1 2 3 4 5&#39; &gt; vstup.txt<br />awk -F&quot;
        &quot; &#39;BEGIN{s3 = 0; s5 = 0} {print $3, $5; s3 += $3; s5 += $5}
        END{print s3 + s5}&#39; vstup.txt</span
      >
    </p>
    <p class="c4">
      <span class="c23">-F&rdquo; &rdquo;</span
      ><span class="c2"
        >: odd&#283;lova&#269; slov je mezera. To je i defaultn&#283;,
        m&#367;&#382;eme vynechat.</span
      >
    </p>
    <p class="c34">
      <span class="c23">$3</span
      ><span
        >: t&#345;et&iacute; slovo v &#345;&aacute;dku. ($0 znamen&aacute;
        cel&yacute; &#345;&aacute;dek)<br /></span
      ><span class="c23">BEGIN{...}</span
      ><span
        >: provede se jednou na za&#269;&aacute;tku. Inicializace
        prom&#283;nn&yacute;ch, nastaven&iacute; separatoru apod.<br /></span
      ><span class="c23">{...}</span
      ><span
        >: provede se pro ka&#382;d&yacute; record (&#345;&aacute;dek)<br /></span
      ><span class="c23">END{...}</span
      ><span class="c2">: provede se jednou na konci.</span>
    </p>
    <p class="c34"><span class="c2">---</span></p>
    <p class="c34">
      <span class="c2"
        >FS: field separator, odd&#283;lova&#269; slov, default je whitespace<br />NF:
        num fields, po&#269;et slov na &#345;&aacute;dku<br />RS: record
        separator, odd&#283;lova&#269; &#345;&aacute;dk&#367;, default je
        newline<br />NR: num records, po&#269;et &#345;&aacute;dk&#367;</span
      >
    </p>
    <p class="c34">
      <span class="c26"
        ><a
          class="c15"
          href="https://www.google.com/url?q=https://likegeeks.com/awk-command/&amp;sa=D&amp;source=editors&amp;ust=1615816876904000&amp;usg=AOvVaw3BmZQO-OI2_H-tRfLIwuJN"
          >30 Examples for Awk Command in Text Processing - Like Geeks</a
        ></span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-11">
      <span class="c8"
        >15. Popi&scaron;te jak pracuje p&#345;&iacute;kaz awk.</span
      >
    </h3>
    <p class="c4 c10">
      <span class="c2"
        >AWK slou&#382;&iacute; pro zpracov&aacute;n&iacute; textu.
        Rozd&#283;l&iacute; vstupn&iacute; text na recordy (&#345;&aacute;dky)
        dle nastaven&eacute;ho odd&#283;lova&#269;e RS (defaultn&#283; znak
        nov&eacute;ho &#345;&aacute;dku - proto &#345;&aacute;dky), kter&eacute;
        jsou v itera&#269;n&iacute; &#269;&aacute;sti
        p&#345;&iacute;stupn&eacute; jako prom&#283;nn&aacute; $0. Recordy pak
        je&scaron;t&#283; rozd&#283;l&iacute; na Fieldy (slova) dle
        nastaven&eacute;ho odd&#283;lova&#269;e FS (defaultn&#283; znak
        whitespace - proto slova), kter&eacute; jsou v itera&#269;n&iacute;
        &#269;&aacute;st&iacute; p&#345;&iacute;stupn&eacute; jako
        prom&#283;nn&eacute; $1 &hellip; $n. Argumentem p&#345;&iacute;kazu jsou
        pot&eacute; bloky p&#345;&iacute;kaz&#367;: blok BEGIN{...},
        itera&#269;n&iacute; blok{}, a blok END{...}, kde m&#367;&#382;eme text
        zpracov&aacute;vat.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-12">
      <span class="c8"
        >16. Jak&yacute;mi p&#345;&iacute;kazy v jazyce C je mo&#382;no
        zpracovat textov&eacute; datov&eacute; soubory ?</span
      >
    </h3>
    <p class="c4"><span class="c2">fgets, scanf</span></p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 438.15px;
          height: 206.05px;
        "
        ><img
          alt=""
          src="./images/image66.png"
          style="
            width: 438.15px;
            height: 206.05px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-13">
      <span class="c8"
        >17. Jak&eacute; t&#345;&iacute;dy a jak&eacute; metody jsou
        Jav&#283; vhodn&eacute; pro zpracov&aacute;n&iacute; textov&yacute;ch
        datov&yacute;ch soubor&#367; ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >BufferedReader.readLine(), FileReader, String.split(&ldquo;,&rdquo;),
        &hellip;</span
      >
    </p>
    <p class="c32 c7"><span class="c2"></span></p>
    <p class="c32 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.nmjylv9yllav-14">
      <span class="c8"
        >18. Jak&yacute;m zp&#367;sobem se zpracov&aacute;vaj&iacute;
        datov&eacute; soubory v XML form&aacute;tu ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2">Pomoc&iacute; stream (SAX) nebo DOM parser&#367;.</span>
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 519.5px;
          height: 230.41px;
        "
        ><img
          alt=""
          src="./images/image82.png"
          style="
            width: 519.5px;
            height: 230.41px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-15">
      <span class="c8"
        >19. Jak&yacute; je rozd&iacute;l mezi SAX a DOM parserem ?</span
      >
    </h3>
    <ol class="c17 lst-kix_uqd2wkkpx5pu-0 start" start="1">
      <li class="c3 li-bullet-0">
        <span class="c2"
          >SAX: parsuje dokument jako Stream (proudov&#283;). Nelze se vracet.
          Generuje ud&aacute;losti na kter&eacute; m&#367;&#382;eme reagovat
          (za&#269;&aacute;tek tagu, konec tagu, &hellip;).
          Pam&#283;&#357;ov&#283; v&yacute;hodn&yacute;, dobr&yacute; pro
          ob&#345;&iacute; dokumenty, ale nep&#345;&iacute;jemn&#283; se s
          t&iacute;m pracuje.</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >DOM: parsuje dokument cel&yacute; do pam&#283;ti jako strom
          objekt&#367;. Pam&#283;&#357;ov&#283; n&aacute;ro&#269;n&yacute;.
          Dob&#345;e se s t&iacute;m pracuje nap&#345;. pomoc&iacute; XPath.
        </span>
      </li>
    </ol>
    <h3 class="c9" id="h.nmjylv9yllav-16">
      <span class="c8"
        >20. Jak se spo&#269;te st&#345;edn&iacute; hodnota souboru dat,
        medi&aacute;n a modus ?</span
      >
    </h3>
    <p class="c4">
      <span class="c14 c35"
        >St&#345;edn&iacute; hodnota (aritmetick&yacute; pr&#367;m&#283;r)</span
      ><span class="c35">&nbsp;-</span><span>&nbsp;</span
      ><img src="./images/image1.png" />
    </p>
    <p class="c4">
      <span class="c14">Medi&aacute;n</span
      ><span class="c2"
        >&nbsp;- set&#345;&iacute;d&#283;n&iacute; dat vzestupn&#283; dle
        hodnoty =&gt; medi&aacute;n je uprost&#345;ed t&eacute;to
        posloupnosti</span
      >
    </p>
    <p class="c32">
      <span>1 1 3 </span><span class="c27 c14">4 7</span
      ><span class="c2">&nbsp;8 9 9</span>
    </p>
    <p class="c32">
      <span>m = </span><img src="./images/image2.png" /><span class="c33"
        >&nbsp;</span
      ><span>=</span><span class="c33">&nbsp;</span><span class="c2">5.5</span>
    </p>
    <p class="c4">
      <span class="c14">Modus</span
      ><span class="c2"
        >&nbsp;- hodnota s nejv&#283;t&scaron;&iacute;
        &#269;etnost&iacute;</span
      >
    </p>
    <p class="c32">
      <span>5 </span><span class="c14">6</span><span>&nbsp;9 1 </span
      ><span class="c14">6</span><span>&nbsp;2 </span><span class="c13">6</span>
    </p>
    <p class="c32"><img src="./images/image3.png" /></p>
    <h3 class="c9" id="h.nmjylv9yllav-17">
      <span class="c8"
        >21. Jak se spo&#269;te rozptyl a sm&#283;rodatn&aacute; odchylka
        ?</span
      >
    </h3>
    <p class="c4">
      <span class="c14">Rozptyl </span><span>- </span
      ><img src="./images/image4.png" />
    </p>
    <p class="c4">
      <span>neboli pro &#269;&iacute;sla </span><span class="c14">1 5 6 4</span
      ><span>&nbsp;je pr&#367;m&#283;r </span><img src="./images/image5.png" />
    </p>
    <p class="c4"><span>a tedy </span><img src="./images/image6.png" /></p>
    <p class="c4">
      <span class="c14">Sm&#283;rodatn&aacute; odchylka </span><span>- </span
      ><img src="./images/image7.png" />
    </p>
    <p class="c4">
      <span>neboli v na&scaron;em p&#345;&iacute;pad&#283;</span
      ><span class="c25">&nbsp;</span><img src="./images/image8.png" />
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-18">
      <span class="c8">22. Co je histogram ?</span>
    </h3>
    <p class="c4 c10">
      <span class="c2"
        >Je grafick&eacute; zn&aacute;zorn&#283;n&iacute; distribuce dat
        pomoc&iacute; sloupcov&eacute;ho grafu. Graf m&aacute; sloupce dohromady
        vyjad&#345;uj&iacute;c&iacute; interval, p&#345;i&#269;em&#382;
        v&yacute;&scaron;ka sloupc&#367; vyjad&#345;uje &#269;etnost
        sledovan&eacute; veli&#269;iny v dan&eacute;m intervalu.</span
      >
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 225.81px;
          height: 239.5px;
        "
        ><img
          alt=""
          src="./images/image63.png"
          style="
            width: 225.81px;
            height: 239.5px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-19">
      <span class="c8"
        >23. Vytvo&#345;te histogram pro atribut s hodnotami
        1,2,2,2,1,1,2,3,4,4,4,4,1,1,2,3,4,3,3,3,3,3,1,3,3,3,2,3,2,3,3,3,3,2</span
      >
    </h3>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 601.7px;
          height: 478.67px;
        "
        ><img
          alt=""
          src="./images/image75.png"
          style="
            width: 601.7px;
            height: 478.67px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.gk0x00ns8hfi">
      <span class="c8"
        >24. Jak&eacute; procento (p&#345;ibli&#382;n&#283;) hodnot se
        nach&aacute;z&iacute; u norm&aacute;ln&iacute;ho rozd&#283;len&iacute; v
        intervalu &lt;-3&sigma;,+3&sigma;&gt; uva&#382;ov&aacute;n&iacute;
        relativn&#283; ke st&#345;edn&iacute; hodnot&#283;. (pozn. &sigma; =
        sigma)</span
      >
    </h3>
    <p class="c4"><span class="c2">99,73% v&scaron;ech hodnot</span></p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 474px;
          height: 446px;
        "
        ><img
          alt=""
          src="./images/image84.png"
          style="
            width: 474px;
            height: 446px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-20">
      <span class="c8"
        >25. Zjist&#283;te, kter&eacute; z
        n&aacute;sleduj&iacute;c&iacute;ch atribut&#367; jsou
        nez&aacute;visl&eacute; (nekorelovan&eacute;) a = { 1,-1,1,-1}, b =
        {-4,-4,4,4}, c = {4, 2, 4, 2}</span
      >
    </h3>
    <p class="c4">
      <span>Nekorelovane jsou </span><span class="c14">a</span
      ><span>&nbsp;a </span><span class="c14">b</span><span>, </span
      ><span class="c14">b </span><span>a </span><span class="c13">c</span>
    </p>
    <p class="c4">
      <span>Korelovane jsou</span><span class="c14">&nbsp;a </span><span>a</span
      ><span class="c13">&nbsp;c</span>
    </p>
    <p class="c0 subtitle" id="h.2iitbslpyxc">
      <span class="c11">Postup pro korelaci AB a AC</span>
    </p>
    <p class="c4"><img src="./images/image9.png" /></p>
    <p class="c4"><img src="./images/image10.png" /></p>
    <p class="c4"><img src="./images/image11.png" /></p>
    <p class="c4"><img /><span class="c2">=&gt; nekorelovane</span></p>
    <p class="c4">
      <img src="./images/image12.png" /><span class="c2">=&gt; korelovane</span>
    </p>
    <p class="c0 subtitle" id="h.6fijslubi2mh">
      <span class="c11">Korelace v excelu </span>
    </p>
    <p class="c4">
      <span class="c2">(fce CORREL) - 0 = nekorelovane, 1 = korelovane</span>
    </p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 493px;
          height: 84px;
        "
        ><img
          alt=""
          src="./images/image59.png"
          style="
            width: 493px;
            height: 84px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-21">
      <span class="c8"
        >26. Vyjmenujte alespo&#328; &#269;ty&#345;i typy
        atribut&#367;.</span
      >
    </h3>
    <ol class="c17 lst-kix_vw2c2fvyqa3t-0 start" start="1">
      <li class="c3 li-bullet-0">
        <span class="c2">nominal - kategoricka promenna</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">numeric - ciselne hodnoty</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">integer - celociselne hodnoty</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">real - realna cisla</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >binominal - kategoricka promenna se dvema kategoriemi (zvlastni
          pripad nominal)</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >polynominal - kategoricka promenna s vice nez dvema kategoriemi
          (zvlastni pripad nominal)</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">date_time - datum a cas</span>
      </li>
      <li class="c3 li-bullet-0"><span class="c2">date - pouze datum</span></li>
      <li class="c3 li-bullet-0"><span class="c2">time - pouze cas</span></li>
    </ol>
    <h3 class="c9" id="h.nmjylv9yllav-22">
      <span class="c8"
        >27. Uve&#271;te alespo&#328; dv&#283; role atribut&#367;.</span
      >
    </h3>
    <ol class="c17 lst-kix_j576xou3erbw-0 start" start="1">
      <li class="c3 li-bullet-0">
        <span class="c2">regular - data (typicky vstupy modelu)</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">label - pozadovany vystup modelu</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">outlier - odlehla hodnota</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2">id - identifikator zaznamu</span>
      </li>
      <li class="c3 li-bullet-0"><span class="c2">weight - vaha</span></li>
      <li class="c3 li-bullet-0"><span class="c2">cluster - shluk</span></li>
    </ol>
    <h3 class="c9" id="h.nmjylv9yllav-23">
      <span class="c8"
        >28. Uve&#271;te vztah pro normalizaci normou min-max.</span
      >
    </h3>
    <p class="c4"><img src="./images/image13.png" /></p>
    <p class="c4"><span class="c2">kde</span></p>
    <ul class="c17 lst-kix_49na2382sqfb-0 start">
      <li class="c3 li-bullet-0">
        <img src="./images/image14.png" /><span class="c2"
          >&nbsp;je promenna (atribut), kterou normalizujeme</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image15.png" /><span class="c2"
          >&nbsp;je normalizovana promenna (vysledek)</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image16.png" /><span>&nbsp;a </span
        ><img src="./images/image17.png" /><span class="c2"
          >&nbsp;je minimalni/maximalni hodnota v datech</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image18.png" /><span>&nbsp;a </span
        ><img src="./images/image19.png" /><span
          >&nbsp;je minimalni/maximalni hodnota normalizovane promenne a urcuje
          rozsah, do ktereho promennou </span
        ><img src="./images/image14.png" /><span class="c2"
          >&nbsp;transformujeme</span
        >
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-24">
      <span class="c8"
        >29. Uve&#271;te vztah pro normalizaci normou Z-scores</span
      >
    </h3>
    <p class="c4"><img src="./images/image20.png" /></p>
    <p class="c4"><span class="c2">kde</span></p>
    <ul class="c17 lst-kix_o25ymk91k3am-0 start">
      <li class="c3 li-bullet-0">
        <img src="./images/image14.png" /><span class="c2"
          >&nbsp;je promenna, kterou normalizujeme</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image21.png" /><span class="c2"
          >&nbsp; je normalizovana promenna (vysledek)</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image22.png" /><span>&nbsp;je stredni hodnota </span
        ><img src="./images/image14.png" />
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image23.png" /><span
          >&nbsp;je smerodatna odchylka </span
        ><img src="./images/image14.png" />
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-25">
      <span class="c8"
        >30. Uve&#271;te vztah pro normalizaci Euclideovskou normou</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Pouziva se pro normalizaci vektoru - neaplikuje se na jednotlive
        atributy, ale na radky (vsechny nebo podmnozina atributu), tj. radek
        nebo podmnozina atributu je povazovan za vektor.</span
      >
    </p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 350px;
          height: 176px;
        "
        ><img
          alt=""
          src="./images/image80.png"
          style="
            width: 350px;
            height: 176px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4"><span class="c2">kde</span></p>
    <ul class="c17 lst-kix_ig6x8ije0c6e-0 start">
      <li class="c3 li-bullet-0">
        <img src="./images/image14.png" /><span class="c2"
          >&nbsp;je radkovy vektor</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image15.png" /><span class="c2"
          >&nbsp;je normalizovany vektor</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image24.png" /><span class="c2"
          >&nbsp;je velikost vektoru - norma pro normalizaci (rozsah)</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <img src="./images/image25.png" /><span class="c2"
          >&nbsp;je dimenze vektoru (pocet slozek)</span
        >
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-26">
      <span class="c8"
        >31. Atribut a = {-5,3,4,6,7} normujte normou min-max. Hodnoty
        uve&#271;te jako zlomky.</span
      >
    </h3>
    <p class="c4">
      <span class="c2 c44"
        >Neni tu uveden rozsah, takze to normuju na &lt;-1,1&gt;</span
      >
    </p>
    <p class="c0 subtitle" id="h.wq5z7f3xq7ev">
      <span class="c11">Vypocet</span>
    </p>
    <p class="c4">
      <span>Vzorec viz </span
      ><span class="c26"
        ><a class="c15" href="#h.nmjylv9yllav"
          >28. Uve&#271;te vztah pro normalizaci normou min-max.</a
        ></span
      >
    </p>
    <p class="c4"><img src="./images/image26.png" /></p>
    <p class="c4"><img src="./images/image27.png" /></p>
    <p class="c4"><img src="./images/image28.png" /></p>
    <p class="c4"><img src="./images/image29.png" /></p>
    <p class="c4"><img src="./images/image30.png" /></p>
    <p class="c4"><img src="./images/image31.png" /></p>
    <p class="c0 subtitle" id="h.28ai5kvwbdfv">
      <span class="c11">RapidMiner</span>
    </p>
    <p class="c4">
      <span>Blok </span><span class="c20">Normalize</span
      ><span>&nbsp;nastaveny na metodu </span
      ><span class="c20">range transformation</span
      ><span class="c2">&nbsp;s rozsahem &lt;-1,1&gt;</span>
    </p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 206px;
          height: 187px;
        "
        ><img
          alt=""
          src="./images/image73.png"
          style="
            width: 206px;
            height: 187px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-27">
      <span class="c8"
        >32. Atribut a = {1,-1,1,-1} normujte Euclideovskou normou.</span
      >
    </h3>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 96.5px;
          height: 82.84px;
        "
        ><img
          alt=""
          src="./images/image65.png"
          style="
            width: 96.5px;
            height: 82.84px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4"><img src="./images/image32.png" /></p>
    <p class="c4"><img src="./images/image33.png" /></p>
    <p class="c4"><img src="./images/image34.png" /></p>
    <p class="c4"><img src="./images/image35.png" /></p>
    <p class="c4"><img src="./images/image36.png" /></p>
    <h3 class="c9" id="h.nmjylv9yllav-28">
      <span>33. Atribut a = {4,2,4,2} normujte Z-scores</span>
    </h3>
    <p class="c4">
      <span>Vzorec viz </span
      ><span class="c26"
        ><a class="c15" href="#h.nmjylv9yllav"
          >29. Uve&#271;te vztah pro normalizaci normou Z-scores</a
        ></span
      >
    </p>
    <p class="c4"><img src="./images/image37.png" /></p>
    <p class="c4"><img src="./images/image38.png" /></p>
    <p class="c4"><img src="./images/image39.png" /></p>
    <p class="c4"><img src="./images/image40.png" /></p>
    <p class="c4"><img src="./images/image41.png" /></p>
    <p class="c4"><img src="./images/image42.png" /></p>
    <h3 class="c9" id="h.nmjylv9yllav-29">
      <span class="c8"
        >34. Spo&#269;t&#283;te Euclideovskou vzd&aacute;lenost
        vektor&#367; a = {3,2,1,4} a b = {4,3,2,5}.</span
      >
    </h3>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 303.5px;
          height: 100.33px;
        "
        ><img
          alt=""
          src="./images/image70.png"
          style="
            width: 303.5px;
            height: 100.33px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 205.65px;
          height: 252.46px;
        "
        ><img
          alt=""
          src="./images/image68.png"
          style="
            width: 205.65px;
            height: 252.46px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c32 c7"><span class="c2"></span></p>
    <p class="c4"><span class="c2">a = &nbsp; &nbsp; {3, 2, 1, 4}</span></p>
    <p class="c4"><span class="c2">b = &nbsp; &nbsp; {4, 3, 2, 5}</span></p>
    <p class="c4">
      <span class="c2">----------------------- [a + (-b)], 3 + (-4) ....</span>
    </p>
    <p class="c4"><span class="c2">a-b = &nbsp;{-1,-1,-1,-1}</span></p>
    <p class="c4">
      <span>|| a-b || (velikost) = </span><img src="./images/image43.png" />
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.nmjylv9yllav-30">
      <span class="c8"
        >35. Spo&#269;t&#283;te Hammingovu vzd&aacute;lenost vektor&#367;
        a = {1,0,0,0,0,1,1,1,0}, b = {1,0,1,0,1,1,0,1,0}.</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Hammingova vzd&aacute;lenost je po&#269;et slo&#382;ek ve
        kter&yacute;ch se dva vektory li&scaron;&iacute;. XOR mezi
        slo&#382;kami.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c0 subtitle" id="h.9b51v3rj1obv"><span class="c11">XOR</span></p>
    <p class="c4"><span class="c2">a b y</span></p>
    <p class="c4"><span class="c2">0 0 0</span></p>
    <p class="c4"><span class="c13">1 0 1</span></p>
    <p class="c4"><span class="c13">0 1 1</span></p>
    <p class="c4"><span class="c2">1 1 0</span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span>a = {1,0,</span><span class="c27 c14">0</span><span>,0,</span
      ><span class="c27 c14">0</span><span>,1,</span
      ><span class="c27 c14">1</span><span class="c2">,1,0}</span>
    </p>
    <p class="c4">
      <span>b = {1,0,</span><span class="c27 c14">1</span><span>,0,</span
      ><span class="c27 c14">1</span><span>,1,</span
      ><span class="c14 c27">0</span><span class="c2">,1,0}</span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4"><span class="c14 c24">vzd&aacute;lenost = 3</span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.nmjylv9yllav-31">
      <span class="c8"
        >36. Ur&#269;ete Levensteinovu vzd&aacute;lenost
        &#345;et&#283;z&#367; hrad a had.</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Vyjad&#345;uje kolik minim&aacute;ln&#283; operac&iacute;
        odstran&#283;n&iacute;, p&#345;id&aacute;n&iacute; nebo nahrazen&iacute;
        p&iacute;smenka musim ud&#283;lat, abych se dostal ze slova A na slovo
        B.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c0 subtitle" id="h.8k23mt1ms2ep">
      <span class="c11"
        >&#344;e&scaron;en&iacute; jednoduchou &uacute;vahou:</span
      >
    </p>
    <p class="c4"><span class="c2">hrad -?-&gt; had</span></p>
    <p class="c4"><span class="c13">1 &uacute;prava:</span></p>
    <p class="c4"><span class="c2">-r (odstranit r)</span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c13"
        >D&aacute; se to ale po&#269;&iacute;tat i matic&iacute; (tak to bude
        asi cht&iacute;t Skrbek):</span
      >
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 656.5px;
          height: 413.31px;
        "
        ><img
          alt=""
          src="./images/image81.png"
          style="
            width: 656.5px;
            height: 413.31px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.nmjylv9yllav-32">
      <span class="c8"
        >37. Jak&yacute;m geometrick&yacute;m &uacute;tvarem
        prokl&aacute;d&aacute;me data p&#345;i line&aacute;rn&iacute; regresi a
        jak&eacute; jsou jeho parametry.</span
      >
    </h3>
    <p class="c4"><span class="c2">P&#345;&iacute;mkou</span></p>
    <p class="c4"><img src="./images/image44.png" /></p>
    <p class="c4">
      <img src="./images/image45.png" /><span class="c2">- bod</span>
    </p>
    <p class="c4">
      <img src="./images/image46.png" /><span class="c2"
        >- strmost p&#345;&iacute;mky</span
      >
    </p>
    <p class="c4">
      <img src="./images/image47.png" /><span class="c2"
        >- posunut&iacute; (hodnota p&#345;i protnut&iacute; osy y, x=0)</span
      >
    </p>
    <p class="c4">
      <img src="./images/image48.png" /><span class="c2">- odchylka</span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.nmjylv9yllav-33">
      <span
        >38. Spo&#269;t&#283;te parci&aacute;ln&iacute; derivaci funkce
        <br />f(x,y,z)=</span
      ><img src="./images/image49.png" />
    </h3>
    <p class="c4"><img src="./images/image50.png" /></p>
    <h3 class="c9" id="h.nmjylv9yllav-34">
      <span class="c8"
        >39. Jak se &#345;e&scaron;&iacute; neline&aacute;rn&iacute;
        z&aacute;vislosti s pou&#382;it&iacute;m line&aacute;rn&iacute; regrese
        ?</span
      >
    </h3>
    <p class="c4">
      <span>P&#345;id&aacute;me atribut </span
      ><img src="./images/image51.png" /><span
        >do rovnice p&#345;&iacute;mky line&aacute;rn&iacute; regrese (viz
        ot&aacute;zka &#269;. 37), kde </span
      ><img src="./images/image45.png" /><span class="c2">&nbsp;je bod</span>
    </p>
    <p class="c4">
      <span>neboli </span><img src="./images/image52.png" /><span class="c29 c25"
        >.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.ck10b7cqdhle">
      <span class="c8">40. Co je dendrogram ?</span>
    </h3>
    <p class="c4">
      <span
        >Dendrogram je diagram zachycuj&iacute;c&iacute; hierarchii shluk&#367;
        a ukazuje jednotliv&eacute; kroky shlukov&eacute; anal&yacute;zy (u
        hierarchick&eacute;ho shlukov&aacute;n&iacute;).</span
      ><span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 601.7px;
          height: 254.67px;
        "
        ><img
          alt=""
          src="./images/image72.png"
          style="
            width: 601.7px;
            height: 254.67px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9 c39" id="h.72udvcbd5z7j"><span class="c8"></span></h3>
    <h3 class="c9" id="h.esgscukg2upg">
      <span class="c8">41. Popi&scaron;te k-means algoritmus.</span>
    </h3>
    <p class="c4 c10">
      <span
        >K-means je algoritmus, kter&yacute; shroma&#382;&#271;uje data do
        shluk&#367;, kde hodnota </span
      ><img src="./images/image53.png" /><span class="c2"
        >&nbsp;je po&#269;et shluk&#367;.</span
      >
    </p>
    <ol class="c17 lst-kix_r1cx42oi3tr5-0 start" start="1">
      <li class="c4 c28 li-bullet-0">
        <span>(Obvykle n&aacute;hodn&#283;) vygenerujeme st&#345;edy </span
        ><img src="./images/image53.png" /><span class="c2"
          >&nbsp;shluk&#367; (st&#345;edy jsou vektory).</span
        >
      </li>
      <li class="c4 c28 li-bullet-0">
        <span class="c2"
          >P&#345;i&#345;ad&iacute;me vzory (objekty) ke shluk&#367;m dle
          vzd&aacute;lenosti od st&#345;edu (nejbli&#382;&scaron;&iacute;
          shluk).</span
        >
      </li>
      <li class="c4 c28 li-bullet-0">
        <span class="c2"
          >Spo&#269;&iacute;t&aacute;me nov&yacute; st&#345;ed shluku z
          p&#345;idru&#382;en&yacute;ch dat, tak aby &scaron;lo o
          t&#283;&#382;i&scaron;t&#283; vybran&yacute;ch vzor&#367;.</span
        >
      </li>
      <li class="c4 c28 li-bullet-0">
        <span class="c2"
          >Body 2 a 3 opakujeme, dokud se shluky neust&aacute;l&iacute; (tj.
          dokud se st&#345;edy posouvaj&iacute; o v&iacute;ce, ne&#382; je
          zadan&aacute; hodnota).</span
        >
      </li>
    </ol>
    <h3 class="c9" id="h.nmjylv9yllav-35">
      <span class="c8"
        >42. Jak&eacute; zn&aacute;te metody ur&#269;en&iacute;
        vzd&aacute;lenosti mezi shluky?</span
      >
    </h3>
    <ul class="c17 lst-kix_dfy5npuez9sf-0 start">
      <li class="c3 li-bullet-0">
        <span class="c2">Metoda nejbli&#382;&scaron;&iacute;ho souseda</span>
      </li>
    </ul>
    <ul class="c17 lst-kix_dfy5npuez9sf-1 start">
      <li class="c4 c28 li-bullet-0">
        <span class="c2"
          >minimum ze vzd&aacute;lenost&iacute; mezi vzory shluk&#367; A a
          B</span
        >
      </li>
    </ul>
    <ul class="c17 lst-kix_dfy5npuez9sf-0">
      <li class="c3 li-bullet-0">
        <span class="c2"
          >Metoda nejvzd&aacute;len&#283;j&scaron;&iacute;ho souseda</span
        >
      </li>
    </ul>
    <ul class="c17 lst-kix_dfy5npuez9sf-1 start">
      <li class="c4 c28 li-bullet-0">
        <span class="c2"
          >maximum ze vzd&aacute;lenost&iacute; mezi vzory shluk&#367; A a
          B</span
        >
      </li>
    </ul>
    <ul class="c17 lst-kix_dfy5npuez9sf-0">
      <li class="c3 li-bullet-0">
        <span class="c2"
          >Metoda pr&#367;m&#283;rn&eacute; vzd&aacute;lenosti</span
        >
      </li>
    </ul>
    <ul class="c17 lst-kix_dfy5npuez9sf-1 start">
      <li class="c4 c28 li-bullet-0">
        <span class="c2"
          >pr&#367;m&#283;rn&aacute; vzd&aacute;lenost mezi vzory shluk&#367; A
          a B</span
        >
      </li>
    </ul>
    <ul class="c17 lst-kix_dfy5npuez9sf-0">
      <li class="c3 li-bullet-0">
        <span class="c2">Metoda centroidn&iacute;</span>
      </li>
    </ul>
    <ul class="c17 lst-kix_dfy5npuez9sf-1 start">
      <li class="c4 c28 li-bullet-0">
        <span class="c2"
          >vzd&aacute;lenost je d&aacute;na vzd&aacute;lenost&iacute;
          st&#345;ed&#367; shluk&#367;</span
        >
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-36">
      <span class="c8"
        >43. Co je metoda nejbli&#382;&scaron;&iacute;ho souseda ?</span
      >
    </h3>
    <p class="c4 c10">
      <span class="c2"
        >Metoda nejbli&#382;&scaron;&iacute;ho souseda ur&#269;uje
        vzd&aacute;lenost mezi shluky A a B podle minima ze
        vzd&aacute;lenost&iacute; mezi vzory shluk&#367; A a B.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-37">
      <span class="c8"
        >44. Podle jak&yacute;ch krit&eacute;ri&iacute; se
        vyb&iacute;raj&iacute; atributy v metod&#283; rozhodovac&iacute;ch
        strom&#367; ?</span
      >
    </h3>
    <ul class="c17 lst-kix_dbtaf4o0hyhe-0 start">
      <li class="c3 li-bullet-0">
        <span
          >C&iacute;lem je vybrat atribut, kter&yacute; nejl&eacute;pe
          odli&scaron;&iacute; p&#345;&iacute;klady r&#367;zn&yacute;ch
          t&#345;&iacute;d, tj.</span
        ><span class="c2"
          >&ndash;Maximalizovat po&#269;et vzor&#367; t&eacute;&#382;e
          t&#345;&iacute;dy v podmno&#382;in&aacute;ch vznikl&yacute;ch
          rozd&#283;len&iacute;m mno&#382;iny dan&yacute;m atributem.</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span>Pou&#382;&iacute;van&aacute; krit&eacute;ria:</span>
      </li>
    </ul>
    <p class="c4 c6">
      <span class="c2"
        >&ndash; Entropie (minimalizace entropie &ndash; neur&#269;itosti -
        n&aacute;hodnosti v podmno&#382;in&aacute;ch)</span
      >
    </p>
    <p class="c4 c6">
      <span class="c2"
        >&ndash; Informa&#269;n&iacute; zisk (maximalizace redukce entropie
        p&#345;i pou&#382;it&iacute; zvolen&eacute;ho atributu. Vztahuje se na
        entropii po&#269;&iacute;tanou pro dan&yacute; atribut pro cel&aacute;
        data)</span
      >
    </p>
    <p class="c4 c6">
      <span class="c2"
        >&ndash; Pom&#283;rn&yacute; informa&#269;n&iacute; zisk
        (informa&#269;n&iacute; zisk d&#283;len&yacute; po&#269;tem
        v&#283;tven&iacute;, zohled&#328;uje po&#269;et hodnot atributu)</span
      >
    </p>
    <p class="c4 c6">
      <span class="c2"
        >&ndash; Gini index (vych&aacute;z&iacute; z po&#269;tu
        p&#345;&iacute;klad&#367; dan&eacute; t&#345;&iacute;dy
        zji&scaron;&#357;ovan&eacute; na n&#283;jak&eacute; mno&#382;in&#283;
        nebo podmno&#382;in&#283;)</span
      >
    </p>
    <p class="c4 c6">
      <span class="c2">&ndash; Ch&iacute; kvadr&aacute;t</span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.nmjylv9yllav-38">
      <span>45. Popi&scaron;te algoritmus </span
      ><span>vytv&aacute;&#345;en&iacute;</span
      ><span class="c8">&nbsp;rozhodovac&iacute;ho stromu.</span>
    </h3>
    <ol class="c17 lst-kix_ruomyvjku1x-0 start" start="1">
      <li class="c3 li-bullet-0">
        <span class="c2">Zvolit jeden atribut (ko&#345;en podstromu)</span>
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >Data rozd&#283;lit podle hodnot zvolen&eacute;ho atributu na
          podmno&#382;iny a ka&#382;d&eacute; podmno&#382;in&#283;
          p&#345;i&#345;adit nov&yacute; uzel stromu</span
        >
      </li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >Pokud existuje uzel, kde v&scaron;echna data nepat&#345;&iacute; do
          t&eacute;&#382;e t&#345;&iacute;dy, pak zp&#283;t na bod 1</span
        >
      </li>
      <li class="c3 li-bullet-0"><span class="c2">Konec algoritmu</span></li>
    </ol>
    <h3 class="c9" id="h.nmjylv9yllav-39">
      <span class="c8"
        >46. Co to je k&#345;&iacute;&#382;ov&aacute; validace?</span
      >
    </h3>
    <p class="c4 c10">
      <span class="c2"
        >Metoda testov&aacute;n&iacute; dat. Data se rozd&#283;l&iacute; na n
        &#269;&aacute;st&iacute; (nap&#345;. n = 10). 1/10 dat se pou&#382;ije
        pro testov&aacute;n&iacute; a 9/10 pro u&#269;en&iacute;. Pot&eacute; se
        provede n test&#367; (model se natr&eacute;nuje na
        tr&eacute;novac&iacute; &#269;&aacute;sti a otestuje na testovac&iacute;
        &#269;&aacute;sti) poka&#382;d&eacute; s jinou podmno&#382;inou dat
        tvo&#345;&iacute;c&iacute; tr&eacute;novac&iacute; a testovac&iacute;
        mno&#382;inu. V&yacute;sledky test&#367; se
        zpr&#367;m&#283;ruj&iacute;.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-40">
      <span class="c8">47. Jak funguje metoda leave-one-out ?</span>
    </h3>
    <p class="c4 c10">
      <span>Obdoba k&#345;&iacute;&#382;ov&eacute; validace. </span
      ><span class="c2"
        >M&aacute;me-li n vzor&#367; (vzory jsou nap&#345;. &#345;&aacute;dky v
        csv souboru), n-1 u&#269;&iacute;me a jeden pou&#382;ijeme na
        testov&aacute;n&iacute;. Provedeme n test&#367; s poka&#382;d&eacute;
        jin&yacute;m testovac&iacute;m vzorem. Metoda m&#367;&#382;e b&yacute;t
        &#269;asov&#283; n&aacute;ro&#269;n&aacute; (z&aacute;le&#382;&iacute;
        na mno&#382;stv&iacute; dat a modelu).
      </span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-41">
      <span class="c8"
        >48. Jak&yacute;m zp&#367;sobem vyb&iacute;r&aacute;me vzory do
        tr&eacute;novac&iacute; mno&#382;iny metodou bootstrap ?</span
      >
    </h3>
    <p class="c4">
      <span
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proto&#382;e se v
        metod&#283; bootstrap </span
      ><span class="c14"
        >mohou n&#283;kter&eacute; vzory v tr&eacute;novac&iacute;
        mno&#382;in&#283;</span
      ><span>&nbsp;</span><span class="c14">opakovat</span
      ><span
        >, vyb&iacute;r&aacute;me data na tr&eacute;nov&aacute;n&iacute;
        n&aacute;hodn&#283; n-kr&aacute;t (n&#283;co jako kdy&#382; z
        ko&scaron;&iacute;ku n-kr&aacute;t vyt&aacute;hnu pap&iacute;rek s
        &#269;&iacute;slem, kter&eacute; si </span
      ><span>zapamatuji</span><span>, </span><span>vr&aacute;t&iacute;m</span
      ><span class="c2"
        >&nbsp;ho zp&aacute;tky a zase vytahuju dal&scaron;&iacute;
        pap&iacute;rek s t&iacute;m, &#382;e m&#367;&#382;u vyt&aacute;hnout
        stejn&yacute; pap&iacute;rek).
      </span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-42">
      <span class="c8"
        >49. Jak&yacute;m zp&#367;sobem vyb&iacute;r&aacute;me vzory do
        tr&eacute;novac&iacute; mno&#382;iny metodou n&aacute;hodn&eacute;ho
        v&yacute;b&#283;ru ?</span
      >
    </h3>
    <p class="c4">
      <span
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Z dat vybereme
        n&aacute;hodn&#283; p&#345;ibli&#382;n&#283; 75 % dat pro
        tr&eacute;nov&aacute;n&iacute; a 25 % pro testov&aacute;n&iacute; s
        t&iacute;m, &#382;e se data </span
      ><span class="c14">nesm&iacute; opakovat</span><span class="c2">.</span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-43">
      <span class="c8"
        >50. Po&#382;adovan&aacute; odezva klasifik&aacute;toru je d = {
        1, 3, 2, 2, 2, 3, 1 } a skute&#269;n&aacute; odezva byla y = { 1, 2, 2,
        3, 2, 2, 1} sestavte matici z&aacute;m&#283;n.</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >d (predicted) = &nbsp; &nbsp; &nbsp;{ 1, 2, 2, 3, 2, 2, 1}</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >y (actual) &nbsp; &nbsp; &nbsp;= &nbsp; &nbsp; &nbsp;{ 1, 3, 2, 2, 2,
        3, 1 }</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp;ACTUAL</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >PREDICTED &nbsp; &nbsp; &nbsp;1 &nbsp; &nbsp; 2 &nbsp; &nbsp; 3</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;1
        &nbsp; &nbsp; &nbsp;2 &nbsp; &nbsp; 0 &nbsp; &nbsp; 0</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2
        &nbsp; &nbsp; &nbsp;0 &nbsp; &nbsp; 2 &nbsp; &nbsp; 2</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;3
        &nbsp; &nbsp; &nbsp;0 &nbsp; &nbsp; 1 &nbsp; &nbsp; 0
      </span>
    </p>
    <p class="c4"><span class="c2">--- </span></p>
    <p class="c4">
      <span class="c2">ACCURACY = DIAGONALA / ZBYTEK = 4 / 7</span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-44">
      <span class="c8"
        >51. Po&#382;adovan&aacute; odezva klasifik&aacute;toru je d = {
        true, true, false, false, true, false, true } a skute&#269;n&aacute;
        odezva byla y = { true, false, false, false, true, true, false}
        spo&#269;t&#283;te celkovou p&#345;esnost klasifikace. &nbsp;
      </span>
    </h3>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >d (actual) = &nbsp; &nbsp; &nbsp;{ true, true, false, false, true,
        false, true }</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >y (predicted) = { true, false, flase, false, true, true, false}</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp; &nbsp; &nbsp;Actual</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >PREDICTED &nbsp; &nbsp; &nbsp; &nbsp; true &nbsp;false</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >true &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp;2TP &nbsp;1FP &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      </span>
    </p>
    <p class="c4">
      <span class="c2"
        >false &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; 2FN &nbsp;2TN</span
      >
    </p>
    <p class="c4">
      <span class="c2">ACCURACY = DIAGONALA / ZBYTEK = 4 / 7</span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-45">
      <span class="c8"
        >52. Popi&scaron;te k-NN klasifik&aacute;tor. Co znamen&aacute; k
        ?</span
      >
    </h3>
    <p class="c4 c10">
      <span
        >Je jeden z nejjednodu&scaron;&scaron;&iacute;ch
        klasifik&aacute;tor&#367;, kde se vybere z tr&eacute;novac&iacute;
        mno&#382;iny </span
      ><span class="c20">k</span
      ><span class="c2"
        >&nbsp;nejbli&#382;&scaron;&iacute;ch soused&#367;. V
        p&#345;&iacute;pad&#283; klasifikace vybran&eacute; vzory
        (nejbli&#382;&scaron;&iacute; soused&eacute;) hlasuj&iacute; o
        kategorii, kdy nejv&#283;t&scaron;&iacute; zastoupen&iacute; kategorie
        v&iacute;t&#283;z&iacute;. U&#269;en&iacute; zde nen&iacute;.
        Prob&iacute;h&aacute; pouze kop&iacute;rov&aacute;n&iacute;
        tr&eacute;novac&iacute; mno&#382;iny do klasifik&aacute;toru.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-46">
      <span class="c8"
        >53. Jak&eacute; z&aacute;kladn&iacute; typy graf&#367;
        zn&aacute;te ?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bodov&yacute;,
        spojnicov&yacute;, sloupcov&yacute;, kol&aacute;&#269;ov&yacute;, XY,
        XYZ</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-47">
      <span class="c8">54. Co to je scatter plot?</span>
    </h3>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Scatter plot
        (korela&#269;n&iacute; diagram) nebo t&eacute;&#382; bodov&yacute; graf,
        kter&yacute; zobrazuje v kart&eacute;zsk&yacute;ch
        sou&#345;adnic&iacute;ch hodnoty dvou prom&#283;nn&yacute;ch. Data jsou
        zn&aacute;zorn&#283;na jako mno&#382;ina bod&#367;.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-48">
      <span class="c8">55. Co je to ROC k&#345;ivka?</span>
    </h3>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Je graf, kter&yacute;
        popisuje kvalitu bin&aacute;rn&iacute;ho klasifik&aacute;toru v
        z&aacute;vislosti na nastaven&iacute; jeho klasifika&#269;n&iacute;ho
        prahu.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-49">
      <span class="c8"
        >56. Jak&eacute; typy dataminingu rozli&scaron;ujeme a na
        jak&eacute; datov&eacute; oblasti se soust&#345;e&#271;uj&iacute;?</span
      >
    </h3>
    <ul class="c17 lst-kix_rkiuaobcf4kn-0 start">
      <li class="c3 li-bullet-0">
        <span class="c14">Text mining</span
        ><span class="c2"
          >&nbsp;&ndash; zam&#283;&#345;en na zpracov&aacute;n&iacute;
          dokument&#367;, jejich klasifikaci a extrakci informace z
          dokument&#367; (vyu&#382;&iacute;v&aacute; lingvistick&yacute;ch
          metod)</span
        >
      </li>
    </ul>
    <ul class="c17 lst-kix_wv10rw5g4pdj-0 start">
      <li class="c3 li-bullet-0">
        <span class="c14">Web mining</span
        ><span class="c2"
          >&nbsp;&ndash; zam&#283;&#345;en na zpracov&aacute;n&iacute;
          hypertextov&yacute;ch dokument&#367; sb&iacute;ran&yacute;ch z webu a
          jejich vazeb (vyu&#382;&iacute;v&aacute; metody teorie
          graf&#367;)</span
        >
      </li>
    </ul>
    <ul class="c17 lst-kix_l49d6s6i95t-0 start">
      <li class="c3 li-bullet-0">
        <span class="c14">Data mining</span
        ><span class="c2"
          >&nbsp;- standardn&iacute; datov&#283; orientovan&yacute; datamining
        </span>
      </li>
    </ul>
    <h3 class="c9" id="h.nmjylv9yllav-50">
      <span class="c8"
        >57. [WIP] Co je to bag-of-words a kde se
        pou&#382;&iacute;v&aacute;?</span
      >
    </h3>
    <p class="c4">
      <span class="c2">Mno&#382;ina slov a jejich &#269;etnosti.</span>
    </p>
    <p class="c4">
      <span class="c2">{ahoj, 3x; p&#283;kn&#283;, 5x; &#269;au, 0x}</span>
    </p>
    <p class="c4">
      <span class="c2"
        >Pou&#382;&iacute;v&aacute; se nap&#345;. u m&#283;&#345;en&iacute;
        podobnosti dvou dokument&#367; (Cos&iacute;nova podobnost), nebo u
        detekce spamu v emailu (Bayes).</span
      >
    </p>
    <p class="c4"><span class="c2">---</span></p>
    <p class="c4">
      <span class="c2"
        >Nen&iacute; informace o po&#345;ad&iacute; slov - nelze zrekonstruovat,
        &#269;i porozum&#283;t.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <h3 class="c9" id="h.nmjylv9yllav-51">
      <span class="c8"
        >58. Odvo&#271;te Bayesovu v&#283;tu z
        podm&iacute;n&#283;n&yacute;ch pravd&#283;podobnost&iacute;?</span
      >
    </h3>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 275.5px;
          height: 97.86px;
        "
        ><img
          alt=""
          src="./images/image78.png"
          style="
            width: 275.5px;
            height: 97.86px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4">
      <span class="c2"
        >(Pravd&#283;podobnost jevu A za p&#345;edpokladu jevu B)</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4"><img src="./images/image54.png" /></p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 312.29px;
          height: 103.75px;
        "
        ><img
          alt=""
          src="./images/image71.png"
          style="
            width: 312.29px;
            height: 103.75px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-52">
      <span class="c8"
        >59. Jak funguje a na jak&eacute;m p&#345;edpokladu je
        zalo&#382;en Bayes&#367;v klasifik&aacute;tor?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Bayes&#367;v klasifik&aacute;tor vyu&#382;&iacute;v&aacute; Bayesovu
        v&#283;tu (o podm&iacute;n&#283;n&yacute;ch
        pravd&#283;podobnostech).</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >Vzorec na kter&eacute;m je zalo&#382;en&yacute; Bayes&#367;v
        klasifik&aacute;tor:</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 601.7px;
          height: 177.33px;
        "
        ><img
          alt=""
          src="./images/image77.png"
          style="
            width: 601.7px;
            height: 177.33px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >Naivn&iacute; Bayes&#367;v klasifik&aacute;tor vych&aacute;z&iacute; z
        p&#345;edpokladu nez&aacute;vislosti jednotliv&yacute;ch
        podm&iacute;n&#283;n&yacute;ch pravd&#283;podobnost&iacute;
        evidenc&iacute;:
      </span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 601.7px;
          height: 42.67px;
        "
        ><img
          alt=""
          src="./images/image79.png"
          style="
            width: 601.7px;
            height: 42.67px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4">
      <span class="c2"
        >Pro Bayes&#367;v naivn&iacute; klasifik&aacute;tor tedy
        plat&iacute;:</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 601.7px;
          height: 69.33px;
        "
        ><img
          alt=""
          src="./images/image69.png"
          style="
            width: 601.7px;
            height: 69.33px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4">
      <span
        >Klasifik&aacute;tor vypo&#269;&iacute;t&aacute;
        podm&iacute;n&#283;n&eacute; pravd&#283;podobnosti pro v&scaron;echny
        t&#345;&iacute;dy Ci a vybere tu, kter&aacute; m&aacute;
        nejv&#283;t&scaron;&iacute; pravd&#283;podobnost.
        Podm&iacute;n&#283;n&eacute; pravd&#283;podobnosti se
        z&iacute;sk&aacute;vaj&iacute; s tr&eacute;novac&iacute;
        mno&#382;iny.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-53">
      <span class="c8"
        >60. Jak spo&#269;teme P(E/Ci) z tr&eacute;novac&iacute;
        mno&#382;iny. Kde E je evidence (atribut nab&yacute;vaj&iacute;c&iacute;
        konkr&eacute;tn&iacute; hodnoty a Ci je i-t&aacute; kategorie)</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Vybereme z tr&eacute;novac&iacute;ch dat data kter&aacute;
        pat&#345;&iacute; do t&#345;&iacute;dy Ci. Spo&#269;&iacute;t&aacute;me
        po&#269;et z&aacute;znam&#367;, kde plat&iacute; E i Ci a
        v&yacute;sledek vyd&#283;l&iacute;me po&#269;tem v&scaron;ech
        p&#345;&iacute;pad&#367; Ci.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-54">
      <span class="c8"
        >61. Jak&aacute; jsou specifika zpracov&aacute;n&iacute;
        dokument&#367;?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >&bull; Nutno odstra&#328;ovat tagy v procesu
        p&#345;edzpracov&aacute;n&iacute;</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&bull; Dokument m&#367;&#382;e obsahovat u&#382;ite&#269;n&aacute;
        metadata, kter&aacute; je mo&#382;no vyu&#382;&iacute;t v
        dal&scaron;&iacute; anal&yacute;ze (probl&eacute;m identifikace a
        extrakce u&#382;ite&#269;n&eacute; informace z metadat)</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&bull; Dokument m&#367;&#382;e obsahovat ned&#367;le&#382;it&eacute;
        informace jako je reklama (probl&eacute;m detekce a
        odstran&#283;n&iacute;)</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&bull; Probl&eacute;m identifikace hlavn&iacute;ho bloku.</span
      >
    </p>
    <p class="c4">
      <span
        >&bull; Nutnost anal&yacute;zy stromov&eacute; struktury dokumentu</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-55">
      <span class="c8"
        >62. Co zaji&scaron;&#357;uje normalizace dokumentu?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >&bull; Transformace dat do po&#382;adovan&eacute; rozsahu</span
      >
    </p>
    <p class="c4">
      <span
        >&bull; Zav&aacute;d&iacute; invarianci (nez&aacute;vislost)
        v&#367;&#269;i </span
      ><span class="c2"
        >&ndash;Posunut&iacute; &ndash;M&#283;&#345;&iacute;tku</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&bull; &#344;ada algoritm&#367; pou&#382;&iacute;van&yacute;ch v
        dataminingu vy&#382;aduje normalizovan&aacute; data</span
      >
    </p>
    <p class="c4">
      <span
        >zp&#367;soby: norma min-max, z-scores, Eukleidovsk&aacute; norma
      </span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-56">
      <span class="c8">63. Co je stemming?</span>
    </h3>
    <p class="c4">
      <span class="c2"
        >Nahrazen&iacute; slov v r&#367;zn&yacute;ch tvarech
        z&aacute;kladem.</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >Selh&aacute;v&aacute;, m&aacute;-li slovo v&iacute;ce
        v&yacute;znam&#367;.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-57">
      <span class="c8">64. Co je lematizace?</span>
    </h3>
    <p class="c4">
      <span class="c2"
        >Stemming (nahrazen&iacute; slova z&aacute;kladem) +
        &#345;e&scaron;&iacute; kontext. Neselh&aacute;v&aacute; tedy na
        rozd&iacute;l od stemmingu v p&#345;&iacute;pad&#283;, kdy&#382;
        m&aacute; slovo v&iacute;ce v&yacute;znam&#367;.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-58">
      <span class="c8">65. Co je n-gram?</span>
    </h3>
    <p class="c4">
      <span class="c2"
        >Extrahov&aacute;n&iacute; plovouc&iacute;m ok&eacute;nkem d&eacute;lky
        N z textu -&gt; vzniknou N-tice, tedy n-gramy.</span
      >
    </p>
    <p class="c4"><span class="c2">Bi-gramy: ahoj -&gt; ah, ho, oj</span></p>
    <p class="c4"><span class="c2">Tri-gramy: ahoj -&gt; aho, hoj</span></p>
    <p class="c4">
      <span class="c2"
        >Pou&#382;&iacute;v&aacute; se nap&#345;. pro ur&#269;en&iacute; jazyka
        dokumentu.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-59">
      <span class="c8">66. o je korpus?</span>
    </h3>
    <p class="c4">
      <span class="c2"
        >Mno&#382;ina dokument&#367;. Po&#269;&iacute;ta&#269;ov&#283;
        ulo&#382;en&eacute; texty. Nap&#345;. p&#345;epis z&aacute;znamu
        mluven&eacute;ho slova.</span
      >
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-60">
      <span class="c8"
        >67. Co je Boxplot a jak&eacute; typy zn&aacute;te?</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Boxplot je graf (krabicov&yacute; graf). Tedy jeden ze
        zp&#367;sob&#367; vizualizace numerick&yacute;ch dat. Najdeme v n&#283;m
        kvantily: medi&aacute;n (0,5), horn&iacute; (0,75) a doln&iacute; (0,25)
        kvartil.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >Je n&#283;kolik typ&#367;. T&#283;lo (krabice), tj. kvantily jsou
        v&#382;dy stejn&eacute;. Li&scaron;&iacute; se pouze vousy, kter&eacute;
        mohou reprezentovat:</span
      >
    </p>
    <ul class="c17 lst-kix_mwpidxz3cpgl-0 start">
      <li class="c3 li-bullet-0"><span class="c2">minimum a maximum</span></li>
      <li class="c3 li-bullet-0">
        <span class="c2"
          >1,5 * IQR (mezikvartilov&eacute; rozp&#283;t&iacute;)</span
        >
      </li>
      <li class="c3 li-bullet-0"><span class="c2">&hellip;</span></li>
    </ul>
    <p class="c4">
      <span class="c2"
        >Pozn&aacute;mka: v mezikvartilov&eacute;m rozp&#283;t&iacute; se
        nach&aacute;z&iacute; 50 % v&scaron;ech hodnot.</span
      >
    </p>
    <p class="c4 c6">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 287.5px;
          height: 205.64px;
        "
        ><img
          alt=""
          src="./images/image67.png"
          style="
            width: 287.5px;
            height: 205.64px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title="" /></span
      ><span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 322.5px;
          height: 209.01px;
        "
        ><img
          alt=""
          src="./images/image60.png"
          style="
            width: 322.5px;
            height: 209.01px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-61">
      <span class="c8"
        >68. [OPRAVENO]Pro data
        x=(1,6,7,7,4,3,2,4,3,2,1,4,9,9,2,1,1,1,4,6) vytvo&#345;te boxplot typu
        (min, max, doln&iacute; kvartil, horn&iacute; kvartil,
        medi&aacute;n).</span
      >
    </h3>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 601.7px;
          height: 262.67px;
        "
        ><img
          alt=""
          src="./images/image62.png"
          style="
            width: 601.7px;
            height: 262.67px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title=""
      /></span>
    </p>
    <p class="c4">
      <span class="c2"
        >Tady je trochu ot&aacute;zka, jak po&#269;&iacute;tat kvartily pro
        sud&yacute; po&#269;et &#269;&iacute;sel. J&aacute; jsem pou&#382;il pro
        jednoduchost arit. pr&#367;m&#283;r. Spr&aacute;vn&#283; by se to asi
        m&#283;lo po&#269;&iacute;tat interpolac&iacute;. To u&#382; je ale
        trochu slo&#382;it&#283;j&scaron;&iacute;. Mo&#382;n&aacute; teda
        ud&#283;lat arit. pr&#367;m&#283;r a zaokrouhlit nahoru, jak
        zmi&#328;uje ve skriptech: (?)</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span>Interpolace pro Q</span><span class="c18">1</span
      ><span>&nbsp;(Q</span><span class="c18">0,25</span
      ><span class="c2">):</span>
    </p>
    <p class="c4">
      <span>Q</span><span class="c18">1</span
      ><span class="c2"
        >&nbsp;= &frac14; * (20 + 1) = 5,25t&eacute; &#269;&iacute;slo v
        &#345;ad&#283;</span
      >
    </p>
    <p class="c4"><span class="c2">5.t&eacute; je 1</span></p>
    <p class="c4"><span class="c2">6.t&eacute; je 2</span></p>
    <p class="c4"><span class="c2">rozd&iacute;l: 2 - 1 = 1</span></p>
    <p class="c4">
      <span class="c2"
        >rozd&iacute;l kr&aacute;t decim&aacute;ln&iacute; &#269;&aacute;st: 1 *
        0,25 = 0,25</span
      >
    </p>
    <p class="c4">
      <span
        >p&#345;i&#269;&iacute;st k men&scaron;&iacute;mu je v&yacute;sledek
        interpolace: 1 + 0,25 = </span
      ><span class="c14">1,25 je doln&iacute; kvartil Q</span
      ><span class="c18 c14">1</span
      ><span class="c14">&nbsp;pro metodu interpolace</span>
    </p>
    <p class="c32">
      <span
        style="
          overflow: hidden;
          display: inline-block;
          margin: 0px 0px;
          border: 0px solid #000000;
          transform: rotate(0rad) translateZ(0px);
          -webkit-transform: rotate(0rad) translateZ(0px);
          width: 506.5px;
          height: 262.91px;
        "
        ><img
          alt="aa"
          src="./images/image57.png"
          style="
            width: 506.5px;
            height: 262.91px;
            margin-left: 0px;
            margin-top: 0px;
            transform: rotate(0rad) translateZ(0px);
            -webkit-transform: rotate(0rad) translateZ(0px);
          "
          title="aa" /></span
      ><span class="c2">&nbsp;</span>
    </p>
    <h3 class="c9" id="h.nmjylv9yllav-62">
      <span class="c8"
        >69. Pro metodu nejbli&#382;&scaron;&iacute;ho
        souseda/nejvzd&aacute;len&#283;j&scaron;&iacute;ho
        souseda/centroidn&iacute; ur&#269;ete vzd&aacute;lenost shluk&#367;
        A={(1,1),(2,3),(4,1)} a B={(5,5),(5,4),(4,2)}.</span
      >
    </h3>
    <p class="c4">
      <span>pro Eukleidovskou metriku (</span
      ><img src="./images/image55.png" /><span class="c2">)</span>
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >Zji&scaron;t&#283;n&iacute; vzd&aacute;lenost&iacute; vzor&#367;
        shluk&#367;: (pro v&scaron;echny metody krom&#283;
        centroidn&iacute;):</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1,1) -&gt;
        (5,5)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5,65</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(1,1) -&gt;
        (5,4)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(1,1) -&gt; (4,2)
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3,16</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(2,3) -&gt;
        (5,5)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3,61</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(2,3) -&gt;
        (5,4)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3,16</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(2,3) -&gt; (4,2)
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2,23</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(4,1) -&gt;
        (5,5)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4,1</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(4,1) -&gt;
        (5,4)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3,16</span
      >
    </p>
    <p class="c4 c10">
      <span class="c2"
        >(4,1) -&gt; (4,2)
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1</span
      >
    </p>
    <p class="c4 c10 c7"><span class="c2"></span></p>
    <p class="c4">
      <span class="c2">Metoda nejbli&#382;&scaron;&iacute;ho souseda: 1</span>
    </p>
    <p class="c4">
      <span class="c2"
        >Metoda nejvzd&aacute;len&#283;j&scaron;&iacute;ho souseda: 5,65</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >Metoda pr&#367;m&#283;rn&eacute; vzd&aacute;lenosti: 3,45
        (pr&#367;m&#283;r vzd&aacute;lenost&iacute; shluk&#367;)</span
      >
    </p>
    <p class="c4"><span class="c2">Metoda centroidn&iacute;:</span></p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;centroid A</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1
        + 2 + 4) / 3 = 2,3</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1
        + 3 + 1) / 3 = 1,7</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;centroid B</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(5
        + 5 + 4) / 3 = 4,7</span
      >
    </p>
    <p class="c4">
      <span class="c2"
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(5
        + 4 + 2) / 3 = 3,7</span
      >
    </p>
    <p class="c4">
      <span
        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2.3, 1.7) -&gt; (4.7,
        3.7)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span
      ><span class="c24 c43">3,12</span>
    </p>
    <h3 class="c40" id="h.v6l2ntd9g5p2">
      <span class="c8"
        >70. Prove&#271;te jeden krok k-means algoritmu pro
        data={(1,1),(2,3),(4,1), (5,5),(5,4),(4,2)} a n&aacute;hodn&#283;
        generovan&eacute; st&#345;edy c1=(3,2) a c2=(4,3).</span
      >
    </h3>
    <p class="c4">
      <span class="c2"
        >Nov&eacute; centroidy se z&iacute;skaj&iacute;
        zpr&#367;m&#283;rov&aacute;n&iacute;m atribut&#367; v&scaron;ech
        vzor&#367; ve shluku.</span
      >
    </p>
    <p class="c4 c7"><span class="c2"></span></p>
    <p class="c4">
      <span>C1 = (2.33, 1.66) &nbsp; &nbsp; C2 = (4.66, 3.33)</span>
    </p>
    <a id="t.515ce96ccc06f5e7861faaeb7504083a12e78ab8"></a><a id="t.0"></a>
    <table class="c42">
      <tbody>
        <tr class="c1">
          <td class="c21" colspan="1" rowspan="1">
            <p class="c22"><span class="c2">Zadana data</span></p>
          </td>
          <td class="c21" colspan="1" rowspan="1">
            <p class="c22"><span class="c2">Identifikace shluku</span></p>
          </td>
          <td class="c21" colspan="1" rowspan="1">
            <p class="c22"><span class="c2">Nove centroidy</span></p>
          </td>
        </tr>
        <tr class="c1">
          <td class="c21" colspan="1" rowspan="1">
            <p class="c36">
              <span
                style="
                  overflow: hidden;
                  display: inline-block;
                  margin: 0px 0px;
                  border: 0px solid #000000;
                  transform: rotate(0rad) translateZ(0px);
                  -webkit-transform: rotate(0rad) translateZ(0px);
                  width: 186px;
                  height: 190.67px;
                "
                ><img
                  alt=""
                  src="./images/image64.png"
                  style="
                    width: 186px;
                    height: 190.67px;
                    margin-left: 0px;
                    margin-top: 0px;
                    transform: rotate(0rad) translateZ(0px);
                    -webkit-transform: rotate(0rad) translateZ(0px);
                  "
                  title=""
              /></span>
            </p>
          </td>
          <td class="c21" colspan="1" rowspan="1">
            <p class="c36">
              <span
                style="
                  overflow: hidden;
                  display: inline-block;
                  margin: 0px 0px;
                  border: 0px solid #000000;
                  transform: rotate(0rad) translateZ(0px);
                  -webkit-transform: rotate(0rad) translateZ(0px);
                  width: 186px;
                  height: 180px;
                "
                ><img
                  alt=""
                  src="./images/image61.png"
                  style="
                    width: 186px;
                    height: 180px;
                    margin-left: 0px;
                    margin-top: 0px;
                    transform: rotate(0rad) translateZ(0px);
                    -webkit-transform: rotate(0rad) translateZ(0px);
                  "
                  title=""
              /></span>
            </p>
          </td>
          <td class="c21" colspan="1" rowspan="1">
            <p class="c36">
              <span
                style="
                  overflow: hidden;
                  display: inline-block;
                  margin: 0px 0px;
                  border: 0px solid #000000;
                  transform: rotate(0rad) translateZ(0px);
                  -webkit-transform: rotate(0rad) translateZ(0px);
                  width: 186px;
                  height: 186.67px;
                "
                ><img
                  alt=""
                  src="./images/image56.png"
                  style="
                    width: 186px;
                    height: 186.67px;
                    margin-left: 0px;
                    margin-top: 0px;
                    transform: rotate(0rad) translateZ(0px);
                    -webkit-transform: rotate(0rad) translateZ(0px);
                  "
                  title=""
              /></span>
            </p>
          </td>
        </tr>
      </tbody>
    </table>
    <p class="c4">
      <span>Kdyztak odkaz na vysledek </span
      ><span class="c26"
        ><a
          class="c15"
          href="https://www.google.com/url?q=https://www.geogebra.org/m/hbasst42&amp;sa=D&amp;source=editors&amp;ust=1615816876953000&amp;usg=AOvVaw3hF7McVhTDwYNwoB5_is2x"
          >https://www.geogebra.org/m/hbasst42</a
        ></span
      >
    </p>
