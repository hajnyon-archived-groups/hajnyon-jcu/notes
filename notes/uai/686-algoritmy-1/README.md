# Algoritmy I

## Uvodni pojmy

### Algoritmus

-   navod (kroky) ktere urcuji postup vedouci k reseni dane ulohy
-   vsebecna pravidla urcuji transformaci vstupnich dat na vystupni
-   definice: jednoznačně určena posloupnost konečného počtu elementárních kroků vedoucí k řešení daného problému (úlohy), přičemž musí být splněny základní vlastnosti každého algoritmu
-   presnejsi definice: Algoritmus je procedura proveditelná Turingovým strojem
-   **program** - implementace algoritmu

#### Vlastnosti

**hromadnost** - pouzitelny na libovolnem vstupu splnujicim podminky

**determinismus** - kazdy krok musi byt jednoznacne definovan, alg. nesmi byt zavisly na prostredi

**rezultativnost** - po konecnem mnozstvi kroku algoritmus vzdy vrati vysledek (i chybova hlaska)

**konecnost** - algoritmus musi skoncit po konecnem mnozstvi kroku

**efektivnost** - casova - pametova - prehlednost, srozumitelnost

**spravnost** - pokud algoritmus pro validni vstup vrati validni vystup v konecnem poctu kroku

#### Tvorba algoritmu

-   zname strategie a paradigmata
-   formulace problemu, stanoveni cile, volba strategie, navržení postupu, zápis vytvořených postupu, ověření správnosti
-   definice vstupních dat, definice výstupních dat
-   overeni logicke spravnost
    -   mel by byt dokazan matematicky - tedy dukaz predem znamych operaci, ktere vedou k vysledku
    -   overeni vsech pripustnych dat (krajni pripady)

**dekompozice** - rozdeleni na podproblemy

**abstrakce** - zanedbani/ukryti detailu

**zapis algoritmu:**

-   prirozeny jazyk
-   prirozeny jazyk strukturovane (body)
-   pseudokod
-   konkretni prog. jazyk
-   vyvojovy diagram

#### Klasifikace algoritmu podle implementace

**rekurzivni** - vola sam sebe
**iteracni** - cykly

**seriove** - postupne
**paralelni** - zpracovani vice vstupu najednou

**deterministicke** - pro stejny vstup vzdy stejny vysledek
**nahodne** - nahodne prohledavaji prostor nez naleznou reseni

**presne** -
**priblizne** - pr. grafove problemy (slozitost)

#### Klasifikace algoritmu podle paradigmata

**divide and conquer**

-   rozdel a panuj
-   opakovana redukce problemu na jednodussi casti
-   casto rekurze, pr.: mergesort

**decrease and conquer**

-   sniz a panuj
-   jednodussi verze rozdel a panuj
-   lze pouzit v pripade kdy reseni mensi casti je i reseni celku
-   pr.: puleni intervalu

**greedy methods**

-   zrave metody
-   kazdy krok se vybira moznost ktera se jevi nejlepe (hleda se lokalni optimum)
-   pr.: Kruskaluv alg.

**redukce**

-   prevod problemu na jiny, znamy problem
-   pr.: hledání mediánu v nesetříděném poli -> řazení pole a výběr prostředního prvku v setříděném poli

**dynamicke programovani**

-   pri vypoctu se vyuzivaji predchozi vysledky (ktere se opakuji)
-   pr.: Pascaluv trojuhlenik

**heuristicke algoritmy**

-   vetsinou hledaji priblizne reseni

**geneticke algoritmy**

-   na zaklade napodobovani jevu z prirody

#### Klasifikace algoritmu podle slozitosti

-   slozitost ukazuje trend rustu casu/pameti vzhledem k velikosti vstupnich dat

## Slozitost

-   analyzujeme cas, pamet a efektivitu algoritmu
-   muzeme tak porovnat ruzne algoritmy

### Pristupy

**podle**

-   nejlepsiho pripadu
-   prumerneho
-   nejhorsiho

### Asymptoticka slozitost

-   odhad funkce pro rostouci mnozinu dat
-   zanedbavaji se konstanty a slozitost nizsich radu
-   lepsi je nizsi rust funkce
-   def: Asymptotická složitost algoritmu A je řád růstu funkce f(N), která charakterizuje počet elementárních operací algoritmu A při zpracování dat o rozsahu N.

![notace](./o.png)

-   **funkce**
    -   O(1) - konstantní
    -   O(log N) - logaritmická
    -   O(N) - lineární
    -   O(N log N) - lineárně-logaritmická (supralineární)
    -   O(N2) - kvadratická
    -   O(N3) - kubická
    -   Obecně O(NX) - polynomiální
    -   Obecně O(XN) - exponenciální
    -   O(N!) - faktoriálová

**FLOPS**

-   FLoating point Operations per Second
-   metrika vykonu pocitace (dnesni PC desitky/stovky gigaFLOPS)

**vypocet slozitosti algoritmu**

-   secteni elementarnich operaci (porovnani, aritmetika, presun v pameti)

**presna slozitost**

-   zanedbani konstant apod muze pinest problem ze nektere alg. stejne slozitosti jsou rychlejsi nez druhe
-   meri se na imaginarnim pocitaci, ktery ma neomezene zdroje, kazda operace je stejne dlouha a kazda polozka zabira jeden blok pameti

**efektivni algoritmy**

-   obecne se povazuji: O(n), O(n^2), O(n^3) .. tedy polynomialni
-   na kazdy problem neexistuje efektivni algoritmu (na nektere dokonce zadny)

### Tridy problemu

**P** - polynomialni cas na deterministickem PC
**NP**

-   polynomialni cas na NEdeterministickem PC
-   patri sem rozhodovaci problemy, jejichz spravnost vysledku lze overit polynomialnim algoritmem
    **NP uplny** - nejtezsi problemy z NP

#### Reseni slozitych problemu

**exponencialni algoritmy**

-   pouzitelne jen na male

**aproximacni reseni**

-   jen pro optimalizacni problemy

**pravdepodobnostni**

-   najde rychle reseni ktere je spravne s urcitou pravdepodobnosti

**specialni pripady**

-   resi jen nektere pripady, neresi cely problem

**heuristiky**

-   najde reseni rychle ve vetsine pripadu (ne vzdy)

### Optimalizace

-   ne algoritmy, ale kod
-   heavy lifting udelaji prekladace
    -   presto: znovuprovadeni operace v cyklu, pouzivani predchozich vysledku, minimalizace pristupu do pameti atd.

### Turingovy a RAM stroje

-   2 modely pocitacu/algoritmu/progr. jazyku
-   lze jimy definovat a provest algoritmus

#### Turing

-   podobny konecnemu automatu
-   nekonecna paska
-   definujeme prechodove fce ve tvaru _d(stav, znak) = (novy stav, novy znak, posun)_

#### RAM

-   vychazeji ze skutecnych PC
-   maji pamet, pracuji s cisly, instrukce
-   READ, WRITE, LOAD, STORE, ADD, SUB, MUL, DIV, JUMP ..

## Data a datove typy

**datove struktury**

-   data, operace nad nimi
-   vyber datove struktury - casova a pametova narocnost

### Datove typy

-   pomoci bitu (velikost podle platformy)
-   standartni: integer, float, char
-   definovany operace: scitani, spojovani retezcu atd ...

**zaporna cisla**

-   primy kod (nejvyssi bit urcuje znamenko, +-nula)
-   kod s posunutou nulou ()
-   doplnkovy kod (kladna stejne, zaporna inverzi + 1 tj. doplnek do nejvyssiho cisla + 1)
-   inverzni kod (kladna stejne, zaporna inverzi tj. doplnek do nejvyssiho cisla)

**semilogaritmicky tvar**

-   X = m\*z^e
-   m - mantisa - presost cisla
-   z - zaklad exponentu (v pc dvojkovy)
-   e - exponent (rozsah cisla)
-   IEEE standard
    -   2 zaklad
    -   exponent - kod s posunutou nulou
    -   mantisa - primy kod
    -   _32bitu_ - 8 exponent, 23 mantisa
    -   _64bitu_ - 11 exponent, 52 mantisa

**kodovani znaku**

-   8 bitu (muze se lisit)
-   ASCII (American Standard Code for International Interchange)
    -   prvnich 127 znaku abeceda, cisla, znaky atd, zbytek ramecky, akcentovana pismena ..
    -   nelze obsahnout vsechny znaky ruznych jazyku
-   cestina
    -   mnoho kodovani: cp-1250, iso-8859-2
    -   nutnost znat kodovani puvodniho textu
-   UNICODE
    -   16 bitu, novejsi dokonce 32bitu
    -   vejdou se tam vsechny znaky, emoji, klingonstina atd.
    -   zabira misto
    -   evropske znaky - UTF-8 - ruzne delky znaku 1-3byty

### Pole

-   seznam prvku
-   pristup pres indexy
-   hledani
    -   O(n)
    -   O(log n) v setridenem
-   vyhody
    -   prima adresace prvku
    -   rychly pristup
    -   snadne zjisteni delky
    -   snadna alokace
-   nevyhody
    -   zmena velikosti nakladna
    -   alokace pevne delky
    -   pri insertu nutno preskladat cele pole

### Spojovy seznam

-   prvky propojene referenci
-   alokace kazdeho prvku zvlast
-   oznacene zacatek a konec
-   musime si hlidat delku
-   vlozeni prvku na konec/zacatek
    -   O(1)
    -   O(n) pokud si nepamatuju konec
-   vlozeni prvku na pozici
    -   O(n)
-   modifikace: cyklicky seznam
-   nevyhody:
    -   prochazeni od zacatku

### Dvojite zretezene spojove seznamy

-   podobne jako spojove, jen lze prochazet i pozadu

### Seznamy

-   vkladani
    -   O(1) - specialni pripady
    -   O(n)
-   mazani
    -   O(1) - specialni pripady
    -   O(n)
-   hledani
    -   O(n)
    -   zlepseni: napr slovnik (pamatuju si dulezite body), kombinace poli a seznamu

### Zasobnik

-   LIFO - last in first out
-   push, pop, isEmpty, isFull

### Fronta

-   FIFO - first in first out
-   enqueue, dequeue, isEmpty, isFull

## Razeni

-   stabilni - zachovava poradi stejnych prvku
-   podle pameti - in-place
-   slozitost

### Insert sort

-   pole rozdelim na serazene a puvodni, iteruju a prvky davam na spravne misto
-   O(n^2)
-   binarni vkladani - vyuziva binarniho puleni na serazene casti pole

### Select sort

-   hleda se nejmensi prvek - interval se zkracuje
-   O(n^2)
-

### Bubble sort

-   postupne porovnavame sousdici prvky dokud nejsou serazene
-   O(n^2)
-   vyhodne na temer serazenem poli

### Heap sort

-   halda - binarni strom - lze reprezentovat polem
-   O(n log n)
-   vyhodne na temer serazenem poli

### Quick sort

-   nejrychlejsi
-   pole rozdelujeme rekurzivne na 2 podle zvoleneho pivota (ruzne strategie), kazde zase setridime
-   O(n log n)
-   vyhodne na temer serazenem poli

### Counting sort

-   snazime se vypocitat poradi prvku v serazene posloupnosti
-   potrebujeme znalost rozsahu, vstupni pole, vystupni pole a pole pomocne
-   O(n log n)
-   stabilni
-   vhodny pro prvky ktere se daji kvantizovat k << n
-   nevhodny pro velke rozsahy

### Radix sort

-   pro klice, ktere lze reprezentovat cislem z rozsahu
-   neporovnava klice, ale zpracovava jejich casti
-   radim po prihradkach, eg. 1000 -> 0-99, 100-199 ..

### Bucket sort

-   prihradkove trideni
-   skoro linearni O(n) pri rovnomernem rozlozeni prvku

## Stromy

-   mnozina uzlu
-   koren - start
-   rodice -> deti, pokud nema deti => list
-   ke kazdemu uzlu existuje jedina cesta
-   preorder, postorder, levelorder (vypis po patrech)
-   DFS (zasobnik), BFS (fronta)

### Binarni stromy

-   bud prazdny strom nebo uzel obsahujici klic a 2 potomky (levy a pravy binarni podstrom)
    **BST**
-   binary search tree
-   obsahuje klice u kterych lze urcit <,>,=
-   klice nalevo jsou menze nez koren, napravo vetsi

### AVL

-   pro kazdy strom se hloubka jeho deti muze lisit max o 1 => rozdily hloubek -1,0,+1
-   vkladani
    -   najdu misto, vlozim prvek, cestou zpet kontroluju a opravuju vyvazenost
    -   rotace doprava/doleva, doleva-doprava
-   mazani podobne

### Red-black stromy

-   volnejsi pravidla
-   uzly bude cerne nebo cervene
-   koren je cerny, deti cerveneho uzlu jsou cerne, kazdy list je cerny, cerna hloubka vsech listu je stejna

### 2-3-4 stromy

-   resi problem s castym vyvazovanim
-   moznost navazat na uzel vice deti
-   2,3,4 deti, 1,2,3 datovych polozek na uzel
    -   1 dat.pol. -> 2 deti
    -   2 dat.pol. -> 3 deti
    -   3 dat.pol. -> 4 deti
-   uzel nesmi mit jen jedno dite
-   list nesmi byt prazdny
-   pravidla (a,b,c,d deti, A,B,C dat.pol.)
    -   polozky **a** musi byt mensi nez A
    -   polozky **b** musi byt mensi nez A a B
    -   polozky **c** musi byt mensi nez A a B a C
    -   polozky **d** musi byt vetsi nez C
-   hledani
    -   koren -> vybere se vhodny potomek a pokracujeme
-   vkladani
    -   pokud mame volny uzel - vlozime
    -   pokud mame plny uzel
        -   rozdeleni -> vyvazeni stromu
        -   oddelime nejvetsi prvek, prostredni prvek presuneme do rodice, upravime vazby
    -   pokud mame plny koren
        -   hloubka se zvetsi o 1
        -   vytvorime novy koren obsahujici prostredni prvek, nejmensi a nejvetsi tvori novy level, upravim vazby
-   plytva se mistem (2/7 se vyhodi)

### 2-3 stromy

-   2,3 deti, 1,2 datovych polozek na uzel
    -   1 dat.pol. -> 2 deti
    -   2 dat.pol. -> 3 deti
-   hledani - stejne jako 2-3-4
-   vkladani
    -   nelze upravovat cestou dolu
    -   neplny list - pouze vlozim
    -   plny uzel, neplny rodic
        -   rozdelim list na 2, prostredni prvek dam do rodice
    -   plny uzel, plny rodic
        -   rozdeleni rodice (prip. rodic rodice)

### B-stromy

-   uzel max. n polozek (n-rad stromu)
-   kazdy uzel (mimo koren) alespon n/2 polozek
-   kazdy uzel je bud list nebo ma m+1 nasledovniku (m je pocet polozek)
-   hloubka vsech listu je stejna
-   zajisteno naplneni alespon na 50%
-   TODO: vkladani
-   vyuziti: souborove systemy (NTFS), velke db

## SkipList

-   n-vrstva struktura
-   pravdepodobnostni alternativa k vyvazenym stromum
-   rovnomerne rozlozeni cisel
-   logaritmicke slozitosti pro vsechny operace

## Hashovaci tabulky

-   hashovaci funkce mapuje klic na cislo (hash)
-   cilem je rovnomerne rozprostreni dat po tabulce
-   kolize

## Misc

### Interpolacni metoda

-   puleni intervalu - hodnota podle ktere pulim se bere podle linearni interpolace `m = d + (h − d) ∗ ((x − A[d])/(A[h] − A[d]))`
-   d - spodni index, h - horni index, x - hledana hodnota, A pole se serazenymi cisly
-   O(log log n)
