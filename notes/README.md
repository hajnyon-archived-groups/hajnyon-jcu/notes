---
home: true
heroText: Notes
tagline: My class notes from JCU.
footer: MIT Licensed | Copyright © 2019-present Ondřej Hajný
---

<div class="features">
    <div class="feature">
        <a href="./uai">
            <h2>UAI</h2>
            <p>Ústav aplikovane informatiky</p>
        </a>
    </div>
    <div class="feature">
        <a href="./szz">
            <h2>SZZ</h2>
            <p>Otázky ke státní závěrečné zkoušce</p>
        </a>
    </div>
    <div class="feature">
        <a href="./kja">
            <h2>KJA</h2>
            <p>Oddělení jazyků</p>
        </a>
    </div>
</div>
