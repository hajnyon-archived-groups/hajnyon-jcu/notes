# 231 - Bakalářská angličtina BA3 - NS

## Lessons

-   [Lesson 1 tasks](tasks/task-1.md)
-   [Lesson 4 tasks](tasks/task-4.md)
-   [Lesson 5 tasks](tasks/task-5.md)
-   [Lesson 6 tasks](tasks/task-6.md)
-   [Lesson 7 tasks](tasks/task-7.md)
-   [Lesson 8 tasks](tasks/task-8.md)
-   [Lesson 9 tasks](tasks/task-9.md)
-   [Lesson 10 tasks](tasks/task-10.md)

## Vocabulary

### Lesson 1

| English     | Description/Synonym | Czech                 |
| ----------- | :-----------------: | --------------------- |
| dwindling   |  decrease, reduce   | ubyvajici             |
| scarcity    |  shortage, lack of  | nedostatek            |
| persecution |                     | pronásledování        |
| hitherto    |                     | az dosud              |
| lupine      |      wolf like      | vlčí                  |
| offspring   |      children       | potomek               |
| vigorous    |      energetic      | energicky             |
| distinct    |      different      | odlišný               |
| aberrant    |                     | bloudící              |
| imagery     |                     | snímky                |
| scattered   |                     | rozptýlený, rozházený |

### Lesson 2

| English        | Description/Synonym | Czech            |
| -------------- | :-----------------: | ---------------- |
| precedent      |                     | předchozí případ |
| unprecedented  |                     | nebývalý         |
| fundamentally  |                     | zásadně          |
| wreak          |   to cause chaos    | způsobit chaos   |
| havoc          |        chaos        |                  |
| mitigate       |                     | zmírnit          |
| supposedly     | suppose to be truth | údajně           |
| circumstantial |                     | okolnost         |

### Lesson 4

| English    | Description/Synonym | Czech         |
| ---------- | :-----------------: | ------------- |
| bog        |        swamp        | bažina, močál |
| sprites    |                     | skřítek, víla |
| hostile    |     unfriendly      | nepřátelský   |
| lineage    |                     | rodokmen      |
| famine     |                     | hladomor      |
| mortician  |     undertaker      | hrobník       |
| grueling   |       tiring        | vyčerpávající |
| intestines |                     | střeva        |

### Lesson 5

| English      |   Description/Synonym   | Czech        |
| ------------ | :---------------------: | ------------ |
| purge        |         cleanse         | čištění      |
| poverty      |       being poor        | chudoba      |
| intervention |                         | zásah        |
| electoral    |                         | volební      |
| cradle       |                         | kolébka      |
| seldom       |         rarely          | zřídka       |
| harness      | control and make use of |              |
| reckless     |                         | bezohledný   |
| charter      | reserve for private use | pronájem     |
| shifts       |                         | posun, změna |
| established  |       well-known        | zavedeno     |
| policy       |  contract of insurance  | pojistka     |
| leech        |                         | pijavice     |

### Lesson 6

| English             | Description/Synonym | Czech                   |
| ------------------- | :-----------------: | ----------------------- |
| drenched            |                     | promokly                |
| drench              |                     | zmacet                  |
| discharge           |                     | vybit, propustit (vodu) |
| peak                |                     | vrchol, spicka          |
| separate assessment |                     | samostatne hodnoceni    |
| in particular       |                     | zejmena                 |
| induce              |                     | vyvolat                 |
| sustainable         |                     | udrzitelne              |

### Lesson 7

| English      | Description/Synonym | Czech |
| ------------ | :-----------------: | ----- |
| deteriorates |                     |       |
| assess       |                     |       |
| mimic        |                     |       |
| trials       |                     |       |
| widespread   |                     |       |

### Lesson 8

| English          | Description/Synonym | Czech                                 |
| ---------------- | :-----------------: | ------------------------------------- |
| committee        |                     | vybor                                 |
| conservationists |                     | pamatkar, ochr. prirody               |
| restrained       |                     | umirneny                              |
| invasive         |                     |                                       |
| waged            |      carry on       | vest                                  |
| cane toad        |                     | ropucha obrovska                      |
| eradicate        |                     | vymitit, vyhladit                     |
| utter            |                     | naprosty, totalni / pronest, vyjadrit |
| rationale        |                     | zduvodneni                            |
| nuisance         |                     | obtiz, neprijemnost                   |

### Lesson 9

| English | Description/Synonym | Czech |
| ------- | :-----------------: | ----- |
| hacking |                     |       |

### Lesson 10

| English      | Description/Synonym  | Czech |
| ------------ | :------------------: | ----- |
| plausible    |      beliavable      |       |
| resistent to |                      |       |
| idealistic   |                      |       |
| pragmatism   | realistic, practical |       |
| status quo   | unchanging situation |       |
