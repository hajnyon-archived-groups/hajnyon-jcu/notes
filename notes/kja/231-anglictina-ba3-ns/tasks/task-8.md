# Lesson 8 tasks

## Task 1 - Questions

[Text - In Defense of Invaders](https://www.prf.jcu.cz/data/files/20/70/853/50227.in-defence-of-invaders.pdf)

**How is Europe trying to fight invasive species?**

Banning cross-border trade of species and wiping out invaders if possible.

**How are they discouraged in the rest of the world?**

Waste of money and effort.

**What does the author think of attempts to eradicate invasive species?**

That it is not worth as invaders are not very successful nor harmful.

**What are the advantages of invasive species?**

They often colonise polluted or wasteland where nothing else lives and it increases biological diversity.

**What are the pros and cons of culls?**

Pros: can eradicate invaders on small islands
Cons: otherwise has short-term effect

**Why is the “philosophical rationale” behind getting rid of invasive species flawed?**

Because it states that invaders should be eradicated to restore the nature's balance, however it is not possible as the climate changed and also humans just sped up the process.

**What is a “rational attitude” to invaders?**

To fight damaging invaders.

**What does the article say about:**

_The grey squirrel_ - that they brought disease and damages trees in Europe

_Japanese knotweed_ - that it shouldn't be fought in large scale but gardeners should handle it on their own

_the American honey bee_ - it is the main crop-pollinator of America and it declines

_bracken_ - vigorous native Britain's plant

_the Nile perch_ - helped drive some fish species to extinction

## Task 2 - antonyms

Departure - arrival

Homogeny - diversity

Invincible - vulnerable

Harmless - harmful

Pointless - useful

Approvals - objections

Lethargic - vigorous

Unimportant - important/worthwhile/vital

Nearby - remote

Slowed down - accelerated

## Task 3 - Videos

### Video 1

[video](https://www.youtube.com/watch?v=i3UlH4xpwOE)

Watch the video and take notes on each animal:
How did they “invade?”
What damage do they cause?

**the Asian Carp**

They were brought by fisherman in 70s and spread during floods. They are voracious eaters.

**the Lionfish**

They were accidentally introduced to Atlantic because of hurricane. They eat native species as they don't have natural fear of them.

**the Burmese Python**

They are often released accidentally as pets by their owners. They hunt small mammals and significantly lower their numbers in Florida.

### Video 2

This is a TedEd lesson (a great resource for improving your English and general knowledge)
Watch the [video](https://ed.ted.com/lessons/the-threat-of-invasive-species-jennifer-klos#watch)
Answer the follow up [questions](https://ed.ted.com/lessons/the-threat-of-invasive-species-jennifer-klos#review)
(you will need to sign up for a free account if you wish to enter your answers. Otherwise, you can just take notes for your own learning)
If you feel compelled to get involved with the debate, you can post your thoughts here

### Video 3

[video](https://www.youtube.com/watch?time_continue=273&v=44GaC-YsAtQ&feature=emb_title)

**Why are Lionfish such a problem?**

Voracious predators - they eat a lot of food and as native fish don't recognize them as a predator it makes it easy for them.

**What solution(s) are proposed here?**

Hunting robot that kills or stuns the lionfish with electrodes.

**Are they good solutions? Are they ethical?**

James Morris thinks it is ethical because fish are harvested all over the planet.
