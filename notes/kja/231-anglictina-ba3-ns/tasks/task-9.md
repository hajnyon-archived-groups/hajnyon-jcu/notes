# Lesson 9 tasks

## Task 1 - Introduction

Check out the Philips HealthSuite Digital Platform website here and watch the following video for more information about this product:https://www.youtube.com/watch?v=0RhSytysl5U

Answer the following questions:
**What is the point of this product/service?**

To connect person's health data directly with doctors.

**How does it work?**

**How could it help a patient with Diabetes?**

Monitor their diet and glucose level and share it with their doctors.

## Task 2 - Questions

[Text - Meet My Data](https://www.prf.jcu.cz/data/files/20/70/853/50238.meet-my-data.pdf)

**What is a “cyberchondriac” and what do they do?**

A person who looks for information about his/her health online.

**How does this cause problems for doctors and medics?**

Cyberchondriacs often misjudge their symptoms (headache? bam cancer!).

**How do British people collect data about their health?**

Wearables, smartphones and apps in them.

**What are the main problems connected with web based diagnoses and digital health tracking devices?**

Not accurate data from patients. Multiple sources of patients records of previous treatment.

**How will the Phillips HealthSuite Digital Platform help?**

Cloud based system that connects patients data with doctors.

**What does the “Picture of Health” report say about this proposed system?**

It would benefit the doctors and coordinate their work allowing them to fulfill their role.

## Task 3 - Vocabulary

Try to use all of the words below in answering the following questions.
To embrace, To contribute, To collaborate, To engage, Challenge, Inadequate, Symptoms, Services, Pattern, Condition, Signs, Resources

**What do you think of the health care system of your country? What are its positives and negatives?**

**Have you experienced the health care system of another country? What was your impression?**
**What characteristics and skills must a good doctor/nurse/medical worker have?**
**What are the key elements of a successful public health system?**
