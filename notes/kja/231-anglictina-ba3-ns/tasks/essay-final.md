# Text comparison

For the text comparison I choose articles "Alan Turing and the Apple" and "In Praise of Human Guinea Pigs". The **former** describes why was Alan Turing important in **process** of developing a computer and whether or not is Apple logo an **honour** to his achievements, whereas the latter focuses on a method called Randomized Controlled Trial (or RCT) which helps to detect the better **approach** not only in medical care, but also in politics and other decision-making.

The text about Alan Turing is mostly a story of Alan's life wrapped with the possible **connection** to the Apple logo. Even though it doesn't **feature** a lot of specific points, it still **delivers** some interesting information about his achievements and history of computers. The main **purpose** of the second text is to inform about RCT method and **despite** it also mentioning the history, it focuses more to **recent** usage and possible or missed opportunities to use it. You can see that it is taken more seriously than the first one, which is supported by the small pun hidden at the end of the first article.

The topics of both articles are different. As the **previous** paragraph suggest, the Alan Turing article is more about telling a story of not so well-known mathematician whose actions and inventions **turned out** to be used even in today's computers and **likely** **led to** shortening the WW II by years. However, the article using playful title Human Guinea Pigs, focuses on why is a good idea to use RCT method, where are we using and **questions** the **lack of** usage in some areas. It **supports** its ideas with successful **examples** and also **provides** some facts.

Judging by the style of writing, both articles seem to be meant for wide public without previous knowledge of the topic. There are **rarely** used some specific terms that not all readers can understand such as maybe a "German Enigma". Other than that the text is easy to read, well-structured and clearly organized. Generally both articles well-arranged with optimal length and are a good starting point for anyone interested in either topic.

Personally I like both articles because they both serve an interesting content. Author of the first text used a simple question about the logo and then described part of Alan's life before answering. This is a nice idea and plays with the story well. He could highlighted a bit more the **exceptional** mind of Turing who not only **established** basics of modern informatics but also **contributed** to maths, logic and crypto analysis. That is obviously subjective as I have already known his name before reading. I just think he deserves more recognition. The second article was a nice introduction to RCT and it made me look into this topic more thoroughly.

So even though each text is written a little bit differently and describes various subjects, both articles accomplish their purpose in the same way. The well-written text, used vocabulary and interesting topics keep the reader focused till the end.

Words count: 506
Highlighted words count: 23
