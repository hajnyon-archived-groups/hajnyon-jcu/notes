# Lesson 1 tasks

## Task 1

### Definitions

**scarcity** - shortage, short of supply, lack of

**deliberate** - do something intentionally

**vigorous** - full of energy, strong, healthy

**vast** - huge, broad, wide

**claim** - to state or assert something, usually without evidence or proof

### Critical reading

-   article: A New Animal Species Emerging in North America
-   author: unknown
-   source: adapted from The Economist

In the article “A New Animal Species Emerging in North America” the unknown author from The Economist magazine informs about a new animal. In the world of animals there are a lot of species that have unusual abilities, different diet habits or unique physique but what makes “coywolfs” interesting are their ancestors. Due to lack of partners wolfs were made to mate with different species, namely coyotes and dogs, which led to an extraordinarily fit new animal. But can we label it a new species or not? The opinions differ.

First of we are introduced to the cause of the situation. Biologists recon, that century or two ago, wolves faced to the shortage of sexual partners which together with deforestation and persecution led to them widely breeding with dogs and coyotes. Even though this kind of interbreeding usually results in less healthy and strong offspring, this time it was an exception. The newly developed animal has a big advantages such as faster legs and larger jaws as states Dr Kays. Thanks to this “coywolfs” were able to spread through large territory even touching cities like New York. The main question is whether it is a new species or not. Some claim it is because of its sufficient divergence from its ancestors as stated by Jonathan Way. Others disagree supporting their claim by the definition of a species. It states that population of a species will not interbreed. However, this argument is debatable as all the mentioned species mated together and therefore they don't fit the description as well. As illustrated in the last paragraph, the concept of species is not clear-cut as recent discovery suggest that even Homo sapiens may be also product of hybridisation.

The purpose of the article is to inform about an interesting interbreeding occurrence in the North America. Introduction is clear and descriptive and more details are provided as the text continues. Each paragraph presents next idea, slowly going to the root question. All ideas and information are well-structured and the arguments contain views of both sides together with statements of experts on the field. All together the text is easy to read and the reader doesn't need special knowledge of the topic.

## Task 2

**What interesting new things have you learned about naked mole rats?**
They can survive on low oxygen supply. Independent teeth movement. Similar behavior in colony as some types of insect.

**What challenges do these animals face and how have they evolved to overcome them?**
Finding food is difficult because of scattered roots. -> They started to form colonies. There are few professions in colony ea workers who supplies roots for the rest.
Low oxygen. -> Abandoned thermoregulation which consumes most oxygen. Are cold blooded.

**Why do they live in colonies?**
To better find food.

**How are they different to other mammals?**
Survival in low oxygen environment. They are cold blooded.

**How would it benefit human beings to be more like them?**
During heart attacks and other medical emergencies we could prevent the oxygen deprivation which leads to death or serious organ damage.

### Synalpheus regalis - species of snapping shrimp

-   have largest colonies of eusocial shrimps
-   one colony has up to 300 of organisms with one breeding queen.
-   they divide into workers and defenders

### Armadillo

-   'little armoured one' in Spanish
-   leathery armour shell
-   roll up into ball when in danger
-   largest: 150cm, 54kg, smallest: ~10cm, 120g
-   story: bullet bounced off armadillo and hits the shooter
