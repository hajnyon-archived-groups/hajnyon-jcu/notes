# Lesson 4 tasks

## Task 1

[Text - Mummies around the world](https://www.prf.jcu.cz/data/files/20/70/853/50183.mummies-around-the-world.pdf)

### Questions

**Take notes on the following questions. Are these statements true or False?**

_The Polish study of Egyptian mummies will provide information about what job the mummified person formally occupied_ :heavy_check_mark:

_Egyptian mummies are the oldest known mummies in the world_ :x:

_Bog bodies are very famous and well-known in Ireland_ :x:

_The lack of oxygen in Irish bogs means that the DNA of the mummy is preserved to the present day_ :x:

_The bodies were deliberately put into the bog to preserve the memory of powerful kings for millenia_ :x:

_“Black mummies” are named after the metal which coats their bodies_ :heavy_check_mark:

_Morticians used hot coals to dry out the corpse before mummification_ :heavy_check_mark:

_The Buddhist process of self-mummification takes about three years in total _ :x:

_Monks killed themselves by drinking a poisonous tea_ :heavy_check_mark:

_Self-mummification is not encouraged by Buddhist leaders today_ :heavy_check_mark:

### Further comprehension questions

**What might the Polish study teach us about mummies?**

Clues about ancient diseases, their former occupation and if they were left/right handed.

**What and who are bog bodies? How have they been so well preserved?**

They are probably unwanted mummies stored in the bogs in Ireland. Because bogs have little oxygen which keeps out the bacteria and therefore they can't compose the body.

**Who are the Chinchorro?**

Oldest known intentionally created mummies from Chile.

**What do black mummies look like and how are they created?**

They are coated black with manganese. They are created by cutting of limbs and head, scooping out organs and flesh, peeling the skin (which is reattached later), shoving hot coals in the trunk cavity to dry it. After that they rebuilt the body with sticks and animals hair and covered it with ash. Finally they painted it black with manganese.

**Why were they created?**

Possibly they believed in afterlife or as ancestor worship.

**Why did some Buddhist monks want to become mummies?**

Some believes they gain superpowers, others that they will wake up years later.

**How did these monks self-mummify? Did it work?**

They restricted themselves to a diet, drunk poisonous tea and lock themselves in a tomb and after it was done, others sealed the tomb.

### Questions from video

You are going to write a beginner’s guide on how to create a mummy.
Watch the video and answer the following questions:
https://www.youtube.com/watch?v=9gD0K7oH92U

**When does decomposition start?**

After death when cells can't rebuild.

**What role do enzymes play?**

They decompose dead cells.

**When preserving a mummy, which organs did the ancient Egyptians preserve?**

Guts: liver, stomach, intestines, lungs - in jars. Heart in the mummy.

**What is Natron?**

Mixture of 2 alkaline salts which are deadly to enzymes.

**Which diseases did ancient Egyptian mummies show high rates of?**

Cardiovascular and tuberculosis.

**What were mummies taxed as?**

Salted fish.

### Guide to making a mummy

**Why make a mummy**
**How to make a mummy**
**Possible problems**

Please use the following words (or others from the article) to write a short guide about the process of mummification: Preserve, Layer, To peel, To empty, Diet, Poisonous, Fluid, Cut off

Who wouldn't want his/her body to last even after death? I guess maybe few years later you would find a few people. They believed that it will gain you superpowers or that they will just wake up few years later refreshed and full of energy. Let's look what will you need if you wanted to do it today. There are a few options to choose from but I will present you my own way. Test on your own danger! First things first you will need to keep pretty restricted **diet**. Just seeds as monks in Asia did not that long ago. You will need to stick to this diet for a while, at least two months. The goal is to loose some weight so that enzymes have few stuff to eat. After that you are ready for next stage. It is pretty simple. Just drink some **poisonous** tea! It will make you vomit but it is a good thing! You will get rid of most of the **fluid**, it will help **to empty** your stomach and also your body will be full of poison which will help repel the enzymes. Next stage is pretty tricky because you will not feel good after previous. But we are nearly finished. You will need to lay to the spot you want to be **preserved** at. Then you need to **cut off** your legs so you can't run away. Peel off skin from your remaining body and put multiple layers of a linen soaked in heavily salted water around you instead. Now you can lay down and wait till the poison will finish its job.

## Task 2

[Notes](https://drive.google.com/file/d/1zxRxpqEeAdaFpFufpOGhcbD2lEo0JtNz/view?usp=sharing)

[Slides](https://drive.google.com/file/d/1E2knuEfvt3juEiWk5bs3BRnhsABI4RAH/view?usp=sharing)

# Task 3

![Task 3](./task4-ba3ns.jpg)
