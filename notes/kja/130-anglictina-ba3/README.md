# 130 - Bakalářská angličtina BA3

[Grammar](#tenses)

[Vocabulary](#vocabulary)

## Tenses

### Present simple

-   things that are always true or generally true
-   `A dog has four legs.`
-   habits or repeated actions
-   `She plays golf.`

### Present continuous

-   action happening at the moment
-   `She is playing golf.`
-   action happening around the time of speaking but not necessarily at this exact moment
-   `I'm reading a great book.`
-   definitive future plans
-   `I'm meeting him tomorrow.`

### Present perfect

-   unfinished
    -   since July, for 10 years ...
    -   `I have known him for a long time.`
-   finished
    -   life experience
        -   ever, never
        -   `I haven't been in Paris.`
    -   news/recent activity
        -   just, yet, recently, already
        -   `Robbers have robbed a bank.`
    -   present result
        -   `I have lost my keys (so I can't get to my house).`
    -   unfinished time word
        -   today, this week/month/year
        -   `I haven't seen her this week.`

### Past simple

-   finished actions from past
-   `I went home early. I didn't meet anybody.`

### Past continuous

-   story background, overlapping action, some past habits
-   `I was going home when he approached me.`

### Present perfect simple

-   an action that is still going on or that stopped recently, but has an influence on the present
-   `I've been waiting for hours.`

### Future simple

#### Going to

-   prediction
-   `It is going to rain.`
-   plans and intentions
-   `I'm going to buy some milk.`

#### Will / shall

-   future predictions
-   `The sun will rise at 7 am.`
-   promises, offers, requests
-   `I'll help you.`

### Future continuous

-   overlapping action
-   `I'll be waiting when you arrive.`
-   future as a matter of course (definitive action)
-   `Politic will be making a statement.`

### Future perfect simple

-   how long to a point in future (by the time)
-   `I will have known him for five years.`
-   action finished before a point in future
-   `I will have finished the homework by then.`

## Gerund and infinitive

### Gerund

-   verb which acts as noun (verb + ing)
-   after certain verbs: _admit, avoid, can’t stand, can’t help, carry on, deny, enjoy, fancy, finish, give up, keep on, imagine, involve, mind, miss, postpone,practice, risk, suggest, etc_
-   after prepositions `before leaving`
-   as subject or object `Swimming is good exercise.`

### Infinitive with to

-   after certain verbs: _Agree, appear, be able to, can’t afford, can’t wait, decide, expect, happen, have (got), help, learn, manage, offer, plan, pretend, promise, refuse, seem, teach, tend, threaten, want, would like, etc_
-   after many adjectives `It's difficult to get up early. I came here to study math.`

### Infinitive without to

-   after modal verbs: can, may, must, should ... `I can meet you at six o'clock`
-   after let, make, help `The teacher let us leave early.`
-   after some verbs of perception see, watch, hear, notice, feel, sense.. `I watched her walk away`
-   after expressions with why - `Why go out the night before an exam?`

### Gerund or infitive

-   begin, start, continue

### Gerund or infinitive (change of meaning)

-   stop, remember, forget, try, need

## Wish

**I wish == If only I**

### Wish + past simple

-   impossible or unlikely things
-   `I wish I had big house. I wish I could drive.`
-   `I wish I was/were taller.`

### Wish + would

-   generally about other people who are doing something we don't like and want to change
-   `I wish you wouldn't smoke.`

### Wish + past perfect

-   things that (didn't) happen(ed) in the past and you regret
-   `I wish you had told me that.`

## Conditionals

### Zero

-   general truths and general habits
-   **If** + present simple, present simple
-   `If you add two and two, you get four.`

### First

-   possible or likely things in the future
-   **If** + present simple, will + infinitive
-   `If it rains later, we'll stay at home.`

### Second

-   impossible things in the present / unlikely things in the future
-   **If** + past simple, would + infinitive
-   `If I won the lottery, I would sail round the world.`

### Third

-   things that didn't happen in the past and their imaginary results
-   **If** + past perfect, would + have + past participle
-   `If I had studied harder, I would have passed the exam.`

## Relative clauses

### Extra information clause / non-defining

-   commas
-   who - for people
-   which - for things
-   `'Tingo', which is a word from foreing language, means to borrow ...`

### Defining

-   no commas

#### Czech prvni pad

-   who/that - for people `the man who works`
-   which/that - for things `the car which`

#### Czech druhy pad

-   whose - for people `the man whose brother`
-   whose - for things

#### Czech ctvrty pad

-   that/who/'' - for people `the woman that/who/'' we saw`
-   which/that/'' - for things `the car that I bought`

#### Prepositions

-   `the man (that) I spoke to`
-   `the man with whom I spoke`
    ing

## Relative clause

### The

-   from old english [thát]
-   shared information/concept

-   situational context
    -   immediate context
    -   unique
    -   ``
-   verbal context
    -   mentioned before or explianed later
    -   `The women we saw yesterday`
-   numbers - poradi
    -   `the first`, `the second`
-   after superlatives
    -   `the tallest`
-   some geographical names
    -   rivers - `the Nile`, `the Mississippi`
    -   seas - `the Black sea`
    -   oceans - `the Pacific ocean`
    -   group of islands - `the Bahamas`, `the Philippines`
    -   mountains - `the Alps`
    -   countries
        -   common nouns - republic, kingdom, state, unions
    -   countries in plural - `the Netherlands`

### A/An

-   form old english [cán] - ein in german

-   new concept (appearance on the scene)
    -   `there was a king`
-   categories/classification
    -   jobs
    -   `He was a shy boy`
-   numbers
    -   `a hundred`, `a milion`

### No article

-   preposition + institution
    -   `at school`, `in church`, `in prison`, `in hospital`
-   plural of a/an
    -   in general
-   after determiniers
    -   this, that, my, your, ..
    -   `Jane's brother`
-   uncountable nouns
    -   in general
    -   `he likes music`
-   names
    -   of people
    -   some places
    -   months, days
-   some geographical names
    -   lakes
    -   single island - `Corsica`, `Sardinia`
    -   single mountain - `Mt. Everest`
    -   other countries - `Great Britain`, `France`

## Contrast and purpose

-   vocab from articles from lecture will be in final exam
-   vocab for next exam
    -   brands, misleading, claiming, advert, ad, airbrushed (photoshopped), commercials, celebrity endorsers, an advertising campaign, consumers, sued

### Contrast

-   beginning or middle

    -   even though `I played football even though I don't like it.`
    -   although
    -   though (informal, in middle or end) `She sometimes annoys me, though.`

-   in spite of/despite
    -   NOUN phrase
    -   VERB ING `Despite sleeping whole night I'm tired`
    -   THE FACT THAT `The fact that I was tired`

### Purpose

-   infinitive
-   in order to
-   so as to/so as not to
-   so that `I went to the bank so that I could speak to the manager`

## Vocabulary

### General

| English       | Description/Synonym  | Czech           |
| ------------- | :------------------: | --------------- |
| Britons       | native citizen of GB | původní britové |
| delight       |       pleasure       | potěšení        |
| earnestly     |      seriously       | vážně           |
| intrinsically |          -           | vnitřně         |
| peculiar      |        weird         | podivný         |
| reluctant     |      unwilling       | neochotný       |
| reserve       |          -           | rezervovaný     |

### Weather

| English    | Description/Synonym | Czech                   |
| ---------- | :-----------------: | ----------------------- |
| below zero |                     | pod nulou               |
| breeze     |                     | vánek                   |
| blizzard   |      snowstorm      | vánice / sněhová bouřka |
| damp       |        humid        | vlhký, mokrý            |
| drizzling  |                     | mrholit                 |
| drought    |                     | sucho                   |
| cool       |                     | chladný                 |
| flood      |                     | záplavy                 |
| freezing   |                     | mrazivý                 |
| gale-force |                     | silný vítr              |
| changeable |                     | proměnlivo              |
| chilly     |                     | chladný                 |
| hailstorm  |                     | krupobití               |
| heatwave   |                     | vlna tepla              |
| icy        |                     | ledový                  |
| mild       |                     | mírný / přívětivý       |
| monsoon    |                     | monzun (vítr)           |
| pouring    |    pouring raing    | lít, proudit            |
| scorching  |                     | spalujicí               |
| showers    |                     | přeháňky                |
| thunder    |                     | bouřka                  |
| warm       |                     | teplý                   |

### Fortune tellers

| English      | Description/Synonym | Czech                |
| ------------ | :-----------------: | -------------------- |
| clairvoyant  |   fortune teller    | věštec               |
| charlatan    |       a fraud       | šarlatán / podvodník |
| palmist      |     palm reader     | věštec z dlaně       |
| witch doctor |       shaman        | šaman                |

### Origin of foreign words

| English  | Description/Synonym | Czech            |
| -------- | :-----------------: | ---------------- |
| addict   |                     | narkoman         |
| alarm    |                     | poplach, budík   |
| broke    |    out of money     | bez peněz        |
| cab      |        taxi         | taxi             |
| escape   |                     | uniknout         |
| genuine  |                     | originální, ryzí |
| jeans    |                     | džíny            |
| hooligan |                     | chuligán         |
| husband  |                     | manžel           |
| tip      |                     | spropitné, tip   |

### Verbs

| English          |        Description/Synonym         | Czech            |
| ---------------- | :--------------------------------: | ---------------- |
| abdicate         |   give up position of king/queen   | abdikovat        |
| burden           | obligation carried with difficulty | břemeno          |
| descend          |             come down              | sestoupit        |
| discharge a duty |       do what you have to do       | splnit povinnost |
| withhold         |         keep back, not say         | zadržet, odepřít |

### Illnesses and injuries

| English              | Description/Synonym | Czech                                |
| -------------------- | :-----------------: | ------------------------------------ |
| cough                |                     | kašel                                |
| headache             |                     | bolest hlavy                         |
| rash                 |                     | vyrážka                              |
| temperature          |                     | teplota                              |
| sunburn              |                     | popálenina od slunce                 |
| being sick           |                     | být nemocný                          |
| vomit                |                     | zvracet                              |
| sneeze               |                     | kýchát                               |
| swollen              |                     | oteklý                               |
| bleed                |                     | krvácet                              |
| unconscious          |                     | bezvědomý                            |
| sprained             |                     | vymknutý                             |
| blood pressure       |                     | tlak                                 |
| food poisoning       |                     | otrava jídlem                        |
| choke                |                     | dávit se                             |
| burn                 |                     | spálenina                            |
| faint                |                     | slabý, chabý                         |
| blister              |                     | puchýř                               |
| bruise               |                     | bruise                               |
| sore throat          |                     | bolest v krku                        |
| diarrhea             |                     | průjem                               |
| a cold               |                     | rýma, nachlazení                     |
| flu                  |                     | chřipka                              |
| dizzy                |                     | závrať                               |
| a cut                |                     | řezná rána                           |
| pass out             |                     | omdlít                               |
| under the weather    |                     | pod psa                              |
| heart rate           |                     | tep                                  |
| surgery              |                     | operace                              |
| infection            |                     | infekce                              |
| pulse                |                     | puls                                 |
| life-threatening     |                     | ohrožující život                     |
| tumour               |                     | nádor                                |
| mouth ulcer          |                     | vřed v ústech                        |
| cancer               |                     | rakovina                             |
| alternative remedies |                     | alternativní medicína (bylinky atd.) |
| miracle cures        |                     | zázračné uzdravení                   |
